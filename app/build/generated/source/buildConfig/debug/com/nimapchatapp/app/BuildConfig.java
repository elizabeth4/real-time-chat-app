/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.nimapchatapp.app;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.nimapchatapp.app";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 8;
  public static final String VERSION_NAME = "1.2";
  // Fields from build type: debug
  public static final String BASE_URL = "DEBUG_BASE_URL";
  public static final String ROOT_NODE = "debug";
}
