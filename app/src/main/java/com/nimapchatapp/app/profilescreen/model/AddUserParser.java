package com.nimapchatapp.app.profilescreen.model;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.nimapchatapp.app.globel.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddUserParser extends AsyncTask<Void, Void, Integer> implements
        AsyncLoaderNew.Listener {


    private String TAG = "AddUserParser";

    // Variable Declarations
    private AsyncLoaderNew asyncLoader;
    private Context context;
    private String responseString;
    private WhooshhDB db;
//    private ArrayList<ContactModel> listOfContact;

    // Callbacks for the initiator
    public interface RegistrationListener {

        void didReceivedAddUserResult(int resultCode);
    }

    private RegistrationListener listener;

    public void setListener(RegistrationListener listener) {
        this.listener = listener;
    }

    public AddUserParser(Context context) {
        super();
        this.context = context;
    }

    public void addUserWith(String username, String status, String last_seen, String auto_download) {

//        listOfContact = db.getAllDeviceContacts();
//        ArrayList<String> listOfNumber = new ArrayList<>();

//        if(listOfContact.size() > 0){
//            for(int i = 0; listOfContact.size() > i; i++ ){
//
//                listOfNumber.add(listOfContact.get(i).mobileNumber);
//            }
//        }
//        JSONArray jsonArrayNumber = new JSONArray(listOfNumber);



        if(last_seen.trim().equalsIgnoreCase("true")){
            last_seen = "1";
        }else {
            last_seen = "0";
        }


        if(auto_download.trim().equalsIgnoreCase("true")){
            auto_download = "1";
        }else {
            auto_download = "0";
        }


        String country_code = PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.COUNTRY_CODE, AppConstant.TAG.DEFAULT);

        if(country_code.contains("+")){
            country_code = country_code.replace("+","");
        }

        String fcm_id = PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.DEVICE_TOKEN, AppConstant.TAG.DEFAULT);

        if(fcm_id.isEmpty() || fcm_id.equals(AppConstant.TAG.DEFAULT)){
            getFcmID(username, status, last_seen, auto_download,country_code);
        }
        else {

            asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER,
                    AsyncLoaderNew.OutputData.TEXT, context);
            asyncLoader.setListener(this);
            JSONObject jsonObject = new JSONObject();

            db = new WhooshhDB(context);

            JSONArray jsonArrayNumber = new JSONArray();


            try {

                // We need set the user type to always vendor
                jsonObject.accumulate(AppConstant.PARSER_TAG.USERNAME, username);
                jsonObject.accumulate(AppConstant.PARSER_TAG.USER_ID, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.USER_ID, AppConstant.TAG.DEFAULT));
                jsonObject.accumulate(AppConstant.PARSER_TAG.MOBILE, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.MOBILE, AppConstant.TAG.DEFAULT));

                jsonObject.accumulate(AppConstant.PARSER_TAG.COUNTRY_CODE, country_code);

                jsonObject.accumulate(AppConstant.PARSER_TAG.IMAGE, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.IMAGE, AppConstant.TAG.DEFAULT));
                jsonObject.accumulate(AppConstant.PARSER_TAG.THUMB_IMAGE, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.THUMB_IMAGE, AppConstant.TAG.DEFAULT));
                jsonObject.accumulate(AppConstant.PARSER_TAG.DEVICE_TOKEN, fcm_id);
                jsonObject.accumulate(AppConstant.PARSER_TAG.LAST_SEEN, last_seen);
                jsonObject.accumulate(AppConstant.PARSER_TAG.AUTO_DOWNLOAD, auto_download);
//                jsonObject.accumulate(AppConstant.PARSER_TAG.FCM_ID, fcm_id);
                jsonObject.accumulate(AppConstant.PARSER_TAG.OS_TYPE, AppConstant.OS_TYPE);

                jsonObject.accumulate(AppConstant.PARSER_TAG.STATUS_LOWER, status);


                jsonObject.accumulate(AppConstant.PARSER_TAG.LIST_OF_CONTACTS, jsonArrayNumber);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            // 4. convert JSONObject to JSON to String
            String jsonString = jsonObject.toString();
            AppConstant.showErrorLog("AddUserParser", "jsonString " + jsonString);
            asyncLoader.requestMessage = jsonString;
            asyncLoader.requestActionMethod = "";
            asyncLoader.executeRequest(AppConstant.BASE_URL + AppConstant.ADD_USER_URL);

        }
    }



    private void addUserWith(String username, String status, String last_seen, String auto_download,String fcm_id,String country_code){

        asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER,
                AsyncLoaderNew.OutputData.TEXT, context);
        asyncLoader.setListener(this);
        JSONObject jsonObject = new JSONObject();

        db = new WhooshhDB(context);

        JSONArray jsonArrayNumber = new JSONArray();


        try {

            // We need set the user type to always vendor
            jsonObject.accumulate(AppConstant.PARSER_TAG.USERNAME, username);
            jsonObject.accumulate(AppConstant.PARSER_TAG.USER_ID, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.USER_ID, AppConstant.TAG.DEFAULT));
            jsonObject.accumulate(AppConstant.PARSER_TAG.MOBILE, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.MOBILE, AppConstant.TAG.DEFAULT));

            jsonObject.accumulate(AppConstant.PARSER_TAG.COUNTRY_CODE, country_code);

            jsonObject.accumulate(AppConstant.PARSER_TAG.IMAGE, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.IMAGE, AppConstant.TAG.DEFAULT));
            jsonObject.accumulate(AppConstant.PARSER_TAG.THUMB_IMAGE, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.THUMB_IMAGE, AppConstant.TAG.DEFAULT));
            jsonObject.accumulate(AppConstant.PARSER_TAG.DEVICE_TOKEN, fcm_id);
            jsonObject.accumulate(AppConstant.PARSER_TAG.LAST_SEEN, last_seen);
            jsonObject.accumulate(AppConstant.PARSER_TAG.AUTO_DOWNLOAD, auto_download);
//            jsonObject.accumulate(AppConstant.PARSER_TAG.FCM_ID, fcm_id);
            jsonObject.accumulate(AppConstant.PARSER_TAG.OS_TYPE, AppConstant.OS_TYPE);

            jsonObject.accumulate(AppConstant.PARSER_TAG.STATUS_LOWER, status);


            jsonObject.accumulate(AppConstant.PARSER_TAG.LIST_OF_CONTACTS, jsonArrayNumber);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 4. convert JSONObject to JSON to String
        String jsonString = jsonObject.toString();
        AppConstant.showErrorLog("AddUserParser", "jsonString " + jsonString);
        asyncLoader.requestMessage = jsonString;
        asyncLoader.requestActionMethod = "";
        asyncLoader.executeRequest(AppConstant.BASE_URL + AppConstant.ADD_USER_URL);


    }

    private void getFcmID(final String username, final String status, final String last_seen, final String auto_download,final String country_code) {

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());

                            if (listener != null) {
                                listener.didReceivedAddUserResult(AppConstant.PROCESSING_ERROR);
                            }
                        asyncLoader = null;

                        }
                        else {

                            // Get new Instance ID token
                            String token = task.getResult().getToken();
                            Log.d(TAG, "fcm token = "+token);
                            addUserWith(username,status,last_seen,auto_download,token,country_code);

                        }
                    }
                });

    }

    //AsyncLoaderNew.Listener callbacks
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status, int dataIndex) {
        if (listener != null) {
            listener.didReceivedAddUserResult(AppConstant.CONNECTION_ERROR);
        }
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {
        responseString = new String(data);
        // we need to call the background thread to process the data.
        AppConstant.showErrorLog("AddUserParser", "responseString " + responseString);
        asyncLoader = null;
        execute();
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }

    // Life cycle of AsyncTask
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    @Override
    protected Integer doInBackground(Void... params) {
        try {
             db = new WhooshhDB(context);
             JSONObject result = new JSONObject(responseString);

            int Status = result.getInt(AppConstant.PARSER_TAG.STATUS);

            if (Status == 200 || Status == 204) {

                //WhooshhDB db = new WhooshhDB(context);


                // It is successful response from the server we can process the json file
                JSONObject userObject = result.getJSONObject(AppConstant.PARSER_TAG.DATA);

                UserModel userModel = new UserModel(userObject,AppConstant.DEFAULT_TRUE,AppConstant.DEFAULT_ZERO);



                if(db.isUserExit(userModel.userId)){
                    db.updateUser(userModel);
                }
                else {
                    db.insertUSER(userModel);
                }

                return AppConstant.SUCCESS;

            } else if (Status == 201) {
                return AppConstant.PROCESSING_ERROR;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("JSONException", "JSONException " + e);
            cancel(true);
        }

        return AppConstant.PROCESSING_ERROR;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        int resultCode = integer.intValue();

        if (listener != null) {
            if (resultCode == AppConstant.SUCCESS) {
                listener.didReceivedAddUserResult(AppConstant.SUCCESS);
            } else {
                listener.didReceivedAddUserResult(AppConstant.PROCESSING_ERROR);
            }
        }
    }
}
