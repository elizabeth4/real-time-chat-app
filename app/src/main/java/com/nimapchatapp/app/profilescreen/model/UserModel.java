package com.nimapchatapp.app.profilescreen.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.WMasterModel;

import org.json.JSONException;
import org.json.JSONObject;


public class UserModel extends WMasterModel  implements Parcelable {

    public String id;
    public String Lid;
    public boolean isLoginUser;
    public String country_code;
    public String last_seen_hide;
    public String auto_download;
    public String os_type;
    public String userToken;
    public String offline_path;
    public String created_date;
    public String modified_date;

    public UserModel() { }

    public UserModel(JSONObject userObject, boolean isLoginUser, String lid) throws JSONException {


        id = userObject.getString(AppConstant.PARSER_TAG.ID);
        country_code = userObject.getString(AppConstant.PARSER_TAG.COUNTRY_CODE);
        last_seen_hide = userObject.getString(AppConstant.PARSER_TAG.LAST_SEEN);
        auto_download = userObject.getString(AppConstant.PARSER_TAG.AUTO_DOWNLOAD);
        os_type = userObject.getString(AppConstant.PARSER_TAG.OS_TYPE);
        created_date = userObject.getString(AppConstant.PARSER_TAG.CREATED_DATE);
        modified_date = userObject.getString(AppConstant.PARSER_TAG.MODIFIED_DATE);

        userToken = userObject.getString(AppConstant.PARSER_TAG.USER_TOKEN);

        name = userObject.getString(AppConstant.PARSER_TAG.USERNAME);
        userId = userObject.getString(AppConstant.PARSER_TAG.USER_ID);
        mobileNumber = userObject.getString(AppConstant.PARSER_TAG.MOBILE);
        image = userObject.getString(AppConstant.PARSER_TAG.IMAGE);
        thumb_image = userObject.getString(AppConstant.PARSER_TAG.THUMB_IMAGE);
        device_token = userObject.getString(AppConstant.PARSER_TAG.DEVICE_TOKEN);
        status = userObject.getString(AppConstant.PARSER_TAG.USER_STATUS);

        offline_path = "";

        this.isLoginUser = isLoginUser;
        this.Lid = lid;


    }


    protected UserModel(Parcel in) {
        id = in.readString();
        Lid = in.readString();
        isLoginUser = in.readByte() != 0;
        country_code = in.readString();
        last_seen_hide = in.readString();
        auto_download = in.readString();
        os_type = in.readString();
        userToken = in.readString();
        offline_path = in.readString();
        created_date = in.readString();
        modified_date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(Lid);
        dest.writeByte((byte) (isLoginUser ? 1 : 0));
        dest.writeString(country_code);
        dest.writeString(last_seen_hide);
        dest.writeString(auto_download);
        dest.writeString(os_type);
        dest.writeString(userToken);
        dest.writeString(offline_path);
        dest.writeString(created_date);
        dest.writeString(modified_date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
