package com.nimapchatapp.app.profilescreen.model;

import android.content.Context;
import android.os.AsyncTask;

import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.AsyncLoaderNew;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.globel.WhooshhDB;

import org.json.JSONException;
import org.json.JSONObject;

public class EditUserParser extends AsyncTask<Void, Void, Integer> implements
        AsyncLoaderNew.Listener {


    private String TAG = "EditUserParser";

    // Variable Declarations
    private AsyncLoaderNew asyncLoader;
    private Context context;
    private String responseString;
    private WhooshhDB db;
//    private ArrayList<ContactModel> listOfContact;

    // Callbacks for the initiator
    public interface EditUserProfileListener {

        void didReceivedEditdUserResult(int resultCode);
    }

    private EditUserParser.EditUserProfileListener listener;

    public void setListener(EditUserParser.EditUserProfileListener listener) {
        this.listener = listener;
    }

    public EditUserParser(Context context) {
        super();
        this.context = context;
    }

    public void editUserWith(String username, String status, String last_seen, String auto_download,String userToken,String userId,boolean is_image_change) {


        String string_is_image_change = is_image_change ? "1":"0";



        if(last_seen.trim().equalsIgnoreCase("true")){
            last_seen = "1";
        }else {
            last_seen = "0";
        }


        if(auto_download.trim().equalsIgnoreCase("true")){
            auto_download = "1";
        }
        else {
            auto_download = "0";
        }



        PreferenceManager.saveStringForKey(context,AppConstant.HEADER_PARM_USER_ID, userId);
        PreferenceManager.saveStringForKey(context, AppConstant.HEADER_PARM_USER_TOKEN, userToken);

            asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER,
                    AsyncLoaderNew.OutputData.TEXT, context);
            asyncLoader.setListener(this);
            JSONObject jsonObject = new JSONObject();

            db = new WhooshhDB(context);


            try {

                // We need set the user type to always vendor
                jsonObject.accumulate(AppConstant.PARSER_TAG.USERNAME, username);


                jsonObject.accumulate(AppConstant.PARSER_TAG.IMAGE, PreferenceManager.getStringForKey(context, AppConstant.PREFERENCE_TAG.IMAGE, AppConstant.TAG.DEFAULT));

                jsonObject.accumulate(AppConstant.PARSER_TAG.LAST_SEEN, last_seen);
                jsonObject.accumulate(AppConstant.PARSER_TAG.AUTO_DOWNLOAD, auto_download);
                jsonObject.accumulate(AppConstant.PARSER_TAG.IS_IMAGE_CHANGE, string_is_image_change);

                jsonObject.accumulate(AppConstant.PARSER_TAG.STATUS_LOWER, status);





            } catch (JSONException e) {
                e.printStackTrace();
            }

            // 4. convert JSONObject to JSON to String
            String jsonString = jsonObject.toString();
            AppConstant.showErrorLog("EditUserParser", "jsonString " + jsonString);
            asyncLoader.requestMessage = jsonString;
            asyncLoader.requestActionMethod = "";
            asyncLoader.executeRequest(AppConstant.BASE_URL + AppConstant.EDIT_USER_URL);

//        }
    }





    //AsyncLoaderNew.Listener callbacks
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status, int dataIndex) {
        if (listener != null) {
            listener.didReceivedEditdUserResult(AppConstant.CONNECTION_ERROR);
        }
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {
        responseString = new String(data);
        // we need to call the background thread to process the data.
        AppConstant.showErrorLog("EditUserParser", "responseString " + responseString);
        asyncLoader = null;
        execute();
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }

    // Life cycle of AsyncTask
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    @Override
    protected Integer doInBackground(Void... params) {
        try {
            db = new WhooshhDB(context);
            JSONObject result = new JSONObject(responseString);

            int Status = result.getInt("Status");

            if (Status == 200 || Status == 204) {

                //WhooshhDB db = new WhooshhDB(context);


                // It is successful response from the server we can process the json file
                JSONObject userObject = result.getJSONObject(AppConstant.PARSER_TAG.DATA);

                UserModel userModel = new UserModel(userObject,AppConstant.DEFAULT_TRUE,AppConstant.DEFAULT_ZERO);



                if(db.isUserExit(userModel.userId)){
                    db.updateUser(userModel);
                }
                else {
                    db.insertUSER(userModel);
                }

                return AppConstant.SUCCESS;

            } else if (Status == 201) {
                return AppConstant.PROCESSING_ERROR;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("JSONException", "JSONException " + e);
            cancel(true);
        }

        return AppConstant.PROCESSING_ERROR;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        int resultCode = integer.intValue();

        if (listener != null) {
            if (resultCode == AppConstant.SUCCESS) {
                listener.didReceivedEditdUserResult(AppConstant.SUCCESS);
            } else {
                listener.didReceivedEditdUserResult(AppConstant.PROCESSING_ERROR);
            }
        }
    }
}
