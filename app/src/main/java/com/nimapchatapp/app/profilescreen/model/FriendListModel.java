package com.nimapchatapp.app.profilescreen.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.WMasterModel;
import org.json.JSONException;
import org.json.JSONObject;

public class FriendListModel extends WMasterModel implements Parcelable {


    public FriendListModel(){

    }

    public FriendListModel(JSONObject jsonObject) throws JSONException {
        name = jsonObject.getString(AppConstant.PARSER_TAG.USERNAME);
        userId = jsonObject.getString(AppConstant.PARSER_TAG.USER_ID);
        mobileNumber = jsonObject.getString(AppConstant.PARSER_TAG.MOBILE);
        image = jsonObject.getString(AppConstant.PARSER_TAG.IMAGE);
        thumb_image = jsonObject.getString(AppConstant.PARSER_TAG.THUMB_IMAGE);
        device_token = jsonObject.getString(AppConstant.PARSER_TAG.DEVICE_TOKEN);

    }

    protected FriendListModel(Parcel in) {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FriendListModel> CREATOR = new Creator<FriendListModel>() {
        @Override
        public FriendListModel createFromParcel(Parcel in) {
            return new FriendListModel(in);
        }

        @Override
        public FriendListModel[] newArray(int size) {
            return new FriendListModel[size];
        }
    };
}
