package com.nimapchatapp.app.profilescreen.controller;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.crashlytics.android.Crashlytics;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.globel.*;
import com.nimapchatapp.app.invite.model.CheckUserParser;
import com.nimapchatapp.app.profilescreen.model.AddUserParser;
import com.nimapchatapp.app.profilescreen.model.EditUserParser;
import com.nimapchatapp.app.profilescreen.model.UserModel;
import com.nimapchatapp.app.recentchatuser.controller.RecentChatUserActivity;

import java.io.*;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class ProfileActivity extends BaseAppCompatActivity implements
        AddUserParser.RegistrationListener,
        EditUserParser.EditUserProfileListener,
        ImagePickerDialog.InputActionListener,
        View.OnClickListener,
        CheckUserParser.CheckUserListener
//        FetchContacts.ContactListener
{

    private static final String TAG = ProfileActivity.class.getSimpleName();
    private EditText nameEditText, numberEditText, statusEditText;
    private TextView submitTextView;
    private CircularImageView imgProfile;
    private ImagePickerDialog imagePickerDialog;
    private int GALLERY = 1, CAMERA = 2;
    public static final int REQUEST_READ_CONTACTS = 100;
    private String IMAGE_DIRECTORY = "photo";
    private Bitmap bitmap;
    private SwitchCompat switchCompatLastSeen, switchCompatAutoDownload;
    private boolean editFlag;
    private UserModel loginUser;
    private WhooshhDB whooshhDB;
    private boolean is_image_change;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_profile);

        requestCameraPermissions();

        setData();
        setListener();

//        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
//                == PackageManager.PERMISSION_GRANTED) {
//            getAllContactList();
//        } else {
//            requestContactPermission();
//        }
    }


    //BaseAppCompatActivity
    @Override
    protected void setData() {
        submitTextView = findViewById(R.id.submit_text_view);
        nameEditText = findViewById(R.id.name_edit_text);
        numberEditText = findViewById(R.id.number_edit_text);
        statusEditText = findViewById(R.id.status_edit_text);
        imgProfile = findViewById(R.id.img_profile);
        switchCompatLastSeen = findViewById(R.id.last_seen_switch);
        switchCompatAutoDownload = findViewById(R.id.auto_download_switch);

        statusEditText.setVisibility(View.GONE);
        switchCompatLastSeen.setVisibility(View.GONE);
        switchCompatAutoDownload.setVisibility(View.GONE);

        numberEditText.setText(PreferenceManager.getStringForKey(myContext, AppConstant.PREFERENCE_TAG.MOBILE, AppConstant.TAG.DEFAULT));

        whooshhDB = new WhooshhDB(ProfileActivity.this);

        String editProfile = getIntent().getExtras().getString(AppConstant.PROFILE_INTENT_KEY, AppConstant.EDIT_PROFILE);

        if (editProfile.equalsIgnoreCase(AppConstant.EDIT_PROFILE)) {
            editFlag = true;
        } else {

            String number = PreferenceManager.getStringForKey(myContext, AppConstant.PREFERENCE_TAG.MOBILE, AppConstant.TAG.DEFAULT);
            if (!number.equals(AppConstant.TAG.DEFAULT)) {
                checkUser(number);
            }
        }


        if (editFlag) {
            updateUI();
        }


    }

    private void updateUI() {

        loginUser = whooshhDB.getLoginUser();

        if (loginUser != null) {
            nameEditText.setText(loginUser.name);
            numberEditText.setText(loginUser.mobileNumber);
            statusEditText.setText(loginUser.status);

            /*if (loginUser.auto_download.equalsIgnoreCase("1")) {
                switchCompatAutoDownload.setChecked(true);
            } else {
                switchCompatAutoDownload.setChecked(false);
            }*/

/*            if (loginUser.last_seen_hide.equalsIgnoreCase("1")) {
                switchCompatLastSeen.setChecked(true);
            } else {
                switchCompatLastSeen.setChecked(false);
            }*/


            PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.IMAGE, loginUser.image);

            //ImageLoaderPicasso.loadImage(myContext, imgProfile, loginUser.image);
          /*  Bitmap image= drawableToBitmap(getResources().getDrawable(R.drawable.ic_user));

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 100, stream);*/

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_user);
            requestOptions.error(R.drawable.ic_user);

            Glide.with(this).setDefaultRequestOptions(requestOptions).load(loginUser.image).into(imgProfile);


        }

    }

   /* public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }*/

    @Override
    protected void setListener() {
        submitTextView.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
    }

//    private void getAllContactList(){
//        showDialog();
//        FetchContacts contacts = new FetchContacts(myContext);
//        contacts.setContactListener(this);
//        contacts.execute();
//
//    }


    private void checkUser(String number) {
        showDialog();
        CheckUserParser checkUserParser = new CheckUserParser(myContext);
        checkUserParser.setListener(this);

        checkUserParser.checkUserWith(number, AppConstant.DEFAULT_FALSE);

    }


    private void verificationField() {

        if (TextUtils.isEmpty(nameEditText.getText())) {
            nameEditText.setError("Name is required");
            nameEditText.requestFocus();
        } else /*if (TextUtils.isEmpty(statusEditText.getText())) {
            statusEditText.setError("Status is required");
            statusEditText.requestFocus();
        } else*/ {
            showDialog();

            sendUserData();

        }
    }

    private void sendUserData() {

    //    String id = FirebaseHelper.getCurrentUser().getUid();


        final HashMap<String, Object> userUpdate = new HashMap<>();

        userUpdate.put(AppConstant.PARSER_TAG.USERNAME, nameEditText.getText().toString());
        userUpdate.put(AppConstant.PARSER_TAG.STATUS, statusEditText.getText().toString());

        userUpdate.put(AppConstant.PARSER_TAG.AUTO_DOWNLOAD, switchCompatAutoDownload.isChecked());
        userUpdate.put(AppConstant.PARSER_TAG.LAST_SEEN, switchCompatLastSeen.isChecked());


        PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.USERNAME, userUpdate.get(AppConstant.PARSER_TAG.USERNAME).toString());
        PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.STATUS, userUpdate.get(AppConstant.PARSER_TAG.STATUS).toString());

        PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.AUTO_DOWNLOAD, userUpdate.get(AppConstant.PARSER_TAG.AUTO_DOWNLOAD).toString());
        PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.LAST_SEEN, userUpdate.get(AppConstant.PARSER_TAG.LAST_SEEN).toString());


        if (editFlag) {


            EditUserParser editUserParser = new EditUserParser(myContext);
            editUserParser.setListener(ProfileActivity.this);
            editUserParser.editUserWith(userUpdate.get(AppConstant.PARSER_TAG.USERNAME).toString().trim(),
                    userUpdate.get(AppConstant.PARSER_TAG.STATUS).toString().trim()
                    , userUpdate.get(AppConstant.PARSER_TAG.LAST_SEEN).toString(),
                    userUpdate.get(AppConstant.PARSER_TAG.AUTO_DOWNLOAD).toString(),
                    loginUser.userToken, loginUser.userId, is_image_change);


        } else {

            AddUserParser addUserParser = new AddUserParser(myContext);
            addUserParser.setListener(ProfileActivity.this);
            addUserParser.addUserWith(userUpdate.get(AppConstant.PARSER_TAG.USERNAME).toString().trim(),
                    userUpdate.get(AppConstant.PARSER_TAG.STATUS).toString().trim(),
                    userUpdate.get(AppConstant.PARSER_TAG.LAST_SEEN).toString(),
                    userUpdate.get(AppConstant.PARSER_TAG.AUTO_DOWNLOAD).toString());


        }


//        FirebaseHelper.getUserDateRef(id).updateChildren(userUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//                if (task.isSuccessful()) {
//
//                    PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.USERNAME, userUpdate.get(AppConstant.PARSER_TAG.USERNAME).toString());
//                    PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.STATUS, userUpdate.get(AppConstant.PARSER_TAG.STATUS).toString());
//
//                    AddUserParser addUserParser = new AddUserParser(myContext);
//                    addUserParser.setListener(ProfileActivity.this);
//                    addUserParser.addUserWith(userUpdate.get("name").toString(),
//                            userUpdate.get("status").toString());
//
//                } else {
//                    AppConstant.showToast(myContext, AppConstant.REGISTERATION);
//                }
//            }
//        });

    }

//    private void updateUserDataInFirbase(){
//
//
//        String userId = PreferenceManager.getStringForKey(myContext,
//                AppConstant.PREFERENCE_TAG.USER_ID,AppConstant.TAG.DEFAULT);
//        String name = PreferenceManager.getStringForKey(myContext,
//                AppConstant.PREFERENCE_TAG.USERNAME,AppConstant.TAG.DEFAULT);
//        String number = PreferenceManager.getStringForKey(myContext,
//                AppConstant.PREFERENCE_TAG.MOBILE, AppConstant.TAG.DEFAULT);
//        String status = PreferenceManager.getStringForKey(myContext,
//                AppConstant.PREFERENCE_TAG.STATUS, AppConstant.TAG.DEFAULT);
//        String image = PreferenceManager.getStringForKey(myContext,
//                AppConstant.PREFERENCE_TAG.IMAGE, AppConstant.TAG.DEFAULT);
//        String thumb_image = PreferenceManager.getStringForKey(myContext,
//                AppConstant.PREFERENCE_TAG.THUMB_IMAGE, AppConstant.TAG.DEFAULT);
//        String device_token = PreferenceManager.getStringForKey(myContext,
//                AppConstant.PREFERENCE_TAG.DEVICE_TOKEN, AppConstant.TAG.DEFAULT);
//
//
//        HashMap<String, String> userMap = new HashMap<>();
//        userMap.put(AppConstant.PARSER_TAG.USERNAME, name);
//        userMap.put(AppConstant.PARSER_TAG.MOBILE, number);
//        userMap.put(AppConstant.PARSER_TAG.STATUS, status);
//        userMap.put(AppConstant.PARSER_TAG.IMAGE, image);
//        userMap.put(AppConstant.PARSER_TAG.THUMB_IMAGE, thumb_image);
//        userMap.put(AppConstant.PARSER_TAG.DEVICE_TOKEN, device_token);
//
//        FirebaseHelper.getUserDateRef(userId).setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//                 if(task.isSuccessful()){
//                     hideDialog();
//                     startActivity(new Intent(myContext, ContactListActivity.class));
//                     finish();
//                 }else {
//                     hideDialog();
//                     Toast.makeText(myContext,"User Not Register",Toast.LENGTH_LONG).show();
//                 }
//            }
//        });
//    }


    private void showImagePickerDialog() {

        imagePickerDialog = ImagePickerDialog.newInstance(TAG, AppConstant.CAMERA_MESSAGE, AppConstant.TAG.CAMERA, AppConstant.TAG.GALLERY);
        imagePickerDialog.setListener(this);
        imagePickerDialog.show(getSupportFragmentManager(), TAG);

    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    protected void requestContactPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.READ_CONTACTS)) {
            // show UI part if you want here to show some rationale !!!

        } else {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
        }

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                    getAllContactList();

                } else {
                    requestContactPermission();
                    // permission denied,Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        byte[] imageData = null;
        Bitmap rotatedBitmap = null;
        String selectedImagePath = null;

        imagePickerDialog.dismiss();
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
               // Uri uri = data.getData();
                selectedImagePath = new FileOperation(this).getPath(contentURI);

                if (selectedImagePath != null) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 3;
                    options.inJustDecodeBounds = false;
                    Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

                    rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                            bitmap.getWidth(), bitmap.getHeight(),
                            getImageMatrix(selectedImagePath), true);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    /*Must compress the Image to reduce image size to make upload easy*/
                    //rotatedBitmap.compress(Bitmap.CompressFormat.WEBP, 30, stream);

                    imageData = stream.toByteArray();
                    PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.IMAGE, AppConstant.getBase64Image(rotatedBitmap));


                    if(imgProfile !=null){
                        imgProfile.setBackground(null);
                        imgProfile.setImageBitmap(rotatedBitmap);
                        is_image_change = true;
                        Toast.makeText(myContext, "Image Saved!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            if (thumbnail != null) {
                Bitmap converetdImage = getResizedBitmap(thumbnail, 150);
                PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.IMAGE, AppConstant.getBase64Image(converetdImage));
                imgProfile.setImageBitmap(thumbnail);

                is_image_change = true;
            }
            //saveImage(thumbnail);
            //Toast.makeText(ProfileActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    private static Matrix getImageMatrix(String imagePath) {
        Matrix matrix = new Matrix();
        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            if (rotation != 0) {
                int rotationInDegrees = exifToDegrees(rotation);
                matrix.postRotate(rotationInDegrees);
            }


        } catch (IOException e) {
            e.printStackTrace();
            //TxtmeUtils.show("MoreFragment", "e " + e);
        }

        return matrix;
    }
    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private void requestCameraPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            //showImagePickerDialog();
                            //Toast.makeText(getApplicationContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    //View.OnClickListener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_text_view:
                verificationField();


                break;
            case R.id.img_profile:

                showImagePickerDialog();


                break;
        }
    }


    @Override
    public void didReceivedAddUserResult(int resultCode) {
        hideDialog();
        if (resultCode == AppConstant.SUCCESS) {


//            AppConstant.showToast(myContext, AppConstant.SUCCESSFULLY_UPDATED_MESSAGE);

//            updateUserDataInFirbase();
            moveToRecentChatActivity();
        } else if (resultCode == AppConstant.NO_FRIEND_AVAILABLE) {

//            updateUserDataInFirbase();
            AppConstant.showToast(myContext, AppConstant.SUCCESSFULLY_UPDATED_MESSAGE);

            moveToRecentChatActivity();

        } else if (resultCode == AppConstant.CONNECTION_ERROR) {
            AppConstant.showToast(myContext, AppConstant.CONNECTION_ERROR_MESSAGE);
        }
    }


    private void moveToRecentChatActivity() {

        startActivity(new Intent(myContext, RecentChatUserActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        );
        ProfileActivity.this.finish();
    }

    // ImagePickerDialog.InputActionListener,
    @Override
    public void dualPrimaryActionButtonPressed(String tag) {

        takePhotoFromCamera();
    }

    @Override
    public void dualSecondaryActionButtonPressed(String tag) {

        choosePhotoFromGallary();
    }

    @Override
    public void didReceivedEditdUserResult(int resultCode) {
        hideDialog();

        if (resultCode == AppConstant.SUCCESS) {


            AppConstant.showToast(myContext, AppConstant.EDIT_PROFILE_SUCCESSFULLY_UPDATED_MESSAGE);
            ProfileActivity.this.finish();

        } else if (resultCode == AppConstant.NO_FRIEND_AVAILABLE) {

//            updateUserDataInFirbase();
            AppConstant.showToast(myContext, AppConstant.SUCCESSFULLY_UPDATED_MESSAGE);

            moveToRecentChatActivity();

        } else if (resultCode == AppConstant.CONNECTION_ERROR) {
            AppConstant.showToast(myContext, AppConstant.CONNECTION_ERROR_MESSAGE);
        }

    }

    @Override
    public void didReceivedCheckUserResult(int resultCode, String userId) {
        hideDialog();
        if (resultCode == AppConstant.SUCCESS) {
            updateUI();
        } else if (resultCode == AppConstant.User_Not_Registered) {

        } else if (resultCode == AppConstant.CONNECTION_ERROR) {
            AppConstant.showToast(myContext, AppConstant.CONNECTION_ERROR_MESSAGE);
        }
    }
    //FetchContacts.ContactListener

//    @Override
//    public void didReceivedContactResult(int intCode) {
//        hideDialog();
//        if(intCode == AppConstant.SUCCESS){
//           AppConstant.showToast(myContext,"Please fill remaining field");
//        }
//
//    }


    //
    @Override
    public void onBackPressed() {

        if (!editFlag) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Click back again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }
    }
}
