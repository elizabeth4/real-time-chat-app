package com.nimapchatapp.app.globel;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import com.nimapchatapp.app.R;

public class ProgressDialog extends DialogFragment {

    //variable declarations

    String message;
    String tag;
    boolean isAnimate;

    public ProgressDialog() {
        // Required empty public constructor
    }

    /*
    Static method for creating the fragment object for passing the arguments.
     */

    public static ProgressDialog newInstance(String tag, String message, boolean animate) {
        ProgressDialog alert = new ProgressDialog();

        Bundle bundle = new Bundle();

        bundle.putString("tag", tag);
        bundle.putString("message", message);
        bundle.putBoolean("isAnimate", animate);

        alert.setArguments(bundle);

        return alert;
    }

    // Dialogue Fragment Life Cycle Methods
    /*
    Following life cycle meyhod will bind the fragment view and will get the entire xml into the
    fragment's view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.dialogue_progress, container, false);

        message = getArguments().getString("message");
        tag = getArguments().getString("tag");
        isAnimate = getArguments().getBoolean("isAnimate");

        TextView messageTextView = (TextView) view.findViewById(R.id.alert_message);
        //messageTextView.setTypeface(Application.HelveticaNeue_Medium);
        messageTextView.setTextColor(Color.BLACK);
        messageTextView.setText(message);

//        TextView loadingTextView = (TextView) view.findViewById(R.id.loadingTextView);
//        loadingTextView.setTypeface(Application.OrnitonsSerial_Regular);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Application.isActivityRunning = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        //Application.isActivityRunning = false;
    }

    /* This method we will be configure the dialogue window property as per the requirement. */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        isAnimate = getArguments().getBoolean("isAnimate");
        if (isAnimate) {
            // dialog.getWindow().getAttributes().windowAnimations = R.style.AlertAnimation;
        }
        return dialog;
    }


    @Override
    public void show(android.support.v4.app.FragmentManager manager, String tag) {
        /*Issue caught by Crash Analytics*/
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            //AppConstant.show("PROGRESS ALERT", "Exception" + e);
        }
    }
}
