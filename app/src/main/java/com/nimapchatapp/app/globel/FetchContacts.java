package com.nimapchatapp.app.globel;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import com.nimapchatapp.app.contactscreen.model.ContactModel;

import java.util.ArrayList;


public class FetchContacts extends AsyncTask<Void, Void, Integer> {

    public ContactListener listener;

    private Context myContext;

    private WhooshhDB db;

    private ArrayList<ContactModel> list;

    private String phoneNumber;

    public interface ContactListener {

        void didReceivedContactResult(int intCode, ArrayList<ContactModel> list);
    }

    public void setContactListener(ContactListener listener) {
        this.listener = listener;
    }


    public FetchContacts(Context context, String phoneNumber) {
        this.myContext = context;
        this.phoneNumber = phoneNumber;

    }

    @Override
    protected Integer doInBackground(Void... params) {


        try {


            db = new WhooshhDB(myContext);
            this.list = new ArrayList<>();

            ContentResolver cr = myContext.getContentResolver();

            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC");
            if ((cur != null ? cur.getCount() : 0) > 0) {

                while (cur != null && cur.moveToNext()) {

                    String id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME));

                    if (cur.getInt(cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        Cursor pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id}, null);
                        while (pCur.moveToNext()) {
                            String phoneNo = pCur.getString(pCur.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER));

                            ContactModel model = new ContactModel();

                            if (phoneNo.replaceAll("\\s", "").trim().length() > 7) {
                                model.name = name;
                                model.mobileNumber = phoneNo.replaceAll("\\s", "");
                                if (model.mobileNumber.contains("-")) {
                                    model.mobileNumber = model.mobileNumber.replaceAll("-", "");
                                }
                                model.iconHexColor = AppConstant.getRandomSubscriptionHexColor(myContext);
                                if (!phoneNumber.equals(model.mobileNumber)) {
                                    list.add(model);
                                }

                            }

                            Log.i("FetchContacts", "Name: " + name);
                            Log.i("FetchContacts", "Phone Number: " + phoneNo);
                        }
                        pCur.close();
                    }
                }
            }
            if (cur != null) {
                cur.close();
            }

            return AppConstant.SUCCESS;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        if (listener != null) {
            listener.didReceivedContactResult(integer, list);
        }
    }
}
