package com.nimapchatapp.app.globel;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

public abstract class BaseFragment extends Fragment {

    public static final String TAG = BaseFragment.class.getSimpleName();
    protected abstract void setData(View view);
    protected abstract void setListener(View view);
    protected Context myContext;
    protected ProgressDialog progressAlert;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myContext = getContext();
    }

    public void showDialog() {
        if(progressAlert != null)
            progressAlert.dismissAllowingStateLoss();
        progressAlert = null;
        progressAlert = ProgressDialog.newInstance(TAG, "", false);
        progressAlert.show(getFragmentManager(), TAG);
    }

    public void hideDialog() {
        if(progressAlert != null && progressAlert.isVisible()){
            progressAlert.dismiss();
        }
    }
}
