package com.nimapchatapp.app.globel;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by priyankranka on 07/11/14.
 */
public class AsyncLoaderNew extends AsyncTask<String, Double, byte[]> {

    private static final String TAG = "AsyncLoaderNew";

    private OutputData ouputType;
    Boolean isTaskCancelled;

    private String serviceURL = "";

    private static Context context;

    private RequestMethod requestMethod;
    public State state;
    public Status status;
    public int dataIndex = -1;
    public int sectionIndex = -1;

    private SectionListener sectionListener;

    public HashMap<String, String> headers;

    public HashMap<String, String> getHeaders() {
        if (headers == null) {
            headers = new HashMap<String, String>();
        }
        return headers;
    }

    private Listener listener;

    public String requestMessage;
    public String requestActionMethod;

    public interface Listener {
        void didReceivedError(Status status, int dataIndex);

        void didReceivedData(byte[] data, int dataIndex);

        void didReceivedProgress(Double progress);
    }

    public interface SectionListener {
        void didReceivedError(Status status, int dataIndex, int sectionIndex);

        void didReceivedData(byte[] data, int dataIndex, int sectionIndex);

        void didReceivedProgress(Double progress, int dataIndex, int sectionIndex);
    }

    public interface DataIndexErrorListener {
        void didReceivedError(Status status, int dataIndex);
    }

    public enum RequestMethod {
        GET, POST, POST_HEADER, GET_HEADER, SOAP_POST, POST_HEADER_NO_SEC, GET_HEADER_NO_SEC, DELETE_GET,DELETE_POST;
    }

    public enum State {
        NOT_DOWNLOAD, DOWNLOADING, DOWNLOADED;
    }

    public enum Status {
        IDEAL, SUCCESS, ERROR,
        HTTP_404, MALFORMED_URL, URISYNTAX_ERROR, UNSUPPORTED_ENCODING, ABRUPT, NULL;
    }

    public enum OutputData {
        TEXT, IMAGE, VIDEO, AUDIO
    }


    public SectionListener getSectionListener() {
        return sectionListener;
    }

    public void setSectionListener(SectionListener sectionListener) {
        this.sectionListener = sectionListener;
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }


    public AsyncLoaderNew(RequestMethod requestMethod, OutputData ouputType,
                          Context context) {

        status = Status.IDEAL;
        this.requestMethod = requestMethod;
        this.ouputType = ouputType;
        this.context = context;
    }

    // If % character is not found that means we need to perform escape of
    // the percentage characters
    // so that url is properly formatted. if percentage character is found
    // that means url string already contains
    // escape character so we ignore injecting the percentage character

    public void executeRequest(String requestURL) {

        try {
            requestURL = requestURL.trim();
            if (requestURL.indexOf("%", 0) == -1) {
                URL url = new URL(requestURL);
                URI uri = null;

                uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
                requestURL = String.valueOf(url);
            }

            //AppConstant.showErrorLog(TAG, "Input URL -> " + requestURL);

        } catch (URISyntaxException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("URISyntaxException", "e " + e);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("MalformedURLException", "e " + e+" URL"+requestURL);
        }

        //cancel(true);
        this.execute(requestURL);

    }

    public void executeRequest(String requestURL, int dataIndex) {

        this.dataIndex = dataIndex;
        try {
            requestURL = requestURL.trim();
            if (requestURL.indexOf("%", 0) == -1) {
                URL url = new URL(requestURL);
                URI uri = null;

                uri = new URI(url.getProtocol(), url.getUserInfo(),
                        url.getHost(), url.getPort(), url.getPath(),
                        url.getQuery(), url.getRef());
                url = uri.toURL();
                requestURL = String.valueOf(url);
            }

            //AppConstant.show(TAG, "Input URL -> " + requestURL);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //cancel(true);
        this.execute(requestURL);

    }

    public void executeRequest(String requestURL, int dataIndex, int sectionIndex) {

        this.dataIndex = dataIndex;
        this.sectionIndex = sectionIndex;

        try {
            requestURL = requestURL.trim();
            if (requestURL.indexOf("%", 0) == -1) {
                URL url = new URL(requestURL);
                URI uri = null;

                uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(),
                        url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
                requestURL = String.valueOf(url);
            }

            //AppConstant.show(TAG, "Section Input URL -> " + requestURL);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //cancel(true);
        this.execute(requestURL);

    }

    public byte[] convertInputStreamToByteArray(InputStream inputStream) {
        byte[] bytes = null;

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            byte data[] = new byte[1024];
            int count;

            while ((count = inputStream.read(data)) != -1) {
                bos.write(data, 0, count);
            }

            bos.flush();
            bos.close();
            inputStream.close();

            bytes = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }


    @Override
    protected byte[] doInBackground(String... params) {

        state = State.DOWNLOADING;

        serviceURL = params[0];
        if (ouputType == OutputData.TEXT) {
            try {
                /*CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509","BC");
                InputStream instream = context.getResources().openRawResource(R.raw.beta);
                X509Certificate cert = (X509Certificate) certificateFactory.generateCertificate(instream);
                String alias = cert.getSubjectX500Principal().getName();
                KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                trustStore.load(null);
                trustStore.setCertificateEntry(alias, cert);*/
//                trustEveryone();
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection1 = (HttpURLConnection) url.openConnection();
                /*HttpsURLConnection urlConnection1 = (HttpsURLConnection) url.openConnection();
                urlConnection1.setSSLSocketFactory(buildSslSocketFactory(context));*/
                if (!this.isCancelled()) {
                    try {

                        if (this.requestMethod.equals(RequestMethod.GET) || this.requestMethod.equals(RequestMethod.DELETE_GET)) {
                            if(this.requestMethod.equals(RequestMethod.DELETE_GET)){
                                urlConnection1.setRequestMethod("DELETE");
                            }else{
                                urlConnection1.setRequestMethod("GET");
                            }

                            urlConnection1.setDoOutput(false);
                            // Need to add security Signature
                            addSignatureHeaders(urlConnection1);
                        } else if (this.requestMethod.equals(RequestMethod.POST)) {
                            urlConnection1.setRequestMethod("POST");
                            // We need to set true when we are doing post else false
                            urlConnection1.setDoOutput(true);
                        } else if (this.requestMethod.equals(RequestMethod.POST_HEADER)  ||
                                this.requestMethod.equals(RequestMethod.DELETE_POST)) {
                            urlConnection1.setDoOutput(true);// We need to set true when we are doing post else false
                            if(this.requestMethod.equals(RequestMethod.DELETE_POST)){
                                Log.e("Asyncloader","DELETE_POST");
                                urlConnection1.setRequestMethod("DELETE");
                            }else{
                                urlConnection1.setRequestMethod("POST");
                            }
                            urlConnection1.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                            for (String key : getHeaders().keySet()) {
                                urlConnection1.setRequestProperty(key, getHeaders().get(key));
                            }
                            // Need to add security Signature
                            addSignatureHeaders(urlConnection1);

                            PrintWriter out = new PrintWriter(urlConnection1.getOutputStream());
                            out.print(this.requestMessage);
                            out.close();
                            InputStream in;
                            int httpResult = urlConnection1.getResponseCode();
                            if (httpResult >= HttpURLConnection.HTTP_OK)
                                in = new BufferedInputStream(urlConnection1.getInputStream());
                            else
                                in = new BufferedInputStream(urlConnection1.getErrorStream());

                            status = Status.SUCCESS;

                            return convertInputStreamToByteArray(in);
                        } else if (this.requestMethod.equals(RequestMethod.POST_HEADER_NO_SEC)) {
                            urlConnection1.setDoOutput(true);// We need to set true when we are doing post else false
                            urlConnection1.setRequestMethod("POST");
                            urlConnection1.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                            PrintWriter out = new PrintWriter(urlConnection1.getOutputStream());

                            out.print(this.requestMessage);
                            out.close();

                            InputStream in;
                            int httpResult = urlConnection1.getResponseCode();
                            if (httpResult >= HttpURLConnection.HTTP_OK)
                                in = new BufferedInputStream(urlConnection1.getInputStream());
                            else
                                in = new BufferedInputStream(urlConnection1.getErrorStream());

                            status = Status.SUCCESS;

                            return convertInputStreamToByteArray(in);
                        } else if (this.requestMethod.equals(RequestMethod.GET_HEADER_NO_SEC)) {
                            urlConnection1.setRequestMethod("GET");
                            urlConnection1.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                            urlConnection1.setDoOutput(false);
                        } else if (this.requestMethod.equals(RequestMethod.SOAP_POST)) {
                            urlConnection1.setRequestMethod("POST");
                            urlConnection1.setRequestProperty("X-API-KEY", "PARSE-REST-KEY");
                            urlConnection1.setRequestProperty("Content-Type", "application/json");
                            urlConnection1.setRequestProperty("Accept", "application/json");
                            Uri.Builder builder = new Uri.Builder();
                            for (String key : getHeaders().keySet()) {
                                builder.appendQueryParameter(key, getHeaders().get(key));
                            }
                            String query = builder.build().getEncodedQuery();
                            OutputStream os = urlConnection1.getOutputStream();
                            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(os, "UTF-8"));
                            writer.write(query);
                            writer.flush();
                            writer.close();
                            os.close();

                        }

                        int statusCode = urlConnection1.getResponseCode();
                        StringBuilder response = new StringBuilder();
                        AppConstant.showErrorLog("statusCode", "statusCode " + statusCode);
                        if (statusCode == HttpURLConnection.HTTP_OK) {
                            BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection1.getInputStream()));
                            String line;
                            while ((line = r.readLine()) != null) {
                                response.append(line);
                            }
                            AppConstant.showErrorLog(TAG, response.toString());
                            status = Status.SUCCESS;
                            return response.toString().getBytes();

                        } else {

                            status = Status.HTTP_404;
                            return null; //"Failed to fetch data!";
                        }

                    } catch (UnsupportedEncodingException e) {
                        status = Status.UNSUPPORTED_ENCODING;
                        status = Status.ERROR;
                        AppConstant.showErrorLog("exception", "" + e);
                    } catch (SocketTimeoutException e) {
                        e.printStackTrace();
                        state = State.NOT_DOWNLOAD;
                        status = Status.ERROR;
                        AppConstant.showErrorLog("exception", "" + e);
                    } catch (SocketException e) {
                        e.printStackTrace();
                        state = State.NOT_DOWNLOAD;
                        status = Status.ERROR;
                        AppConstant.showErrorLog("exception", "" + e);
                    } catch (IOException e) {
                        e.printStackTrace();
                        state = State.NOT_DOWNLOAD;
                        status = Status.ERROR;
                        AppConstant.showErrorLog("exception", "" + e);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        state = State.NOT_DOWNLOAD;
                        status = Status.ERROR;
                        AppConstant.showErrorLog("exception", "" + e);
                    }
                } else {
                    state = State.NOT_DOWNLOAD;
                    status = Status.ABRUPT;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                state = State.NOT_DOWNLOAD;
                status = Status.ERROR;
                AppConstant.showErrorLog("exception", "" + e);
            } catch (IOException e) {
                e.printStackTrace();
                state = State.NOT_DOWNLOAD;
                status = Status.ERROR;
                AppConstant.showErrorLog("exception", "" + e);
            } /*catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }*/
        } else if (ouputType == OutputData.IMAGE) {
            try {
                /*CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509","BC");
                InputStream instream = context.getResources().openRawResource(R.raw.beta);
                X509Certificate cert = (X509Certificate) certificateFactory.generateCertificate(instream);
                String alias = cert.getSubjectX500Principal().getName();
                KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                trustStore.load(null);
                trustStore.setCertificateEntry(alias, cer
        android:layout_marginLeft="@dimen/size_15dp"
        android:layout_marginTop="@dimen/size_10dp"
        android:layout_marginRight="@dimen/size_15dp"
        android:layout_marginBottom="@dimen/size_10dp"
        android:orientation="vertical">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginBottom="@dimen/size_8dp"
            android:background="@drawable/custom_border_rot);*/
//                trustEveryone();
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();
                AppConstant.showErrorLog("File Size", String.valueOf(fileLength));
                InputStream in = new BufferedInputStream(url.openStream());
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];
                double total = 0;
                /*int max = 1024;
                for (int i = 0; i < max; i++) {
                    double percentageCompleted = (i*100)/max;
                    publishProgress(percentageCompleted);
                }*/
                int n = 0;
                while (-1 != (n = in.read(buf))) {
                    // publishing the progress....
                    total += n;
                    if (fileLength > 0) { // only if total length is known
                        publishProgress((total * 100) / fileLength);
                    }
                    out.write(buf, 0, n);
                }
                out.close();
                in.close();
                byte[] response = out.toByteArray();
                status = Status.SUCCESS;
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                state = State.NOT_DOWNLOAD;
                status = Status.ERROR;
                AppConstant.showErrorLog("exception", "" + e);
            } catch (IOException e) {
                e.printStackTrace();
                state = State.NOT_DOWNLOAD;
                status = Status.ERROR;
                AppConstant.showErrorLog("exception", "" + e);
            } /*catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            }*/
            return new byte[0];
        }

        return new byte[0];
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);

        state = State.DOWNLOADED;
        //
        if (bytes == null) {
            status = Status.NULL;
        }
        AppConstant.showErrorLog(TAG, "status " + status + " url " + serviceURL);

        if (this.listener != null) {

            if (this.status.equals(Status.SUCCESS)) {
                this.listener.didReceivedData(bytes, dataIndex);
            } else {
                this.listener.didReceivedError(this.status, dataIndex);
            }

        }
        if (this.sectionListener != null) {
            if (this.status.equals(Status.SUCCESS)) {
                this.sectionListener.didReceivedData(bytes, dataIndex, sectionIndex);
            } else {
                this.sectionListener.didReceivedError(this.status, dataIndex, sectionIndex);
            }
        }


    }

    @Override
    protected void onProgressUpdate(Double... values) {
        super.onProgressUpdate(values);

        if (this.listener != null) {
            this.listener.didReceivedProgress(values[0]);
        }

        if (this.sectionListener != null) {
            this.sectionListener.didReceivedProgress(values[0], dataIndex, sectionIndex);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.listener != null) {
            this.listener.didReceivedError(this.status, dataIndex);
        }
        if (this.sectionListener != null) {
            this.sectionListener.didReceivedError(this.status, dataIndex, sectionIndex);
        }

    }

    private void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }

    // Signature generation methods for web services security

    private void addSignatureHeaders(HttpURLConnection urlConnection1) {
        Long tsLong = System.currentTimeMillis() / 1000;
        String timeStamp = tsLong.toString();
        String finalSignature = null;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String utcTimeStamp = format.format(new Date());
//        AppConstant.show("Utc Date", utcTimeStamp);
        try {
//            finalSignature = hmacSha1(timeStamp);
            finalSignature = hmacSha1(utcTimeStamp);
            // Security Headers values
//            urlConnection1.setRequestProperty("MobileNo", PreferenceManager.getStringForKey(context, "mobileNo", "na"));
//            urlConnection1.setRequestProperty("ImeiNo", PreferenceManager.getStringForKey(context, "imeiNo", "na"));
//            urlConnection1.setRequestProperty("TimeStamp", timeStamp);
//            urlConnection1.setRequestProperty("UTCTimeStamp", utcTimeStamp);
//            urlConnection1.setRequestProperty("UserTypeId", "" + PreferenceManager.getIntForKey(context, "userTypeID", -1));
//            urlConnection1.setRequestProperty("Signature", finalSignature);
            urlConnection1.setRequestProperty(AppConstant.HEADER_PARM_USER_ID, "" + PreferenceManager.getStringForKey(context,AppConstant.HEADER_PARM_USER_ID, AppConstant.DEFAULT_ZERO));
            urlConnection1.setRequestProperty(AppConstant.HEADER_PARM_USER_TOKEN, "" + PreferenceManager.getStringForKey(context, AppConstant.HEADER_PARM_USER_TOKEN,  AppConstant.DEFAULT_ZERO));


            Log.e(TAG,"userId="+PreferenceManager.getStringForKey(context,AppConstant.HEADER_PARM_USER_ID, AppConstant.DEFAULT_ZERO));
            Log.e(TAG,"user-token="+PreferenceManager.getStringForKey(context, AppConstant.HEADER_PARM_USER_TOKEN,  AppConstant.DEFAULT_ZERO));


//            AppConstant.show("request HMAC", " " + PreferenceManager.getStringForKey(context, "HMAC", "na"));
//            AppConstant.show("request URL", "" + serviceURL);
//            AppConstant.show("request UserId", "" + PreferenceManager.getIntForKey(context, "userID", -1));
//            AppConstant.show("request MobileNo", "" + PreferenceManager.getStringForKey(context, "mobileNo", "na"));
//            AppConstant.show("request imeiNo", "" + PreferenceManager.getStringForKey(context, "imeiNo", "na"));
//            AppConstant.show("request timeStamp", "" + timeStamp);
//            AppConstant.show("request userTypeID", "" + PreferenceManager.getIntForKey(context, "userTypeID", -1));
//            AppConstant.show("request Signature", " " + finalSignature);
//            AppConstant.show("request HMAC", " " + PreferenceManager.getStringForKey(context, "HMAC", "na"));

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

    }

    public static String hmacSha1(String timeStamp)
            throws UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException {
        String type = "HmacSHA1";

        // Value will be the combination of MobileNo + ImeiNo +  TimeStamp

        String mobileNo = PreferenceManager.getStringForKey(context, "mobileNo", "na");
        String imeiNo = PreferenceManager.getStringForKey(context, "imeiNo", "na");

        String signatureValue = mobileNo + imeiNo + timeStamp;
        String signatureKey = PreferenceManager.getStringForKey(context, "HMAC", "na");

        SecretKeySpec secret = new SecretKeySpec(signatureKey.getBytes(), type);
        Mac mac = Mac.getInstance(type);
        mac.init(secret);
        byte[] bytes = mac.doFinal(signatureValue.getBytes());
        return bytesToHex(bytes);
    }

    private final static char[] hexArray = "0123456789abcdef".toCharArray();

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
