package com.nimapchatapp.app.globel;

import android.content.Context;
import android.util.Log;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


import com.nimapchatapp.app.R;


public class ImageLoaderPicasso {

    public static void loadImage(final Context context, final CircularImageView imUserImage,
                                 final String imageLink) {
        try {

            Log.e("ImageLoaderPicasso","imageLink = "+imageLink);

            Picasso.with(context).setIndicatorsEnabled(false);
            Picasso.with(context)
                    .load(imageLink)
                    .resize(200,200)
//                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .placeholder(R.drawable.ic_user)
                    .into(imUserImage, new Callback() {
                        @Override
                        public void onSuccess() {

                            AppConstant.showErrorLog("onSuccess", "Image loaded successfully ");
                        }

                        @Override
                        public void onError() {

                            Log.e("ImageLoaderPicasso","onError imageLink = "+imageLink);

                            //Try again online if cache failed
                            Picasso.with(context).setIndicatorsEnabled(false);
                            Picasso.with(context)
                                    .load(imageLink)
                                    .error(R.drawable.ic_user)
                                    .into(imUserImage, new Callback() {
                                        @Override
                                        public void onSuccess() {

                                        }

                                        @Override
                                        public void onError() {
                                            Log.v("Picasso", "Could not fetch image");
                                        }
                                    });
                        }
                    });
        } catch (Exception e) {
            Log.e("ImageLoaderPicasso","catch imageLink = "+imageLink);
            e.printStackTrace();
        }
    }




}
