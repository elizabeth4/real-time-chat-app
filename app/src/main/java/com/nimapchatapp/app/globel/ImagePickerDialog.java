package com.nimapchatapp.app.globel;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.widget.TextView;
import com.nimapchatapp.app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImagePickerDialog extends BaseDialogFragment implements View.OnClickListener {

    private String TAG;
    private String message;
    private String firstActionTitle;
    private String secondActionTitle;
    private TextView messageTextView,firstActionTextView,secondActionTextView;

    public ImagePickerDialog() {

    }
    public static ImagePickerDialog newInstance(
            String tag, String message, String firstActionTitle, String secondActionTitle) {
        ImagePickerDialog imagePickerDialog = new ImagePickerDialog();

        Bundle bundle = new Bundle();

        bundle.putString("TAG", tag);
        bundle.putString("MESSAGES", message);
        bundle.putString("FIRST_BUTTON_TITLE", firstActionTitle);
        bundle.putString("SECOND_BUTTON_TITLE", secondActionTitle);

        imagePickerDialog.setArguments(bundle);

        return imagePickerDialog;
    }

    public interface InputActionListener {

        void dualPrimaryActionButtonPressed(String tag);

        void dualSecondaryActionButtonPressed(String tag);
    }

    private InputActionListener listener;

    public void setListener(InputActionListener listener) {
        this.listener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_image_picker, container);

        setData(view);
        setListener(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        this.setCancelable(true);

        //dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }


    @Override
    protected void setData(View view) {
        TAG = getArguments().getString("TAG");
        message = getArguments().getString("MESSAGES");
        firstActionTitle = getArguments().getString("FIRST_BUTTON_TITLE");
        secondActionTitle = getArguments().getString("SECOND_BUTTON_TITLE");

        messageTextView = view.findViewById(R.id.messages_text_view);
        messageTextView.setText(message);
        firstActionTextView = view.findViewById(R.id.first_action_text_view);
        firstActionTextView.setText(firstActionTitle);
        secondActionTextView = view.findViewById(R.id.second_action_text_view);
        secondActionTextView.setText(secondActionTitle);
    }

    @Override
    protected void setListener(View view) {
        firstActionTextView.setOnClickListener(this);
        secondActionTextView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
     switch (v.getId()){
         case R.id.first_action_text_view:
             if(listener !=null){
                 listener.dualPrimaryActionButtonPressed(AppConstant.TAG.CAMERA);
                 ImagePickerDialog.this.dismiss();
             }
             break;
         case R.id.second_action_text_view:
             if (listener != null){
                 listener.dualSecondaryActionButtonPressed(AppConstant.TAG.GALLERY);
                 ImagePickerDialog.this.dismiss();
             }
             break;
     }
    }

}
