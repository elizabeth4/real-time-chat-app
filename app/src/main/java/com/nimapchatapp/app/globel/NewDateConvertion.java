package com.nimapchatapp.app.globel;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class NewDateConvertion {

    public static String formateddate(String date) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(date);
        DateTime today = new DateTime();
        DateTime yesterday = today.minusDays(1);
        DateTime twodaysago = today.minusDays(2);
        DateTime tomorrow= today.minusDays(-1);

        if (dateTime.toLocalDate().equals(today.toLocalDate())) {
            return "Today ";
        } else if (dateTime.toLocalDate().equals(yesterday.toLocalDate())) {
            return "Yesterday ";
        } else if (dateTime.toLocalDate().equals(twodaysago.toLocalDate())) {
            return "2 days ago ";
        } else if (dateTime.toLocalDate().equals(tomorrow.toLocalDate())) {
            return "Tomorrow ";
        } else {
            return date;
        }
    }
}