package com.nimapchatapp.app.globel;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

abstract public class BaseAppCompatActivity extends AppCompatActivity {

    public static final String TAG = BaseAppCompatActivity.class.getSimpleName();
    protected abstract void setData();
    protected abstract void setListener();
    protected Context myContext;
    protected ProgressDialog progressAlert;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myContext = getApplicationContext();
    }

    public void showDialog() {
        if(progressAlert != null)
           // progressAlert.dismissAllowingStateLoss();
        progressAlert = null;
        progressAlert = ProgressDialog.newInstance("BaseAppCompatActivity", "", false);
        progressAlert.show(getSupportFragmentManager(), TAG);
    }

    public void hideDialog() {

        if(progressAlert != null && progressAlert.isVisible()){
            progressAlert.dismiss();
        }

    }
}
