package com.nimapchatapp.app.globel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.nimapchatapp.app.chatscreen.model.MessageModel;
import com.nimapchatapp.app.profilescreen.model.UserModel;
import com.nimapchatapp.app.recentchatuser.model.RecentChatUserModel;

import java.util.ArrayList;

public class WhooshhDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Contact.db";
    // friend field
//    public static final String FRIEND_TABLE_NAME = "friendContacts";
//    public static final String FRIEND_NAME = "name";
//    public static final String FRIEND_USER_ID = "userId";
//    public static final String FRIEND_MOBILE_NUMBER = "mobile";
//    public static final String FRIEND_IMAGE = "image";
//    public static final String FRIEND_THUMB_IMAGE = "thumb_image";
//    public static final String FRIEND_STATUS = "status";
//    // our mobile contact field
//    public static final String CONTACT_TABLE_NAME = "contacts";
//    public static final String CONTACT_MOBILE_NUMBER = "mobile";
//    public static final String CONTACT_NAME = "name";

    public static final String USER_TABLE_NAME = "users";

    public static final String ID = "id";
    public static final String USER_NAME = "name";
    public static final String USER_ID = "userId";
    public static final String USER_SERVER_ID = "serverUserId";
    public static final String USER_TOKEN = "serverUserToken";
    public static final String USER_CURRENT_USER = "currentUser";
    public static final String USER_MOBILE = "mobile";
    public static final String USER_COUNTRY_CODE = "country_code";
    public static final String USER_IMAGE = "image";
    public static final String USER_THUMB_IMAGE = "thumb_image";
    public static final String USER_OFFLINE_PATH = "offline_path";
    public static final String USER_DEVICE_TOKEN = "device_token";
    public static final String USER_LAST_SEEN_HIDE = "last_seen_hide";
    public static final String USER_AUTO_DOWNLOAD = "auto_download";
    public static final String USER_OS_TYPE = "os_type";
    public static final String USER_CREATED_DATE = "created_date";
    public static final String USER_MODIFIED_DATE = "modified_date";
    public static final String USER_STATUS = "status";

    public static String SELECT_USER_WITH_USERID = "SELECT * FROM users WHERE userId = ?";
    public static String GET_LOGIN_USER = "SELECT * FROM users WHERE currentUser = ?";

//     create table for users list
    String CREATE_USER_TABLE = "CREATE TABLE " + USER_TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + USER_NAME + " TEXT,"
            + USER_ID + " TEXT,"
            + USER_MOBILE + " TEXT,"
            + USER_COUNTRY_CODE + " TEXT,"
            + USER_IMAGE + " TEXT,"
            + USER_THUMB_IMAGE + " TEXT,"
            + USER_OFFLINE_PATH + " TEXT,"
            + USER_DEVICE_TOKEN + " TEXT,"
            + USER_LAST_SEEN_HIDE + " TEXT,"
            + USER_AUTO_DOWNLOAD + " TEXT,"
            + USER_SERVER_ID + " TEXT,"
            + USER_TOKEN + " TEXT,"
            + USER_CURRENT_USER + " TEXT,"
            + USER_OS_TYPE + " TEXT,"
            + USER_CREATED_DATE + " TEXT,"
            + USER_MODIFIED_DATE + " TEXT,"
            + USER_STATUS + " TEXT" + ")";

//    public static final String CONTACT_TABLE_NAME = "contacts";



    public static final String MESSAGE_TABLE_NAME = "messages";

    public static final String MESSAGE_ID = "server_message_id";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String MESSAGE_BODY = "message_body";
    public static final String MESSAGE_DATE = "message_date";
    public static final String MESSAGE_FROM = "message_from";
    public static final String MESSAGE_TO = "message_to";
    public static final String MESSAGE_GROUP_ID = "message_group_id";
    public static final String MESSAGE_SEEN = "message_seen";
    public static final String MESSAGE_IS_FIRST = "message_is_first";
    public static final String MESSAGE_IS_MULTIMEDIA_PATH = "message_multimedia_path";
    public static final String MESSAGE_IS_MULTIMEDIA_OFFLINE_PATH = "message_multimedia_offline_path";
    public static final String MESSAGE_IS_DOWNLOAD = "message_is_download";
    public static final String MESSAGE_IS_DELETED = "message_is_deleted";
    public static final String MESSAGE_IS_VIEW = "message_is_view";
    public static final String MESSAGE_SCHEDULE = "message_schedule";
    public static final String MESSAGE_SCHEDULE_DATE = "message_schedule_date";
    public static final String MESSAGE_CREATED_DATE = "message_created_date";
    public static final String MESSAGE_MODIFIED_DATE = "message_modified_date";


    public static final String RECENT_CHAT_USER_TABLE_NAME = "recent_chat_user";

    public static final String RECENT_CHAT_USER_ID = "user_id";
    public static final String RECENT_CHAT_USER_NAME = "user_name";
    public static final String RECENT_CHAT_USER_NUMBER = "user_number";
    public static final String RECENT_CHAT_USER_LAST_SEEN_FLAG = "last_seen_flag";
    public static final String RECENT_CHAT_USER_LAST_SEEN = "last_seen";
    public static final String RECENT_CHAT_USER_PROFILE_URL = "profile_url";
    public static final String RECENT_CHAT_USER_PROFILE_URL_OFFLINE_PATH = "offline_path";
    public static final String RECENT_CHAT_USER_MESSAGE_SERVER_ID = "message_id";
    public static final String RECENT_CHAT_USER_MESSAGE_TIME_STAMP = "message_time_stamp";
    public static final String RECENT_CHAT_USER_MESSAGE_TYPE = "message_type";
    public static final String RECENT_CHAT_USER_MESSAGE_TEXT = "message_text";
    public static final String RECENT_CHAT_USER_MESSAGE_VIEW_COUNT = "message_view_count";
    public static final String RECENT_CHAT_USER_MESSAGE_GROUP_ID = "message_group_id";
    public static final String RECENT_CHAT_USER_MESSAGE_GROUP_NAME = "message_group_name";
//    public static final String RECENT_CHAT_USER_MESSAGE_IS_GROUP = "message_is_group";
    public static final String RECENT_CHAT_USER_LAST_USER_NUMBER = "last_user_number";
    public static final String RECENT_CHAT_USER_LAST_USER_NAME = "last_user_name";

    public static final String CREATE_RECENT_CHAT_USER_TABLE = "CREATE TABLE " + RECENT_CHAT_USER_TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + RECENT_CHAT_USER_ID + " TEXT,"
            + RECENT_CHAT_USER_NAME + " TEXT,"
            + RECENT_CHAT_USER_NUMBER + " TEXT,"
            + RECENT_CHAT_USER_LAST_SEEN_FLAG + " TEXT,"
            + RECENT_CHAT_USER_LAST_SEEN + " TEXT,"
            + RECENT_CHAT_USER_PROFILE_URL + " TEXT,"
            + RECENT_CHAT_USER_PROFILE_URL_OFFLINE_PATH + " TEXT,"
            + RECENT_CHAT_USER_MESSAGE_SERVER_ID + " TEXT,"
            + RECENT_CHAT_USER_MESSAGE_TIME_STAMP + " TEXT,"
            + RECENT_CHAT_USER_MESSAGE_TYPE + " TEXT,"
            + RECENT_CHAT_USER_MESSAGE_TEXT + " TEXT,"
            + RECENT_CHAT_USER_MESSAGE_VIEW_COUNT + " TEXT,"
            + RECENT_CHAT_USER_MESSAGE_GROUP_ID + " TEXT,"
            + RECENT_CHAT_USER_MESSAGE_GROUP_NAME + " TEXT,"
//            + RECENT_CHAT_USER_MESSAGE_IS_GROUP + " TEXT,"
            + RECENT_CHAT_USER_LAST_USER_NUMBER + " TEXT,"
            + RECENT_CHAT_USER_LAST_USER_NAME + " TEXT" + ")";


    public static String SELECT_MESSAGES_WITH_SERVER_MESSAGE_ID = "SELECT * FROM messages WHERE server_message_id = ?";
    public static String DELETE_MESSAGES_WITH_SERVER_MESSAGE_ID = "DELETE FROM messages WHERE server_message_id = ?";
    public static String SELECT_RECENT_CHAT_USER_WITH_USER_ID = "SELECT * FROM recent_chat_user WHERE user_id = ?";
    public static String SELECT_RECENT_CHAT_USER_WITH_GROUP_ID = "SELECT * FROM recent_chat_user WHERE message_group_id = ?";
    public static String GET_GROUP_ID_WITH_SENDER_ID_AND_RECEIVER_ID = "SELECT * FROM messages WHERE message_from = ? AND message_to = ?";
    public static String GET_LAST_MESSAGE_WITH_SENDER_ID_RECEIVER_ID_AND_GROUP_ID = "SELECT * FROM messages WHERE message_from = ? AND message_to = ? AND message_group_id = ? ORDER BY id DESC LIMIT 1;";
    public static String GET_LAST_MESSAGE_WITH_GROUP_ID = "SELECT * FROM messages WHERE message_group_id = ? ORDER BY id DESC LIMIT 1;";
    public static String GET_ALL_MESSAGES_WITH_GROUP_ID = "SELECT * FROM messages WHERE  message_group_id = ? ORDER BY message_date ASC";
    public static String GET_LAST_MESSAGE_ID = "SELECT * FROM messages ORDER BY id DESC LIMIT 1;";
    public static String GET_FIRST_MESSAGE_ID = "SELECT * FROM messages ORDER BY id ASC LIMIT 1;";

    public static String GET_ALL_RECENT_CHAT_USER = "SELECT * FROM recent_chat_user ORDER BY message_time_stamp DESC";

    public static final String CREATE_MESSAGE_TABLE = "CREATE TABLE " + MESSAGE_TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + MESSAGE_ID + " TEXT,"
            + MESSAGE_TYPE + " TEXT,"
            + MESSAGE_BODY + " TEXT,"
            + MESSAGE_DATE + " TEXT,"
            + MESSAGE_FROM + " TEXT,"
            + MESSAGE_TO + " TEXT,"
            + MESSAGE_GROUP_ID + " TEXT,"
            + MESSAGE_SEEN + " TEXT,"
            + MESSAGE_IS_FIRST + " TEXT,"
            + MESSAGE_IS_MULTIMEDIA_PATH + " TEXT,"
            + MESSAGE_IS_MULTIMEDIA_OFFLINE_PATH + " TEXT,"
            + MESSAGE_IS_DOWNLOAD + " TEXT,"
            + MESSAGE_IS_DELETED + " TEXT,"
            + MESSAGE_IS_VIEW + " TEXT,"
            + MESSAGE_SCHEDULE + " TEXT,"
            + MESSAGE_SCHEDULE_DATE + " TEXT,"
            + MESSAGE_CREATED_DATE + " TEXT,"
            + MESSAGE_MODIFIED_DATE + " TEXT" + ")";

    private Context context;

    public WhooshhDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
        this.context = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        // create table for friend list
//        String CREATE_FRIEND_TABLE = "CREATE TABLE " + FRIEND_TABLE_NAME + "("
//                + FRIEND_NAME + " TEXT,"
//                + FRIEND_USER_ID + " TEXT,"
//                + FRIEND_MOBILE_NUMBER + " TEXT UNIQUE,"
//                + FRIEND_IMAGE + " TEXT,"
//                + FRIEND_THUMB_IMAGE + " TEXT,"
//                + FRIEND_STATUS + " TEXT" + ")";
//
//        // create table for mobile contact list
//        String CREATE_CONTACT_TABLE = "CREATE TABLE " + CONTACT_TABLE_NAME + "("
//                + CONTACT_MOBILE_NUMBER + " TEXT UNIQUE,"
//                + CONTACT_NAME + " TEXT" + ")";



//        db.execSQL(CREATE_FRIEND_TABLE);
//        db.execSQL(CREATE_CONTACT_TABLE);


        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_MESSAGE_TABLE);
        db.execSQL(CREATE_RECENT_CHAT_USER_TABLE);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed

//        db.execSQL("DROP TABLE IF EXISTS " + FRIEND_TABLE_NAME);
//        db.execSQL("DROP TABLE IF EXISTS " + CONTACT_TABLE_NAME);


        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MESSAGE_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + RECENT_CHAT_USER_TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

//    // code to add the new  friend contact
//    public void insertFriendContact(FriendListModel contact) {
//
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(FRIEND_NAME, contact.name);
//        values.put(FRIEND_USER_ID, contact.userId);
//        values.put(FRIEND_MOBILE_NUMBER, contact.mobileNumber);
//        values.put(FRIEND_IMAGE, contact.image);
//        values.put(FRIEND_THUMB_IMAGE, contact.thumb_image);
//        values.put(FRIEND_STATUS, contact.status);
//
//        // Inserting Row
//        db.insert(FRIEND_TABLE_NAME, null, values);
//        //2nd argument is String containing nullColumnHack
//        db.close(); // Closing database connection
//    }
//
//
//    // code to get all friend contacts in a list view
//    public ArrayList<FriendListModel> getAllFriendContacts() {
//
//        ArrayList<FriendListModel> contactList = new ArrayList<>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + FRIEND_TABLE_NAME;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                FriendListModel contact = new FriendListModel();
//                contact.name = cursor.getString(0);
//                contact.userId = cursor.getString(1);
//                contact.mobileNumber = cursor.getString(2);
//                contact.image = cursor.getString(3);
//                contact.thumb_image = cursor.getString(4);
//                contact.status = cursor.getString(5);
//                contact.iconHexColor = AppConstant.getRandomSubscriptionHexColor(context);
//                // Adding contact to list
//                contactList.add(contact);
//            } while (cursor.moveToNext());
//        }
//
//        // return contact list
//        return contactList;
//    }
//
//    // code to add the new contact
//    public void insertDeviceContact(ContactModel contact) {
//
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(CONTACT_MOBILE_NUMBER, contact.mobileNumber);
//        values.put(CONTACT_NAME, contact.name);
//
//        // Inserting Row
//        db.insert(CONTACT_TABLE_NAME, null, values);
//        //2nd argument is String containing nullColumnHack
//        db.close(); // Closing database connection
//    }
//    // code to get all contacts in a list view
//    public ArrayList<ContactModel> getAllDeviceContacts() {
//
//        ArrayList<ContactModel> contactList = new ArrayList<>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + CONTACT_TABLE_NAME;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                ContactModel contact = new ContactModel();
//                contact.mobileNumber = cursor.getString(0);
//                contact.name = cursor.getString(1);
//                contact.iconHexColor = AppConstant.getRandomSubscriptionHexColor(context);
//                // Adding contact to list
//                contactList.add(contact);
//            } while (cursor.moveToNext());
//        }
//
//        // return contact list
//        return contactList;
//    }



    public boolean isUserExit(String userId){

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{userId};
            Cursor cursor = db.rawQuery(SELECT_USER_WITH_USERID, params);
            Boolean result = false;
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    result = true;
                    cursor.moveToNext();
                }
                cursor.close();
            }
            db.close();
            return result;
        }catch (SQLiteException e){
            return false;
        }

    }



    public boolean insertUSER(UserModel model) {
        try{

            ContentValues values = new ContentValues();
            values.put(USER_NAME,model.name);
            values.put(USER_ID,model.userId);
            values.put(USER_MOBILE,model.mobileNumber);
            values.put(USER_COUNTRY_CODE,model.country_code);
            values.put(USER_IMAGE,model.image);
            values.put(USER_THUMB_IMAGE,model.thumb_image);
            values.put(USER_OFFLINE_PATH,model.offline_path);
            values.put(USER_DEVICE_TOKEN,model.device_token);
            values.put(USER_LAST_SEEN_HIDE,model.last_seen_hide);
            values.put(USER_AUTO_DOWNLOAD,model.auto_download);
            values.put(USER_OS_TYPE,model.os_type);
            values.put(USER_SERVER_ID,model.id);
            values.put(USER_TOKEN,model.userToken);
            values.put(USER_CURRENT_USER,model.isLoginUser ? "true" : "false");
            values.put(USER_OS_TYPE,model.os_type);
            values.put(USER_CREATED_DATE,model.created_date);
            values.put(USER_MODIFIED_DATE,model.modified_date);
            values.put(USER_STATUS,model.status);

            SQLiteDatabase db = this.getWritableDatabase();
            long pid = db.insert(USER_TABLE_NAME, null, values);
            db.close();

            return pid > 0;
        } catch (SQLiteException e) {
            return false;
        }
    }



    public boolean updateUser(UserModel model) {
        try{
            ContentValues values = new ContentValues();


            values.put(USER_NAME,model.name);
            values.put(USER_ID,model.userId);
            values.put(USER_MOBILE,model.mobileNumber);
            values.put(USER_COUNTRY_CODE,model.country_code);
            values.put(USER_IMAGE,model.image);
            values.put(USER_THUMB_IMAGE,model.thumb_image);
            values.put(USER_OFFLINE_PATH,model.offline_path);
            values.put(USER_DEVICE_TOKEN,model.device_token);
            values.put(USER_LAST_SEEN_HIDE,model.last_seen_hide);
            values.put(USER_AUTO_DOWNLOAD,model.auto_download);
            values.put(USER_OS_TYPE,model.os_type);
            values.put(USER_SERVER_ID,model.id);
            values.put(USER_TOKEN,model.userToken);
            values.put(USER_CURRENT_USER,model.isLoginUser ? "true" : "false");
            values.put(USER_CREATED_DATE,model.created_date);
            values.put(USER_MODIFIED_DATE,model.modified_date);
            values.put(USER_STATUS,model.status);


            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = USER_ID+" = ?";
            String[] whereArgs = new String[]{model.userId};
            long pid = db.update(USER_TABLE_NAME, values, whereClause, whereArgs);
            db.close();
            if (pid > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLiteException e){
            return false;
        }
    }


    public UserModel getUserWith(String userId) {
        UserModel userModel = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{userId};
            Cursor cursor = db.rawQuery(SELECT_USER_WITH_USERID, params);

            if (cursor.moveToFirst()) {

                while (!cursor.isAfterLast()) {

                    userModel = new UserModel();

                    userModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                    userModel.id = cursor.getString(cursor.getColumnIndex(USER_SERVER_ID));
                    userModel.userToken = cursor.getString(cursor.getColumnIndex(USER_TOKEN));
                    userModel.name = cursor.getString(cursor.getColumnIndex(USER_NAME));
                    userModel.userId = cursor.getString(cursor.getColumnIndex(USER_ID));
                    userModel.mobileNumber = cursor.getString(cursor.getColumnIndex(USER_MOBILE));
                    userModel.country_code = cursor.getString(cursor.getColumnIndex(USER_COUNTRY_CODE));
                    userModel.image = cursor.getString(cursor.getColumnIndex(USER_IMAGE));
                    userModel.thumb_image = cursor.getString(cursor.getColumnIndex(USER_THUMB_IMAGE));
                    userModel.offline_path = cursor.getString(cursor.getColumnIndex(USER_OFFLINE_PATH));
                    userModel.device_token = cursor.getString(cursor.getColumnIndex(USER_DEVICE_TOKEN));
                    userModel.last_seen_hide = cursor.getString(cursor.getColumnIndex(USER_LAST_SEEN_HIDE));
                    userModel.auto_download = cursor.getString(cursor.getColumnIndex(USER_AUTO_DOWNLOAD));
                    userModel.os_type = cursor.getString(cursor.getColumnIndex(USER_OS_TYPE));
                    userModel.created_date = cursor.getString(cursor.getColumnIndex(USER_CREATED_DATE));
                    userModel.modified_date = cursor.getString(cursor.getColumnIndex(USER_MODIFIED_DATE));
                    userModel.status = cursor.getString(cursor.getColumnIndex(USER_STATUS));
                    userModel.isLoginUser = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(USER_CURRENT_USER)));


                    cursor.moveToNext();
                }

                cursor.close();

            }

            db.close();

            return userModel;
        }catch (SQLiteException e){
            return userModel;
        }
    }



    public UserModel getLoginUser() {
        UserModel userModel = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{"true"};
            Cursor cursor = db.rawQuery(GET_LOGIN_USER, params);

            if (cursor.moveToFirst()) {

                while (!cursor.isAfterLast()) {

                    userModel = new UserModel();

                    userModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                    userModel.id = cursor.getString(cursor.getColumnIndex(USER_SERVER_ID));
                    userModel.userToken = cursor.getString(cursor.getColumnIndex(USER_TOKEN));
                    userModel.name = cursor.getString(cursor.getColumnIndex(USER_NAME));
                    userModel.userId = cursor.getString(cursor.getColumnIndex(USER_ID));
                    userModel.mobileNumber = cursor.getString(cursor.getColumnIndex(USER_MOBILE));
                    userModel.country_code = cursor.getString(cursor.getColumnIndex(USER_COUNTRY_CODE));
                    userModel.image = cursor.getString(cursor.getColumnIndex(USER_IMAGE));
                    userModel.thumb_image = cursor.getString(cursor.getColumnIndex(USER_THUMB_IMAGE));
                    userModel.offline_path = cursor.getString(cursor.getColumnIndex(USER_OFFLINE_PATH));
                    userModel.device_token = cursor.getString(cursor.getColumnIndex(USER_DEVICE_TOKEN));
                    userModel.last_seen_hide = cursor.getString(cursor.getColumnIndex(USER_LAST_SEEN_HIDE));
                    userModel.auto_download = cursor.getString(cursor.getColumnIndex(USER_AUTO_DOWNLOAD));
                    userModel.os_type = cursor.getString(cursor.getColumnIndex(USER_OS_TYPE));
                    userModel.created_date = cursor.getString(cursor.getColumnIndex(USER_CREATED_DATE));
                    userModel.modified_date = cursor.getString(cursor.getColumnIndex(USER_MODIFIED_DATE));
                    userModel.status = cursor.getString(cursor.getColumnIndex(USER_STATUS));
                    userModel.isLoginUser = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(USER_CURRENT_USER)));


                    cursor.moveToNext();
                }

                cursor.close();

            }

            db.close();

            return userModel;
        }catch (SQLiteException e){
            return userModel;
        }
    }





    public boolean isMessageExit(String serverMessageId){

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{serverMessageId};
            Cursor cursor = db.rawQuery(SELECT_MESSAGES_WITH_SERVER_MESSAGE_ID, params);
            Boolean result = false;
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    result = true;
                    cursor.moveToNext();
                }
                cursor.close();
            }
            db.close();
            return result;
        }catch (SQLiteException e){
            return false;
        }

    }


    public boolean deleteMessage(String serverMessageId){

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{serverMessageId};
            Cursor cursor = db.rawQuery(DELETE_MESSAGES_WITH_SERVER_MESSAGE_ID, params);
            Boolean result = false;
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    result = true;
                    cursor.moveToNext();
                }
                cursor.close();
            }
            db.close();
            return result;
        }catch (SQLiteException e){
            return false;
        }

    }



    public boolean insertMessage(MessageModel messageModel) {
        try{

            ContentValues values = new ContentValues();

            values.put(MESSAGE_ID,messageModel.id);
            values.put(MESSAGE_TYPE,messageModel.messageType);
            values.put(MESSAGE_BODY,messageModel.messageBody);
            values.put(MESSAGE_DATE,messageModel.messageDate);
            values.put(MESSAGE_FROM,messageModel.messageFrom);
            values.put(MESSAGE_TO,messageModel.messageTo);
            values.put(MESSAGE_GROUP_ID,messageModel.messageGroupId);
            values.put(MESSAGE_SEEN,messageModel.messageSeen);
            values.put(MESSAGE_IS_FIRST,messageModel.messageIsFirst);
            values.put(MESSAGE_IS_MULTIMEDIA_PATH,messageModel.messageMultimediaPath);
            values.put(MESSAGE_IS_MULTIMEDIA_OFFLINE_PATH,messageModel.messageMultimediaOfflinePath);
            values.put(MESSAGE_IS_DOWNLOAD,messageModel.messageIsDownload);
            values.put(MESSAGE_IS_DELETED,messageModel.messageIsDeleted);
            values.put(MESSAGE_IS_VIEW,messageModel.messageIsView);
            values.put(MESSAGE_SCHEDULE,messageModel.messageSchedule);
            values.put(MESSAGE_SCHEDULE_DATE,messageModel.messageScheduleDate);
            values.put(MESSAGE_CREATED_DATE,messageModel.messageCreatedDate);
            values.put(MESSAGE_MODIFIED_DATE,messageModel.messageModifiedDate);


            SQLiteDatabase db = this.getWritableDatabase();
            long pid = db.insert(MESSAGE_TABLE_NAME, null, values);
            db.close();



            return pid > 0;
        } catch (SQLiteException e) {
            return false;
        }
    }


    public boolean updateMessage(MessageModel messageModel) {
        try{
            ContentValues values = new ContentValues();


            values.put(ID,messageModel.Lid);
            values.put(MESSAGE_ID,messageModel.id);
            values.put(MESSAGE_TYPE,messageModel.messageType);
            values.put(MESSAGE_BODY,messageModel.messageBody);
            values.put(MESSAGE_DATE,messageModel.messageDate);
            values.put(MESSAGE_FROM,messageModel.messageFrom);
            values.put(MESSAGE_TO,messageModel.messageTo);
            values.put(MESSAGE_GROUP_ID,messageModel.messageGroupId);
            values.put(MESSAGE_SEEN,messageModel.messageSeen);
            values.put(MESSAGE_IS_FIRST,messageModel.messageIsFirst);
            values.put(MESSAGE_IS_MULTIMEDIA_PATH,messageModel.messageMultimediaPath);
            values.put(MESSAGE_IS_MULTIMEDIA_OFFLINE_PATH,messageModel.messageMultimediaOfflinePath);
            values.put(MESSAGE_IS_DOWNLOAD,messageModel.messageIsDownload);
            values.put(MESSAGE_IS_DELETED,messageModel.messageIsDeleted);
            values.put(MESSAGE_IS_VIEW,messageModel.messageIsView);
            values.put(MESSAGE_SCHEDULE,messageModel.messageSchedule);
            values.put(MESSAGE_SCHEDULE_DATE,messageModel.messageScheduleDate);
            values.put(MESSAGE_CREATED_DATE,messageModel.messageCreatedDate);
            values.put(MESSAGE_MODIFIED_DATE,messageModel.messageModifiedDate);


            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = MESSAGE_ID+" = ?";
            String[] whereArgs = new String[]{messageModel.id};
            long pid = db.update(MESSAGE_TABLE_NAME, values, whereClause, whereArgs);
            db.close();
            if (pid > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLiteException e){
            return false;
        }
    }


    public String getGroupIdFromMessageWith(String sender_id,String receiver_id) {

        String group_id = "-1";

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{sender_id,receiver_id};
            Cursor cursor = db.rawQuery(GET_GROUP_ID_WITH_SENDER_ID_AND_RECEIVER_ID, params);

            if (cursor!=null && cursor.getCount()>0) {
                    cursor.moveToLast();
                    group_id = cursor.getString(cursor.getColumnIndex(MESSAGE_GROUP_ID));
                    cursor.close();
            }
            db.close();
            return group_id;
        }catch (SQLiteException e){
            return group_id;
        }

    }


    public ArrayList<MessageModel> getMessagesWith(String group_id) {

       ArrayList<MessageModel> list = new ArrayList<>();

        MessageModel messageModel = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{group_id};
            Cursor cursor = db.rawQuery(GET_ALL_MESSAGES_WITH_GROUP_ID, params);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();

                do {

                    messageModel = new MessageModel();


                    messageModel.id = cursor.getString(cursor.getColumnIndex(MESSAGE_ID));
                    messageModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                    messageModel.messageType = cursor.getString(cursor.getColumnIndex(MESSAGE_TYPE));
                    messageModel.messageBody = cursor.getString(cursor.getColumnIndex(MESSAGE_BODY));
                    messageModel.messageDate = cursor.getString(cursor.getColumnIndex(MESSAGE_DATE));
                    messageModel.messageFrom = cursor.getString(cursor.getColumnIndex(MESSAGE_FROM));
                    messageModel.messageTo = cursor.getString(cursor.getColumnIndex(MESSAGE_TO));
                    messageModel.messageGroupId = cursor.getString(cursor.getColumnIndex(MESSAGE_GROUP_ID));
                    messageModel.messageSeen = cursor.getString(cursor.getColumnIndex(MESSAGE_SEEN));
                    messageModel.messageIsFirst = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_FIRST));
                    messageModel.messageMultimediaPath = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_MULTIMEDIA_PATH));
                    messageModel.messageMultimediaOfflinePath = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_MULTIMEDIA_OFFLINE_PATH));
                    messageModel.messageIsDownload = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_DOWNLOAD));
                    messageModel.messageIsDeleted = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_DELETED));
                    messageModel.messageIsView = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_VIEW));
                    messageModel.messageSchedule = cursor.getString(cursor.getColumnIndex(MESSAGE_SCHEDULE));
                    messageModel.messageScheduleDate = cursor.getString(cursor.getColumnIndex(MESSAGE_SCHEDULE_DATE));
                    messageModel.messageCreatedDate = cursor.getString(cursor.getColumnIndex(MESSAGE_CREATED_DATE));
                    messageModel.messageModifiedDate = cursor.getString(cursor.getColumnIndex(MESSAGE_MODIFIED_DATE));

                    list.add(messageModel);



                }while (cursor.moveToNext());



                cursor.close();
            }
            db.close();
            return list;
        }catch (SQLiteException e){
            return list;
        }

    }



    public MessageModel getMessageWith(String server_message_id) {

        MessageModel messageModel = null;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{server_message_id};
            Cursor cursor = db.rawQuery(SELECT_MESSAGES_WITH_SERVER_MESSAGE_ID,params);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();

                messageModel = new MessageModel();


                messageModel.id = cursor.getString(cursor.getColumnIndex(MESSAGE_ID));
                messageModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                messageModel.messageType = cursor.getString(cursor.getColumnIndex(MESSAGE_TYPE));
                messageModel.messageBody = cursor.getString(cursor.getColumnIndex(MESSAGE_BODY));
                messageModel.messageDate = cursor.getString(cursor.getColumnIndex(MESSAGE_DATE));
                messageModel.messageFrom = cursor.getString(cursor.getColumnIndex(MESSAGE_FROM));
                messageModel.messageTo = cursor.getString(cursor.getColumnIndex(MESSAGE_TO));
                messageModel.messageGroupId = cursor.getString(cursor.getColumnIndex(MESSAGE_GROUP_ID));
                messageModel.messageSeen = cursor.getString(cursor.getColumnIndex(MESSAGE_SEEN));
                messageModel.messageIsFirst = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_FIRST));
                messageModel.messageMultimediaPath = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_MULTIMEDIA_PATH));
                messageModel.messageMultimediaOfflinePath = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_MULTIMEDIA_OFFLINE_PATH));
                messageModel.messageIsDownload = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_DOWNLOAD));
                messageModel.messageIsDeleted = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_DELETED));
                messageModel.messageIsView = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_VIEW));
                messageModel.messageSchedule = cursor.getString(cursor.getColumnIndex(MESSAGE_SCHEDULE));
                messageModel.messageScheduleDate = cursor.getString(cursor.getColumnIndex(MESSAGE_SCHEDULE_DATE));
                messageModel.messageCreatedDate = cursor.getString(cursor.getColumnIndex(MESSAGE_CREATED_DATE));
                messageModel.messageModifiedDate = cursor.getString(cursor.getColumnIndex(MESSAGE_MODIFIED_DATE));



                cursor.close();
            }
            db.close();
            return messageModel;
        }catch (SQLiteException e){
            return messageModel;
        }

    }


    public MessageModel getLastMessageWithGroupId(String group_id) {

        MessageModel messageModel = null;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{group_id};
            Cursor cursor = db.rawQuery(GET_LAST_MESSAGE_WITH_GROUP_ID, params);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();
                messageModel = new MessageModel();


                messageModel.id = cursor.getString(cursor.getColumnIndex(MESSAGE_ID));
                messageModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                messageModel.messageType = cursor.getString(cursor.getColumnIndex(MESSAGE_TYPE));
                messageModel.messageBody = cursor.getString(cursor.getColumnIndex(MESSAGE_BODY));
                messageModel.messageDate = cursor.getString(cursor.getColumnIndex(MESSAGE_DATE));
                messageModel.messageFrom = cursor.getString(cursor.getColumnIndex(MESSAGE_FROM));
                messageModel.messageTo = cursor.getString(cursor.getColumnIndex(MESSAGE_TO));
                messageModel.messageGroupId = cursor.getString(cursor.getColumnIndex(MESSAGE_GROUP_ID));
                messageModel.messageSeen = cursor.getString(cursor.getColumnIndex(MESSAGE_SEEN));
                messageModel.messageIsFirst = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_FIRST));
                messageModel.messageMultimediaPath = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_MULTIMEDIA_PATH));
                messageModel.messageMultimediaOfflinePath = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_MULTIMEDIA_OFFLINE_PATH));
                messageModel.messageIsDownload = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_DOWNLOAD));
                messageModel.messageIsDeleted = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_DELETED));
                messageModel.messageIsView = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_VIEW));
                messageModel.messageSchedule = cursor.getString(cursor.getColumnIndex(MESSAGE_SCHEDULE));
                messageModel.messageScheduleDate = cursor.getString(cursor.getColumnIndex(MESSAGE_SCHEDULE_DATE));
                messageModel.messageCreatedDate = cursor.getString(cursor.getColumnIndex(MESSAGE_CREATED_DATE));
                messageModel.messageModifiedDate = cursor.getString(cursor.getColumnIndex(MESSAGE_MODIFIED_DATE));



                cursor.close();
            }
            db.close();
            return messageModel;
        }catch (SQLiteException e){
            return messageModel;
        }

    }



    public MessageModel getLastMessageWith(String sender_id,String receiver_id,String group_id) {

        MessageModel messageModel = null;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{sender_id,receiver_id,group_id};
            Cursor cursor = db.rawQuery(GET_LAST_MESSAGE_WITH_SENDER_ID_RECEIVER_ID_AND_GROUP_ID, params);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();
                messageModel = new MessageModel();


                messageModel.id = cursor.getString(cursor.getColumnIndex(MESSAGE_ID));
                messageModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                messageModel.messageType = cursor.getString(cursor.getColumnIndex(MESSAGE_TYPE));
                messageModel.messageBody = cursor.getString(cursor.getColumnIndex(MESSAGE_BODY));
                messageModel.messageDate = cursor.getString(cursor.getColumnIndex(MESSAGE_DATE));
                messageModel.messageFrom = cursor.getString(cursor.getColumnIndex(MESSAGE_FROM));
                messageModel.messageTo = cursor.getString(cursor.getColumnIndex(MESSAGE_TO));
                messageModel.messageGroupId = cursor.getString(cursor.getColumnIndex(MESSAGE_GROUP_ID));
                messageModel.messageSeen = cursor.getString(cursor.getColumnIndex(MESSAGE_SEEN));
                messageModel.messageIsFirst = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_FIRST));
                messageModel.messageMultimediaPath = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_MULTIMEDIA_PATH));
                messageModel.messageMultimediaOfflinePath = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_MULTIMEDIA_OFFLINE_PATH));
                messageModel.messageIsDownload = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_DOWNLOAD));
                messageModel.messageIsDeleted = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_DELETED));
                messageModel.messageIsView = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_VIEW));
                messageModel.messageSchedule = cursor.getString(cursor.getColumnIndex(MESSAGE_SCHEDULE));
                messageModel.messageScheduleDate = cursor.getString(cursor.getColumnIndex(MESSAGE_SCHEDULE_DATE));
                messageModel.messageCreatedDate = cursor.getString(cursor.getColumnIndex(MESSAGE_CREATED_DATE));
                messageModel.messageModifiedDate = cursor.getString(cursor.getColumnIndex(MESSAGE_MODIFIED_DATE));



                cursor.close();
            }
            db.close();
            return messageModel;
        }catch (SQLiteException e){
            return messageModel;
        }

    }

    public String getFirstMessageIdWith() {

        String message_id = "-1";

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(GET_FIRST_MESSAGE_ID, null);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();
                message_id = cursor.getString(cursor.getColumnIndex(MESSAGE_IS_FIRST));
                cursor.close();
            }
            db.close();
            return message_id;
        }catch (SQLiteException e){
            return message_id;
        }

    }


    public String getLastMessageIdWith() {

        String message_id = "-1";

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(GET_LAST_MESSAGE_ID, null);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();
                message_id = cursor.getString(cursor.getColumnIndex(MESSAGE_ID));
                cursor.close();
            }
            db.close();
            return message_id;
        }catch (SQLiteException e){
            return message_id;
        }

    }

    public String getLastMessageIdWith(String sender_id,String receiver_id,String group_id) {

        String message_id = "-1";

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{sender_id,receiver_id,group_id};
            Cursor cursor = db.rawQuery(GET_LAST_MESSAGE_WITH_SENDER_ID_RECEIVER_ID_AND_GROUP_ID, params);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();
                message_id = cursor.getString(cursor.getColumnIndex(MESSAGE_ID));
                cursor.close();
            }
            db.close();
            return message_id;
        }catch (SQLiteException e){
            return message_id;
        }
    }




    public boolean isRecentChatUserExit(String user_id){

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{user_id};
            Cursor cursor = db.rawQuery(SELECT_RECENT_CHAT_USER_WITH_USER_ID, params);
            Boolean result = false;
            if(cursor.getCount()>0) {
                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        result = true;
                        cursor.moveToNext();
                    }
                    cursor.close();
                }
            }
            db.close();
            return result;
        }catch (SQLiteException e){
            return false;
        }

    }




    public boolean insertRecentChatUser(RecentChatUserModel recentChatUserModel) {
        try{

            ContentValues values = new ContentValues();




            values.put(RECENT_CHAT_USER_ID,recentChatUserModel.userId);
            values.put(RECENT_CHAT_USER_NAME,recentChatUserModel.name);
            values.put(RECENT_CHAT_USER_NUMBER,recentChatUserModel.mobileNumber);
            values.put(RECENT_CHAT_USER_LAST_SEEN_FLAG,recentChatUserModel.lastSeenHideFlag);
            values.put(RECENT_CHAT_USER_LAST_SEEN,recentChatUserModel.lastSeenHide);
            values.put(RECENT_CHAT_USER_PROFILE_URL,recentChatUserModel.image);
            values.put(RECENT_CHAT_USER_PROFILE_URL_OFFLINE_PATH,recentChatUserModel.offline_path);
            values.put(RECENT_CHAT_USER_MESSAGE_SERVER_ID,recentChatUserModel.lastMessageId);
            values.put(RECENT_CHAT_USER_MESSAGE_TYPE,recentChatUserModel.lastMessageType);
            values.put(RECENT_CHAT_USER_MESSAGE_TIME_STAMP,recentChatUserModel.lastMessageTime);
            values.put(RECENT_CHAT_USER_MESSAGE_TEXT,recentChatUserModel.lastMessage);
            values.put(RECENT_CHAT_USER_MESSAGE_VIEW_COUNT,recentChatUserModel.unreadMessageCount);
            values.put(RECENT_CHAT_USER_MESSAGE_GROUP_ID,recentChatUserModel.groupId);
            values.put(RECENT_CHAT_USER_MESSAGE_GROUP_NAME,recentChatUserModel.groupName);
//            values.put(RECENT_CHAT_USER_MESSAGE_IS_GROUP,recentChatUserModel.isGroup);
            values.put(RECENT_CHAT_USER_LAST_USER_NUMBER,recentChatUserModel.lastMessageSenderNumber);
            values.put(RECENT_CHAT_USER_LAST_USER_NAME,recentChatUserModel.lastMessageSenderName);


            SQLiteDatabase db = this.getWritableDatabase();
            long pid = db.insert(RECENT_CHAT_USER_TABLE_NAME, null, values);
            db.close();



            return pid > 0;
        } catch (SQLiteException e) {
            return false;
        }
    }


    public boolean updateRecentChatUser(RecentChatUserModel recentChatUserModel) {
        try{
            ContentValues values = new ContentValues();


            values.put(ID,recentChatUserModel.Lid);
            values.put(RECENT_CHAT_USER_ID,recentChatUserModel.userId);
            values.put(RECENT_CHAT_USER_NAME,recentChatUserModel.name);
            values.put(RECENT_CHAT_USER_NUMBER,recentChatUserModel.mobileNumber);
            values.put(RECENT_CHAT_USER_LAST_SEEN_FLAG,recentChatUserModel.lastSeenHideFlag);
            values.put(RECENT_CHAT_USER_LAST_SEEN,recentChatUserModel.lastSeenHide);
            values.put(RECENT_CHAT_USER_PROFILE_URL,recentChatUserModel.image);
            values.put(RECENT_CHAT_USER_PROFILE_URL_OFFLINE_PATH,recentChatUserModel.offline_path);
            values.put(RECENT_CHAT_USER_MESSAGE_SERVER_ID,recentChatUserModel.lastMessageId);
            values.put(RECENT_CHAT_USER_MESSAGE_TYPE,recentChatUserModel.lastMessageType);
            values.put(RECENT_CHAT_USER_MESSAGE_TIME_STAMP,recentChatUserModel.lastMessageTime);
            values.put(RECENT_CHAT_USER_MESSAGE_TEXT,recentChatUserModel.lastMessage);
            values.put(RECENT_CHAT_USER_MESSAGE_VIEW_COUNT,recentChatUserModel.unreadMessageCount);
            values.put(RECENT_CHAT_USER_MESSAGE_GROUP_ID,recentChatUserModel.groupId);
            values.put(RECENT_CHAT_USER_MESSAGE_GROUP_NAME,recentChatUserModel.groupName);
//            values.put(RECENT_CHAT_USER_MESSAGE_IS_GROUP,recentChatUserModel.isGroup);
            values.put(RECENT_CHAT_USER_LAST_USER_NUMBER,recentChatUserModel.lastMessageSenderNumber);
            values.put(RECENT_CHAT_USER_LAST_USER_NAME,recentChatUserModel.lastMessageSenderName);



            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = RECENT_CHAT_USER_ID+" = ?";
            String[] whereArgs = new String[]{recentChatUserModel.userId};
            long pid = db.update(RECENT_CHAT_USER_TABLE_NAME, values, whereClause, whereArgs);
            db.close();
            if (pid > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLiteException e){
            return false;
        }
    }



    public boolean updateRecentChatUserWithMessage(MessageModel messageModel) {
        try{

            RecentChatUserModel recentChatUserModel = getRecentChatUserWithGroupId(messageModel.messageGroupId);

            if(recentChatUserModel!=null){

                ContentValues values = new ContentValues();


                values.put(ID,recentChatUserModel.Lid);
                values.put(RECENT_CHAT_USER_ID,recentChatUserModel.userId);
                values.put(RECENT_CHAT_USER_NAME,recentChatUserModel.name);
                values.put(RECENT_CHAT_USER_NUMBER,recentChatUserModel.mobileNumber);
                values.put(RECENT_CHAT_USER_LAST_SEEN_FLAG,recentChatUserModel.lastSeenHideFlag);
                values.put(RECENT_CHAT_USER_LAST_SEEN,recentChatUserModel.lastSeenHide);
                values.put(RECENT_CHAT_USER_PROFILE_URL,recentChatUserModel.image);
                values.put(RECENT_CHAT_USER_PROFILE_URL_OFFLINE_PATH,recentChatUserModel.offline_path);
                values.put(RECENT_CHAT_USER_MESSAGE_SERVER_ID,messageModel.id);
                values.put(RECENT_CHAT_USER_MESSAGE_TYPE,messageModel.messageType);
                values.put(RECENT_CHAT_USER_MESSAGE_TIME_STAMP,messageModel.messageDate);
                values.put(RECENT_CHAT_USER_MESSAGE_TEXT,messageModel.messageBody);
                values.put(RECENT_CHAT_USER_MESSAGE_VIEW_COUNT,recentChatUserModel.unreadMessageCount);
                values.put(RECENT_CHAT_USER_MESSAGE_GROUP_ID,recentChatUserModel.groupId);
                values.put(RECENT_CHAT_USER_MESSAGE_GROUP_NAME,recentChatUserModel.groupName);
//            values.put(RECENT_CHAT_USER_MESSAGE_IS_GROUP,recentChatUserModel.isGroup);
                values.put(RECENT_CHAT_USER_LAST_USER_NUMBER,recentChatUserModel.lastMessageSenderNumber);
                values.put(RECENT_CHAT_USER_LAST_USER_NAME,recentChatUserModel.lastMessageSenderName);



                SQLiteDatabase db = this.getWritableDatabase();
                String whereClause = RECENT_CHAT_USER_ID+" = ?";
                String[] whereArgs = new String[]{recentChatUserModel.userId};
                long pid = db.update(RECENT_CHAT_USER_TABLE_NAME, values, whereClause, whereArgs);
                db.close();
                if (pid > 0) {
                    return true;
                } else {
                    return false;
                }
            }else {
                return false;
            }



        } catch (SQLiteException e){
            return false;
        }
    }


    public RecentChatUserModel getRecentChatUserWithGroupId(String group_id) {

        RecentChatUserModel recentChatUserModel = null;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{group_id};
            Cursor cursor = db.rawQuery(SELECT_RECENT_CHAT_USER_WITH_GROUP_ID,params);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();

                recentChatUserModel = new RecentChatUserModel();


                recentChatUserModel.userId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_ID));
                recentChatUserModel.name = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_NAME));
                recentChatUserModel.mobileNumber = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_NUMBER));
                recentChatUserModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                recentChatUserModel.lastSeenHideFlag = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_SEEN_FLAG));
                recentChatUserModel.lastSeenHide = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_SEEN));
                recentChatUserModel.image = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_PROFILE_URL));
                recentChatUserModel.offline_path = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_PROFILE_URL_OFFLINE_PATH));
                recentChatUserModel.lastMessageId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_SERVER_ID));
                recentChatUserModel.lastMessageType = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TYPE));
                recentChatUserModel.lastMessageTime = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TIME_STAMP));
                recentChatUserModel.lastMessage = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TEXT));
                recentChatUserModel.unreadMessageCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_VIEW_COUNT)));
                recentChatUserModel.groupId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_GROUP_ID));
                recentChatUserModel.groupName = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_GROUP_NAME));
//                recentChatUserModel.isGroup = Integer.parseInt(cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_IS_GROUP)));
                recentChatUserModel.lastMessageSenderNumber = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_USER_NUMBER));
                recentChatUserModel.lastMessageSenderName = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_USER_NAME));

                recentChatUserModel.iconHexColor = AppConstant.getRandomSubscriptionHexColor(context);

                cursor.close();
            }
            db.close();
            return recentChatUserModel;
        }catch (SQLiteException e){
            return recentChatUserModel;
        }

    }



    public RecentChatUserModel getRecentChatUserWithUserId(String user_id) {

        RecentChatUserModel recentChatUserModel = null;

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String[] params = new String[]{user_id};
            Cursor cursor = db.rawQuery(SELECT_RECENT_CHAT_USER_WITH_USER_ID,params);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();

                recentChatUserModel = new RecentChatUserModel();


                recentChatUserModel.userId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_ID));
                recentChatUserModel.name = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_NAME));
                recentChatUserModel.mobileNumber = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_NUMBER));
                recentChatUserModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                recentChatUserModel.lastSeenHideFlag = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_SEEN_FLAG));
                recentChatUserModel.lastSeenHide = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_SEEN));
                recentChatUserModel.image = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_PROFILE_URL));
                recentChatUserModel.offline_path = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_PROFILE_URL_OFFLINE_PATH));
                recentChatUserModel.lastMessageId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_SERVER_ID));
                recentChatUserModel.lastMessageType = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TYPE));
                recentChatUserModel.lastMessageTime = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TIME_STAMP));
                recentChatUserModel.lastMessage = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TEXT));
                recentChatUserModel.unreadMessageCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_VIEW_COUNT)));
                recentChatUserModel.groupId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_GROUP_ID));
                recentChatUserModel.groupName = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_GROUP_NAME));
//                recentChatUserModel.isGroup = Integer.parseInt(cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_IS_GROUP)));
                recentChatUserModel.lastMessageSenderNumber = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_USER_NUMBER));
                recentChatUserModel.lastMessageSenderName = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_USER_NAME));

                recentChatUserModel.iconHexColor = AppConstant.getRandomSubscriptionHexColor(context);

                cursor.close();
            }
            db.close();
            return recentChatUserModel;
        }catch (SQLiteException e){
            return recentChatUserModel;
        }

    }


    public ArrayList<RecentChatUserModel> getRecentChatUsersWith() {


        ArrayList<RecentChatUserModel> list = new ArrayList<>();
        RecentChatUserModel recentChatUserModel = null;

        try {
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(GET_ALL_RECENT_CHAT_USER,null);

            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();

                do {

                    recentChatUserModel = new RecentChatUserModel();

                    recentChatUserModel.userId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_ID));
                    recentChatUserModel.name = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_NAME));
                    recentChatUserModel.mobileNumber = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_NUMBER));
                    recentChatUserModel.Lid = cursor.getString(cursor.getColumnIndex(ID));
                    recentChatUserModel.lastSeenHideFlag = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_SEEN_FLAG));
                    recentChatUserModel.lastSeenHide = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_SEEN));
                    recentChatUserModel.image = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_PROFILE_URL));
                    recentChatUserModel.offline_path = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_PROFILE_URL_OFFLINE_PATH));
                    recentChatUserModel.lastMessageId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_SERVER_ID));
                    recentChatUserModel.lastMessageType = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TYPE));
                    recentChatUserModel.lastMessageTime = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TIME_STAMP));
                    recentChatUserModel.lastMessage = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_TEXT));
                    recentChatUserModel.unreadMessageCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_VIEW_COUNT)));
                    recentChatUserModel.groupId = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_GROUP_ID));
                    recentChatUserModel.groupName = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_GROUP_NAME));
//                    recentChatUserModel.isGroup = Integer.parseInt(cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_MESSAGE_IS_GROUP)));
                    recentChatUserModel.lastMessageSenderNumber = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_USER_NUMBER));
                    recentChatUserModel.lastMessageSenderName = cursor.getString(cursor.getColumnIndex(RECENT_CHAT_USER_LAST_USER_NAME));

                    recentChatUserModel.iconHexColor = AppConstant.getRandomSubscriptionHexColor(context);

                    list.add(recentChatUserModel);


                }while (cursor.moveToNext());


                cursor.close();
            }
            db.close();
            return list;
        }catch (SQLiteException e){
            return list;
        }

    }





}