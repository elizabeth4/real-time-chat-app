package com.nimapchatapp.app.globel;

import android.os.Parcel;
import android.os.Parcelable;

public  class WMasterModel implements Parcelable {

    public String userId;
    public String name;
    public String mobileNumber;
    public String image;
    public String thumb_image;
    public String device_token;
    public String iconHexColor;
    public String status;

    public WMasterModel(){

    }

    protected WMasterModel(Parcel in) {
        name = in.readString();
        userId = in.readString();
        mobileNumber = in.readString();
        image = in.readString();
        thumb_image = in.readString();
        device_token = in.readString();
        iconHexColor = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WMasterModel> CREATOR = new Creator<WMasterModel>() {
        @Override
        public WMasterModel createFromParcel(Parcel in) {
            return new WMasterModel(in);
        }

        @Override
        public WMasterModel[] newArray(int size) {
            return new WMasterModel[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(userId);
        dest.writeString(mobileNumber);
        dest.writeString(image);
        dest.writeString(thumb_image);
        dest.writeString(device_token);
        dest.writeString(iconHexColor);
    }
}
