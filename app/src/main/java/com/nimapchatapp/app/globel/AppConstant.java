package com.nimapchatapp.app.globel;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.nimapchatapp.app.R;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class AppConstant {

    public static String LOGIN_ERROR_MESSAGE = "Please enter username and password";
    public static String OTP_ERROR_MESSAGE = "Please enter otp";
    public static String REGISTERATION = "Registration not successful";
    //public static String BASE_URL = "http://18.219.0.23/chatapp/api";
    public static String BASE_URL = "http://13.127.177.25/chatapp/api";  //-> live
    public static final String CHECK_USER_URL = "/check_user_registered";
    public static final String ADD_USER_URL = "/AddUser";
    public static final String EDIT_USER_URL = "/Update_profile/UpdateUser";
    public static final String SEND_MESSAGE_URL = "/Send_notification";
    public static final String GET_OLD_MESSAGES_URL = "/old_message";
    public static final String DELETE_MESSAGES_URL = "/delete_message";
    public static final String GET_RECENT_MESSAGES_URL = "/recent_user_list";
    public static final String GET_RECENT_CHAT_USER_URL = "/recent_user_list";
    public static String OS_TYPE = "A";
    public static String PURPOSE_DELETE_MESSGE = "Delete message";
    public static String PURPOSE_SEND_MESSGE = "Send message";
    public static String HEADER_PARM_USER_ID = "userId";
    public static String HEADER_PARM_USER_TOKEN = "user-token";
    public static String DEFAULT_ZERO = "0";
    public static String DEFAULT_ONE = "1";
    public static String DEFAULT_EMPTY = "";
    public static String PROFILE_INTENT_KEY = "profile_key";
    public static String USER_INTENT_KEY = "user_key";
    public static String EDIT_PROFILE = "edit";
    public static String ADD_PROFILE = "add";
    public static boolean DEFAULT_TRUE = true;
    public static boolean DEFAULT_FALSE = false;
    public static final int SUCCESS                  =   1;
    public static final int CONNECTION_ERROR         =   2;
    public static final int PROCESSING_ERROR         =   3;
    public static final int NO_FRIEND_AVAILABLE      =   3;
    public static final int User_Not_Registered      =   4;

    public static final int COMMING_FROM_REAL_TIME       =   100;
    public static final int COMMING_FROM_NORMAL_MESSAGE  =   101;


    public static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    public static final int REQUEST_PERMISSION_SETTING = 101;

    public static final String CONNECTION_ERROR_MESSAGE     = "Connection Error";
    public static final String SUCCESSFULLY_UPDATED_MESSAGE     = "Successfully updated";
    public static final String EDIT_PROFILE_SUCCESSFULLY_UPDATED_MESSAGE     = "Profile updated successfully.";
    public static final String PROCESSING_ERROR_MESSAGE     = "Something Went Wrong!!";
    public static final String REGISTER_USER_MESSAGE     = "Register User.";
    public static final String UNREGISTER_USER_MESSAGE     = "Unregister User.";
    public static final String NO_FRIEND_AVAILABLE_MESSAGE  = "No friend available";
    public static final String NO_CONTACTS_FOUND_MESSAGE  = "No contacts found";
    public static final String NO_RECENT_CHAT_USER_MESSAGE  = "No recent chat";
    public static final String NO_COUNTRY_CODE_AVAILABLE_MESSAGE  = "Country code not available";
    public static final String CAMERA_MESSAGE = "You can choose image from camera or gallery";
    public static final String INVITE_MESSAGE = "User is not registered.\n" +"Send an invitation?";


    public static String EXTRA_ROOM_NAME="EXTRA_ROOM_NAME";

    public static String convertTime(long timestamp) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("HH:mm");
        Date date = new Date(timestamp);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(date);
    }

    public interface PREFERENCE_TAG{
        String CODE_SENT = "codeSent";
        String USERNAME = "name";
        String USER_ID = "userId";
        String COUNTRY_CODE = "country_code";
        String MOBILE = "mobile";
        String DEVICE_TOKEN = "device_token";
        String IMAGE = "image";
        String STATUS = "status";
        String LAST_SEEN = "last_seen";
        String AUTO_DOWNLOAD = "auto_download";
//        String FCM_ID = "fcm_id";
        String OS_TYPE = "os_type";
        String THUMB_IMAGE = "thumb_image";
        String REAL_TIME_CHAT_SENDER_ID = "sender";
        String REAL_TIME_CHAT_RECEIVER_ID = "receiver";
    }
    public interface PARSER_TAG{


        String ID = "id";
        String CREATED_DATE = "created_date";
        String MODIFIED_DATE = "modified_date";
        String USER_TOKEN = "user_token";
        String SENDER_NAME="sendername";


        String USERNAME = "name";
        String USER_ID = "userId";
        String MOBILE = "mobile";
        String COUNTRY_CODE = "country_code";
        String DEVICE_TOKEN = "device_token";
        String IMAGE = "image";
        String STATUS = "Status";
        String STATUS_LOWER = "status";
        String USER_STATUS = "status";
        String STATUS_CODE = "status_code";
        String LAST_SEEN = "last_seen_hide";
        String AUTO_DOWNLOAD = "auto_download";
        String IS_IMAGE_CHANGE = "isImageChange";
//        String FCM_ID = "fcm_id";
        String OS_TYPE = "os_type";
        String THUMB_IMAGE = "thumb_image";

        String LIST_OF_FRIENDS = "friends";
        String LIST_OF_CONTACTS = "contacts";

        String ISO = "iso";
        String PHONE_CODE = "phonecode";

        String DATA = "Data";

        String RECEIVER_SERVER_ID = "Recentuser_serverid";
        String RECEIVER_ID_R = "Recentuser_id";
        String RECEIVER_USER_TOKEN = "Recentuser_user_token";
        String RECEIVER_NAME = "Recentuser_Name";
        String RECEIVER_NUMBER = "Recentuser_number";
        String RECEIVER_COUNTRY_CODE = "Recentuser_country_code";
        String RECEIVER_LAST_SEEN = "Recentuser_last_seen";
        String RECEIVER_AUTO_DOWNLOAD = "Recentuser_auto_download";
        String RECEIVER_OS_TYPE = "Recentuser_os_type";
        String RECEIVER_CREATED_DATE = "Recentuser_created_date";
        String RECEIVER_MODIFIED_DATE = "Recentuser_modified_date";
        String RECEIVER_IMAGE = "Recentuser_image";
        String RECEIVER_STATUS = "Recentuser_status";
        String RECEIVER_THUMB_IMAGE = "Recentuser_thumb_image";
        String RECEIVER_DEVICE_TOKEN = "Recentuser_device_token";
        String LAST_USER_NAME = "lastuser_Name";
        String LAST_USER_NUMBER = "lastuser_number";


        String GROUP_ID = "group_id";
        String GROUP_NAME = "groupName";
        String MESSAGE_TYPE = "message_type";
        String MESSAGE = "message";
        String MESSAGE_FROM = "message_from";
        String MESSAGE_TO = "message_to";
        String SEEN = "seen";
        String IS_FIRST = "is_first";
        String MULTIMEDIA_PATH = "multimedia_path";
        String IS_DOWNLOADED = "is_downloaded";
        String IS_DELETED = "is_deleted";
        String IS_SCHEDULED = "is_scheduled";
        String SCHEDULED_DATE = "is_scheduled";
        String MESSAGE_BODY = "message_body";
        String SENDER_ID = "sender_id";
        String RECEIVER_ID = "receiver_id";
        String MESSAGE_ID = "message_id";
        String NOTIFICATION_PURPOSE = "purpose";
        String NOTIFICATION_BODY = "body";
    }

    public interface MESSAGES_TYPE{

        String TEXT = "text";
        String IMAGE = "image";
        String contact = "contact";
        String AUDIO = "audio";
        String DOCUMENT = "document";

    }

    public interface TAG{

        String CAMERA = "camera";
        String YES = "yes";
        String NO = "no";
        String GALLERY = "gallery";
        String DEFAULT = "default";
        String REAL_TIME_SENDER = "sender";
        String REAL_TIME_RECEIVER = "receiver";

    }

    public static void showToast(Context context,String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public static void showErrorLog(String tag,String messages){
        Log.e(tag,messages);

    }
    private static String removeSpecialCharsFromString(String string) {
        return string.replaceAll("[^a-zA-Z0-9]", "A");
    }

    public static String getInitials(String string) {
        String removeSpecialChar = removeSpecialCharsFromString(string);
        String[] strArray = removeSpecialChar.split("\\s+");
        String capsString = "";
        int count = 0;

        for (int j = 0; j < strArray.length; j++) {
            String str = strArray[j];

            if (!(str.equals("&") || str.trim().toLowerCase().equals("and"))) {
                count++;
                capsString += str.substring(0, 1);
            }

            if (count >= 2) {
                break;
            }
        }

        return capsString.toUpperCase();
    }

    public static String getRandomSubscriptionHexColor(Context context) {

        String colorArray[] = context.getResources().getStringArray(R.array.colorCode);
        if (colorArray.length > 0) {
            int colorId = (int) (Math.random() * (colorArray.length));
            return colorArray[colorId];
        } else {
            return "#80C783";
        }

    }

    public static String getBase64Image(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    public static String getTime(long milliSeconds)

    {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(milliSeconds);

        String date = c.get(Calendar.YEAR)+"-"+c.get(Calendar.MONTH)+"-"+c.get(Calendar.DAY_OF_MONTH);
        String time = c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);

        System.out.println(date + " " + time);

        return time;
    }

                public static void inviteFriend(Context context) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "https://play.google.com/store/apps/details?id=com.nimaptxtme.app";
                String shareSub = "Your subject here";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                Intent new_intent = Intent.createChooser(sharingIntent, "Share using");
                new_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                new_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(new_intent);
            }







}
