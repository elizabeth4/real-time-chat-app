package com.nimapchatapp.app.invite.controller;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.chatscreen.controller.ChatActivity;
import com.nimapchatapp.app.contactscreen.model.ContactModel;
import com.nimapchatapp.app.globel.*;
import com.nimapchatapp.app.invite.model.CheckUserParser;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class InviteUserActivity extends BaseAppCompatActivity implements FetchContacts.ContactListener,
        CheckUserParser.CheckUserListener,
        ImagePickerDialog.InputActionListener
{


    private ArrayList<ContactModel> contactList;
    private ArrayList<ContactModel> contactListFiltered;
    private RecyclerView recyclerView;
    private InviteUserActivity.ContactListAdapter adapter;
    private Toolbar mToolbar;
    private WhooshhDB db;
    private TextView errorTextView;
    public static final int REQUEST_READ_CONTACTS = 100;
    private SearchView searchView;
    private ImagePickerDialog invitePermissionDialog;
    private String phoneNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_invite_user);
        phoneNumber= PreferenceManager.getStringForKey(InviteUserActivity.this, AppConstant.PREFERENCE_TAG.MOBILE, phoneNumber);

        db = new WhooshhDB(myContext);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            getAllContactList();
        } else {
            requestContactPermission();
        }


        setData();
        setListener();
    }

        private void getAllContactList(){
        showDialog();
        FetchContacts contacts = new FetchContacts(myContext,phoneNumber);
        contacts.setContactListener(this);
        contacts.execute();

    }

    protected void requestContactPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.READ_CONTACTS)) {
            // show UI part if you want here to show some rationale !!!

        } else {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getAllContactList();

                } else {
                    requestContactPermission();
                    // permission denied,Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }


    private void showInvitePermissionDialog() {

        invitePermissionDialog = ImagePickerDialog.newInstance(TAG, AppConstant.INVITE_MESSAGE, AppConstant.TAG.YES, AppConstant.TAG.NO);
        invitePermissionDialog.setListener(this);
        invitePermissionDialog.show(getSupportFragmentManager(), TAG);

    }

    @Override
    protected void setData() {
        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Contact List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(myContext));

        errorTextView = findViewById(R.id.error_text_view);

//        hideAndShowView(contactList);

    }




    @Override
    protected void setListener() {

    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new InviteUserActivity.ContactListAdapter();
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }
        else if(id == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.search_menu, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));




        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                AppConstant.showErrorLog(TAG, s);
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                AppConstant.showErrorLog(TAG, s);
                return false;
            }
        });


        return true;
    }

    @Override
    public void didReceivedContactResult(int intCode, ArrayList<ContactModel> list) {
        hideDialog();
        contactList = new ArrayList<>();

        AppConstant.showErrorLog(TAG, "ListContact :"+list.size());

        for(int i=0; i< list.size(); i++  ){

            if(checkIsNumberAvailableInList(list.get(i).mobileNumber)){
                ContactModel model = new ContactModel();
                model.name = list.get(i).name;
                model.mobileNumber = list.get(i).mobileNumber;
                model.iconHexColor = list.get(i).iconHexColor;

                contactList.add(model);
            }
        }
        contactListFiltered = contactList;

        AppConstant.showErrorLog(TAG, "ListContact :"+contactList.size());

        hideAndShowView(contactList);


    }
    private boolean checkIsNumberAvailableInList(String mobileNumber){

        for(int i=0; i< contactList.size(); i++ ){
            if(contactList.get(i).mobileNumber.equals(mobileNumber)){
                return false;
            }
        }

        return true;
    }
    private void hideAndShowView(ArrayList<ContactModel> contactList) {
        if (contactList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText(AppConstant.NO_CONTACTS_FOUND_MESSAGE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            errorTextView.setVisibility(View.GONE);
            setAdapter();
        }
    }


        private void checkUser(String number){
        showDialog();
            CheckUserParser checkUserParser = new CheckUserParser(myContext);
            checkUserParser.setListener(this);

            checkUserParser.checkUserWith(number,AppConstant.DEFAULT_TRUE);

    }

    @Override
    public void didReceivedCheckUserResult(int resultCode, String userId) {
        hideDialog();
        if (resultCode == AppConstant.SUCCESS) {
//            AppConstant.showToast(myContext, AppConstant.REGISTER_USER_MESSAGE);

            moveToChatActivity(userId);
        }
        else if (resultCode == AppConstant.User_Not_Registered) {
//            AppConstant.showToast(myContext, AppConstant.UNREGISTER_USER_MESSAGE);
            showInvitePermissionDialog();
        } else if (resultCode == AppConstant.CONNECTION_ERROR) {
            AppConstant.showToast(myContext, AppConstant.CONNECTION_ERROR_MESSAGE);
        }
    }

    private void moveToChatActivity(String userID) {

        startActivity(new Intent(InviteUserActivity.this,ChatActivity.class).putExtra(AppConstant.USER_INTENT_KEY,userID));
    }

    @Override
    public void dualPrimaryActionButtonPressed(String tag) {

        invitePermissionDialog.dismiss();

        if(adapter !=null){
            AppConstant.inviteFriend(myContext);
        }

    }

    @Override
    public void dualSecondaryActionButtonPressed(String tag) {
        invitePermissionDialog.dismiss();
    }

    class ContactListAdapter extends RecyclerView.Adapter<InviteUserActivity.ContactListAdapter.ViewHolder> implements Filterable {


        @NonNull
        @Override
        public InviteUserActivity.ContactListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_contact_list, viewGroup, false);
            return new InviteUserActivity.ContactListAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull InviteUserActivity.ContactListAdapter.ViewHolder viewHolder, int i) {
            viewHolder.bindData(contactListFiltered);
        }

        @Override
        public int getItemCount() {
            return contactListFiltered.size();
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        contactListFiltered = contactList;
                    } else {
                        ArrayList<ContactModel> filteredList = new ArrayList<>();
                        for (ContactModel row : contactList) {
                            // here we are looking for name or phone number match
                            if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.mobileNumber.contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }
                        contactListFiltered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = contactListFiltered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    contactListFiltered = (ArrayList<ContactModel>) filterResults.values;

                    hideAndShowView(contactListFiltered);
                }
            };
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView nameTextView, numberTextView, tvInitialName, inviteTextView;

            ViewHolder(@NonNull View itemView) {
                super(itemView);
                nameTextView = itemView.findViewById(R.id.tv_name);
                numberTextView = itemView.findViewById(R.id.tv_number);
                tvInitialName = itemView.findViewById(R.id.tv_initial_name);
                inviteTextView = itemView.findViewById(R.id.invite_text_view);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        if(contactListFiltered.get(getAdapterPosition()).isFriend){
//                            Intent intent = new Intent(myContext,ChatActivity.class);
//                            Gson gson = new Gson();
//                            String myJson = gson.toJson(getSelectedModel(getAdapterPosition()));
//                            intent.putExtra("ChatWith",myJson);
//
//                            startActivity(intent);
//                        }else {
////                            inviteFriend();
//                            checkUser(contactListFiltered.get(getAdapterPosition()).mobileNumber);
//                        }

                        checkUser(contactListFiltered.get(getAdapterPosition()).mobileNumber);
                    }
                });
            }

            private void bindData(ArrayList<ContactModel> list) {

                ContactModel contact = list.get(getAdapterPosition());

                nameTextView.setText(contact.name);
                tvInitialName.setText(AppConstant.getInitials(contact.name));
                numberTextView.setText(list.get(getAdapterPosition()).mobileNumber);

                Drawable drawable = getResources().getDrawable(R.drawable.circuler_text_view);
                drawable.setColorFilter(Color.parseColor(contact.iconHexColor), PorterDuff.Mode.SRC_ATOP);
                tvInitialName.setBackground(drawable);

                if (contact.isFriend) {
                    inviteTextView.setBackgroundResource(R.drawable.ic_logo);
                } else {
                    inviteTextView.setBackgroundResource(R.drawable.button_design);
                }

            }

//            private void inviteFriend() {
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                String shareBody = "https://play.google.com/store/apps/details?id=com.scorp.who";
//                String shareSub = "Your subject here";
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//                startActivity(Intent.createChooser(sharingIntent, "Share using"));
//            }

            private ContactModel getSelectedModel(int position){

                return contactListFiltered.get(position);
            }

        }
    }


    @Override
    public void onBackPressed() {

        if(searchView!=null){
            if (!searchView.isIconified()) {
                searchView.setIconified(true);
                super.onBackPressed();
            } else {
                super.onBackPressed();
            }
        }else {
            super.onBackPressed();
        }

    }
}
