package com.nimapchatapp.app.invite.model;

import android.content.Context;
import android.os.AsyncTask;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.AsyncLoaderNew;
import com.nimapchatapp.app.globel.WhooshhDB;
import com.nimapchatapp.app.profilescreen.model.UserModel;
import org.json.JSONException;
import org.json.JSONObject;


public class CheckUserParser extends AsyncTask<Void, Void, Integer> implements AsyncLoaderNew.Listener {



    private String TAG = "CheckUserParser";

    // Variable Declarations
    private AsyncLoaderNew asyncLoader;
    private Context context;
    private String responseString;
    private WhooshhDB db;
    private String userID="";
    private boolean invite_flag;


    // Callbacks for the initiator
    public interface CheckUserListener {

        void didReceivedCheckUserResult(int resultCode,String userId);
    }

    private CheckUserParser.CheckUserListener listener;

    public void setListener(CheckUserParser.CheckUserListener listener) {
        this.listener = listener;
    }

    public CheckUserParser(Context context) {
        super();
        this.context = context;
    }


    public void checkUserWith(String number,boolean invite_flag){


        this.invite_flag = invite_flag;

        if(number.contains("-")){
            number = number.replace("-","").trim();
        }

        if(number.contains("+")){
            number = number.replace("+","").trim();
        }

        if(number.contains(" ")){
            number = number.replace(" ","").trim();
        }

        asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER, AsyncLoaderNew.OutputData.TEXT, context);
        asyncLoader.setListener(this);
        JSONObject jsonObject = new JSONObject();

        db = new WhooshhDB(context);


        try {

            // We need set the user type to always vendor
            jsonObject.accumulate(AppConstant.PARSER_TAG.MOBILE, number);



        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 4. convert JSONObject to JSON to String
        String jsonString = jsonObject.toString();
        AppConstant.showErrorLog("CheckUserParser", "jsonString " + jsonString);
        asyncLoader.requestMessage = jsonString;
        asyncLoader.requestActionMethod = "";
        asyncLoader.executeRequest(AppConstant.BASE_URL + AppConstant.CHECK_USER_URL);
    }


    @Override
    public void didReceivedError(AsyncLoaderNew.Status status, int dataIndex) {
        if (listener != null) {
            listener.didReceivedCheckUserResult(AppConstant.CONNECTION_ERROR,userID);
        }
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {

        responseString = new String(data);
        // we need to call the background thread to process the data.
        AppConstant.showErrorLog("CheckUserParser", "responseString " + responseString);
        asyncLoader = null;
        execute();

    }

    @Override
    public void didReceivedProgress(Double progress) {

    }

    @Override
    protected Integer doInBackground(Void... voids) {
        try {
            db = new WhooshhDB(context);
            JSONObject result = new JSONObject(responseString);

            int Status = result.getInt("Status");

            if (Status == 200 ) {

                //WhooshhDB db = new WhooshhDB(context);


                // It is successful response from the server we can process the json file
                JSONObject userObject = result.getJSONObject(AppConstant.PARSER_TAG.DATA);

                UserModel userModel = null;
                if(invite_flag){
                    userModel = new UserModel(userObject,AppConstant.DEFAULT_FALSE,AppConstant.DEFAULT_ZERO);
                }else {
                    userModel = new UserModel(userObject,AppConstant.DEFAULT_TRUE,AppConstant.DEFAULT_ZERO);
                }



                if(db.isUserExit(userModel.userId)){

                    userModel.Lid = db.getUserWith(userModel.userId).Lid;
                    db.updateUser(userModel);
                }
                else {
                    db.insertUSER(userModel);
                }

                userModel = db.getUserWith(userModel.userId);
                userID = userModel.userId;

                return AppConstant.SUCCESS;

            } else if (Status == 201) {
                return AppConstant.User_Not_Registered;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("JSONException", "JSONException " + e);
            cancel(true);
        }

        return AppConstant.PROCESSING_ERROR;
    }


    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        int resultCode = integer.intValue();

        if (listener != null) {
            if (resultCode == AppConstant.SUCCESS) {
                listener.didReceivedCheckUserResult(AppConstant.SUCCESS,userID);
            }
            else if(resultCode == AppConstant.User_Not_Registered){
                listener.didReceivedCheckUserResult(AppConstant.User_Not_Registered,userID);
            }else {
                listener.didReceivedCheckUserResult(AppConstant.PROCESSING_ERROR,userID);
            }
        }
    }
}
