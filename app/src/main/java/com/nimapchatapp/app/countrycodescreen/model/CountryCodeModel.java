package com.nimapchatapp.app.countrycodescreen.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.WMasterModel;
import org.json.JSONException;
import org.json.JSONObject;

public class CountryCodeModel extends WMasterModel implements Parcelable {

    public String iso;
    public String phonecode;
    public String iconHexColor;

    public CountryCodeModel(){

    }

    public CountryCodeModel(JSONObject jsonObject) throws JSONException{
        iso = jsonObject.getString(AppConstant.PARSER_TAG.ISO);
        phonecode = jsonObject.getString(AppConstant.PARSER_TAG.PHONE_CODE);
        name = jsonObject.getString(AppConstant.PARSER_TAG.USERNAME);
    }


    protected CountryCodeModel(Parcel in) {
        super(in);
        iso = in.readString();
        phonecode = in.readString();
        iconHexColor = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(iso);
        dest.writeString(phonecode);
        dest.writeString(iconHexColor);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CountryCodeModel> CREATOR = new Creator<CountryCodeModel>() {
        @Override
        public CountryCodeModel createFromParcel(Parcel in) {
            return new CountryCodeModel(in);
        }

        @Override
        public CountryCodeModel[] newArray(int size) {
            return new CountryCodeModel[size];
        }
    };
}
