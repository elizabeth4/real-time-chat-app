package com.nimapchatapp.app.countrycodescreen.model;

import android.content.Context;
import android.os.AsyncTask;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.AsyncLoaderNew;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CountryCodeParser extends AsyncTask<Void, Void, Integer> implements
        AsyncLoaderNew.Listener {

    // Variable Declarations
    private AsyncLoaderNew asyncLoader;
    private Context context;
    private String responseString;
    private ArrayList<CountryCodeModel> listOfCountyCode;

    // Callbacks for the initiator
    public interface CountryCodeListener {

        void didReceivedCountryCodeResult(int resultCode,ArrayList<CountryCodeModel> listOfCountyCode);
    }

    private CountryCodeListener listener;

    public void setListener(CountryCodeListener listener) {
        this.listener = listener;
    }

    public CountryCodeParser(Context context) {
        super();
        this.context = context;
    }

    public void getCountryCode() {
        asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET,
                AsyncLoaderNew.OutputData.TEXT, context);
        asyncLoader.setListener(this);
        asyncLoader.requestMessage = "";
        asyncLoader.requestActionMethod = "";
        asyncLoader.executeRequest(AppConstant.BASE_URL + "/Update_profile/get_phone_code");
    }

    //AsyncLoaderNew.Listener callbacks
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status, int dataIndex) {
        if (listener != null) {
            listener.didReceivedCountryCodeResult(AppConstant.CONNECTION_ERROR,null);
        }
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {
        responseString = new String(data);
        // we need to call the background thread to process the data.
        AppConstant.showErrorLog("AddUserParser", "responseString " + responseString);
        asyncLoader = null;
        execute();
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }

    // Life cycle of AsyncTask
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    @Override
    protected Integer doInBackground(Void... params) {
        try {
             listOfCountyCode = new ArrayList<>();

            JSONObject result = new JSONObject(responseString);

            int Status = result.getInt("Status");

            if (Status == 200) {

                // It is successful response from the server we can process the json file
                JSONArray jsonArray = result.getJSONArray(AppConstant.PARSER_TAG.DATA);
                for(int i=0; jsonArray.length()>i; i++){

                    JSONObject userObject =  jsonArray.getJSONObject(i);
                    CountryCodeModel userModel = new CountryCodeModel(userObject);
                    userModel.iconHexColor = AppConstant.getRandomSubscriptionHexColor(context);

                    listOfCountyCode.add(userModel);
                }

                return AppConstant.SUCCESS;

            } else if (Status == 201) {
                return AppConstant.PROCESSING_ERROR;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("JSONException", "JSONException " + e);
            cancel(true);
        }

        return AppConstant.PROCESSING_ERROR;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        int resultCode = integer.intValue();

        if (listener != null) {
            if (resultCode == AppConstant.SUCCESS) {
                listener.didReceivedCountryCodeResult(AppConstant.SUCCESS,listOfCountyCode);
            } else {
                listener.didReceivedCountryCodeResult(AppConstant.PROCESSING_ERROR,null);
            }
        }
    }
}
