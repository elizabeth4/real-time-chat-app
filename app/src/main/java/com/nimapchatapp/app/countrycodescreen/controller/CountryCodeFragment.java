package com.nimapchatapp.app.countrycodescreen.controller;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.*;

import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.countrycodescreen.model.CountryCodeModel;
import com.nimapchatapp.app.countrycodescreen.model.CountryCodeParser;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.BaseFragment;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class CountryCodeFragment extends BaseFragment implements
        CountryCodeParser.CountryCodeListener {

    private static final String TAG = CountryCodeFragment.class.getSimpleName();

    private CountryCodeParser parser;
    private RecyclerView recyclerView;
    private ArrayList<CountryCodeModel> listOfCountyCode;
    private ArrayList<CountryCodeModel> countyCodeListFiltered;
    private CountryCodeAdapter adapter;
    private TextView errorTextView;
    private Toolbar mToolbar;

    public GetCodeListener listener;

    public interface GetCodeListener {

        void getCountryCode(CountryCodeModel model);
    }

    public void setCodeListener(GetCodeListener listener) {
        this.listener = listener;
    }


    public CountryCodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_recycle_view, container, false);
        setHasOptionsMenu(true);
        callCountryCodeParser();

        setData(view);
        setListener(view);


        return view;

    }

    //BaseFragment
    @Override
    protected void setData(View view) {
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        ((AppCompatActivity)Objects.requireNonNull(getActivity())).setSupportActionBar(mToolbar);
        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setTitle("Country Code");
        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(true);
        errorTextView = view.findViewById(R.id.error_text_view);
        recyclerView = view.findViewById(R.id.recycle_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(myContext));

    }

    @Override
    protected void setListener(View view) {

    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new CountryCodeAdapter();
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private void hideAndShowView(ArrayList<CountryCodeModel> listOfCountyCode) {
        if (listOfCountyCode.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText(AppConstant.NO_COUNTRY_CODE_AVAILABLE_MESSAGE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            errorTextView.setVisibility(View.GONE);
            setAdapter();
        }
    }

    private void callCountryCodeParser() {
        showDialog();
        if (parser == null) {
            parser = new CountryCodeParser(myContext);
            parser.setListener(this);
            parser.getCountryCode();

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                AppConstant.showErrorLog(TAG, s);
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                AppConstant.showErrorLog(TAG, s);
                return false;
            }
        });
    }

    class CountryCodeAdapter extends RecyclerView.Adapter<CountryCodeAdapter.ViewHolder>
            implements Filterable {


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_contact_list, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

            viewHolder.bindData(countyCodeListFiltered);
        }

        @Override
        public int getItemCount() {
            return countyCodeListFiltered.size();
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        countyCodeListFiltered = listOfCountyCode;
                    } else {
                        ArrayList<CountryCodeModel> filteredList = new ArrayList<>();
                        for (CountryCodeModel row : listOfCountyCode) {


                            // here we are looking for name or phone number match
                            if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.phonecode.contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }

                        countyCodeListFiltered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = countyCodeListFiltered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                    countyCodeListFiltered = (ArrayList<CountryCodeModel>) filterResults.values;

                    hideAndShowView(countyCodeListFiltered);

                }
            };
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView nameTextView, numberTextView, tvInitialName, inviteTextView;

            ViewHolder(@NonNull View itemView) {
                super(itemView);
                nameTextView = itemView.findViewById(R.id.tv_name);
                numberTextView = itemView.findViewById(R.id.tv_number);
                tvInitialName = itemView.findViewById(R.id.tv_initial_name);
                inviteTextView = itemView.findViewById(R.id.invite_text_view);
                inviteTextView.setVisibility(View.GONE);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {

                            listener.getCountryCode( getSelectedMode(getAdapterPosition()));
                        }
                    }
                });
            }

            private void bindData(ArrayList<CountryCodeModel> list) {

                CountryCodeModel codeModel = list.get(getAdapterPosition());

                nameTextView.setText(codeModel.name);
                tvInitialName.setText(codeModel.iso);
                numberTextView.setText(list.get(getAdapterPosition()).phonecode);

                Drawable drawable = getResources().getDrawable(R.drawable.circuler_text_view);
                drawable.setColorFilter(Color.parseColor(codeModel.iconHexColor), PorterDuff.Mode.SRC_ATOP);
                tvInitialName.setBackground(drawable);
            }

            private CountryCodeModel getSelectedMode(int position){

                return countyCodeListFiltered.get(position);
            }

        }
    }

    //CountryCodeParser.CountryCodeListener
    @Override
    public void didReceivedCountryCodeResult(int resultCode, ArrayList<CountryCodeModel> list) {
        hideDialog();
        if (resultCode == AppConstant.SUCCESS) {
            this.listOfCountyCode = list;
            countyCodeListFiltered = listOfCountyCode;
            hideAndShowView(listOfCountyCode);
        } else if (resultCode == AppConstant.CONNECTION_ERROR) {
            AppConstant.showToast(myContext, AppConstant.CONNECTION_ERROR_MESSAGE);
        }

    }


}
