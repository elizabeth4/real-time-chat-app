package com.nimapchatapp.app.contactscreen.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.nimapchatapp.app.globel.WMasterModel;
import com.nimapchatapp.app.profilescreen.model.FriendListModel;

import java.util.ArrayList;

public class ContactModel extends WMasterModel implements Parcelable {

    public ArrayList<FriendListModel> friendList;
    public boolean isFriend;


    public ContactModel(){

    }
    protected ContactModel(Parcel in) {
        super(in);
        friendList = in.createTypedArrayList(FriendListModel.CREATOR);
        isFriend = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(friendList);
        dest.writeByte((byte) (isFriend ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContactModel> CREATOR = new Creator<ContactModel>() {
        @Override
        public ContactModel createFromParcel(Parcel in) {
            return new ContactModel(in);
        }

        @Override
        public ContactModel[] newArray(int size) {
            return new ContactModel[size];
        }
    };
}