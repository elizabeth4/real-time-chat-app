package com.nimapchatapp.app.contactscreen.controller;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.chatscreen.controller.ChatActivity;
import com.nimapchatapp.app.contactscreen.model.ContactModel;
import com.nimapchatapp.app.profilescreen.model.FriendListModel;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.BaseAppCompatActivity;
import com.nimapchatapp.app.globel.WhooshhDB;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class ContactListActivity extends BaseAppCompatActivity {

    private ArrayList<ContactModel> contactList;
    private ArrayList<ContactModel> contactListFiltered;
    private RecyclerView recyclerView;
    private ContactListAdapter adapter;
    private Toolbar mToolbar;
    private WhooshhDB db;
    private TextView errorTextView, inviteTextView;
    private String contactLastSevenNumber;

    private ArrayList<FriendListModel> friendList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_recycle_view);

        db = new WhooshhDB(myContext);

//        contactList = db.getAllDeviceContacts();
//        friendList = db.getAllFriendContacts();


        if (friendList.size() > 0) {
            contactList = addFriendInContactList();
            contactListFiltered = contactList;
        }else {
            contactListFiltered = contactList;
        }

        setData();
        setListener();

    }

    // friend user add in contact list
    private ArrayList<ContactModel> addFriendInContactList() {

        ArrayList<ContactModel> allUserList = new ArrayList<>();

        if (contactList != null && friendList.size() > 0) {
            for (int i = 0; contactList.size() > i; i++) {

                contactLastSevenNumber = contactList.get(i).mobileNumber.substring(contactList.get(i).mobileNumber.length() - 7);

                for (int j = 0; friendList.size() > j; j++) {

                    FriendListModel model = new FriendListModel();
                    model.mobileNumber = friendList.get(j).mobileNumber.substring(friendList.get(j).mobileNumber.length() - 7);
                    if (model.mobileNumber.equals(contactLastSevenNumber)) {
                        ContactModel contactModel = new ContactModel();
                        contactModel.name = friendList.get(j).name;
                        contactModel.userId = friendList.get(j).userId;
                        contactModel.mobileNumber = friendList.get(j).mobileNumber;
                        contactModel.image = friendList.get(j).image;
                        contactModel.thumb_image = friendList.get(j).thumb_image;
                        contactModel.status = friendList.get(j).status;
                        contactModel.iconHexColor = friendList.get(j).iconHexColor;
                        contactModel.isFriend = true;
                        contactList.remove(i);
                        contactList.add(0, contactModel);
                        allUserList = contactList;
                    }
                }

            }
        }

        return allUserList;
    }


    @Override
    protected void setData() {
        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Contact List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(myContext));
        inviteTextView = findViewById(R.id.invite_text_view);
        inviteTextView.setVisibility(View.GONE);
        errorTextView = findViewById(R.id.error_text_view);

        hideAndShowView(contactList);

    }

    private void hideAndShowView(ArrayList<ContactModel> contactList) {
        if (contactList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText(AppConstant.NO_FRIEND_AVAILABLE_MESSAGE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            errorTextView.setVisibility(View.GONE);
            setAdapter();
        }
    }


    @Override
    protected void setListener() {

    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new ContactListAdapter();
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.search_menu, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                AppConstant.showErrorLog(TAG, s);
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                AppConstant.showErrorLog(TAG, s);
                return false;
            }
        });


        return true;
    }

    class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder>
            implements Filterable {


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_contact_list, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

            viewHolder.bindData(contactListFiltered);
        }

        @Override
        public int getItemCount() {
            return contactListFiltered.size();
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        contactListFiltered = contactList;
                    } else {
                        ArrayList<ContactModel> filteredList = new ArrayList<>();
                        for (ContactModel row : contactList) {


                            // here we are looking for name or phone number match
                            if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.mobileNumber.contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }

                        contactListFiltered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = contactListFiltered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    contactListFiltered = (ArrayList<ContactModel>) filterResults.values;

                    hideAndShowView(contactListFiltered);

                }
            };
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView nameTextView, numberTextView, tvInitialName, inviteTextView;

            ViewHolder(@NonNull View itemView) {
                super(itemView);
                nameTextView = itemView.findViewById(R.id.tv_name);
                numberTextView = itemView.findViewById(R.id.tv_number);
                tvInitialName = itemView.findViewById(R.id.tv_initial_name);
                inviteTextView = itemView.findViewById(R.id.invite_text_view);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(contactListFiltered.get(getAdapterPosition()).isFriend){
                            Intent intent = new Intent(myContext,ChatActivity.class);
                            Gson gson = new Gson();
                            String myJson = gson.toJson(getSelectedModel(getAdapterPosition()));
                            intent.putExtra("ChatWith",myJson);

                            startActivity(intent);
                        }else {
                            inviteFriend();
                        }
                    }
                });
            }

            private void bindData(ArrayList<ContactModel> list) {

                ContactModel contact = list.get(getAdapterPosition());

                nameTextView.setText(contact.name);
                tvInitialName.setText(AppConstant.getInitials(contact.name));
                numberTextView.setText(list.get(getAdapterPosition()).mobileNumber);

                Drawable drawable = getResources().getDrawable(R.drawable.circuler_text_view);
                drawable.setColorFilter(Color.parseColor(contact.iconHexColor), PorterDuff.Mode.SRC_ATOP);
                tvInitialName.setBackground(drawable);

                if (contact.isFriend) {
                    inviteTextView.setBackgroundResource(R.drawable.ic_logo);
                } else {
                    inviteTextView.setBackgroundResource(R.drawable.button_design);
                }

            }

            private void inviteFriend() {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "https://play.google.com/store/apps/details?id=com.nimaptxtme.app";
                String shareSub = "Your subject here";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }

            private ContactModel getSelectedModel(int position){

                return contactListFiltered.get(position);
            }

        }
    }
}
