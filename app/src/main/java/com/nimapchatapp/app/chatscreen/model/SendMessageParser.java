package com.nimapchatapp.app.chatscreen.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.AsyncLoaderNew;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.globel.WhooshhDB;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SendMessageParser extends AsyncTask<Void, Void, Integer> implements
        AsyncLoaderNew.Listener {


    private String TAG = "SendMessageParser";

    // Variable Declarations
    private AsyncLoaderNew asyncLoader;
    private Context context;
    private String responseString;
    private WhooshhDB db;
    private MessageModel messageModel;
//    private ArrayList<ContactModel> listOfContact;

    // Callbacks for the initiator
    public interface SendMessageListener {

        void didReceivedSendMessageResult(int resultCode,MessageModel messageModel);
    }

    private SendMessageParser.SendMessageListener listener;

    public void setListener(SendMessageParser.SendMessageListener listener) {
        this.listener = listener;
    }

    public SendMessageParser(Context context) {
        super();
        this.context = context;
    }

    public void sendMessageWith(String message,String message_type,String group_id,String sender_id,String receiver_id,String userToken,String userId) {

        PreferenceManager.saveStringForKey(context,AppConstant.HEADER_PARM_USER_ID, userId);
        PreferenceManager.saveStringForKey(context, AppConstant.HEADER_PARM_USER_TOKEN, userToken);

        Log.e(TAG,"userId="+userId);
        Log.e(TAG,"user-token="+userToken);


        asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER,
                    AsyncLoaderNew.OutputData.TEXT, context);
            asyncLoader.setListener(this);
            JSONObject jsonObject = new JSONObject();

            db = new WhooshhDB(context);

            JSONArray jsonArrayUserIds = new JSONArray();


            jsonArrayUserIds.put(receiver_id);






            try {

                // We need set the user type to always vendor
                jsonObject.accumulate(AppConstant.PARSER_TAG.GROUP_ID, group_id);
                jsonObject.accumulate(AppConstant.PARSER_TAG.MESSAGE, message);
                jsonObject.accumulate(AppConstant.PARSER_TAG.MESSAGE_TYPE, message_type);
                jsonObject.accumulate(AppConstant.PARSER_TAG.SENDER_ID, sender_id);
                jsonObject.accumulate(AppConstant.PARSER_TAG.RECEIVER_ID, jsonArrayUserIds);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            // 4. convert JSONObject to JSON to String
            String jsonString = jsonObject.toString();
            AppConstant.showErrorLog("SendMessageParser", "jsonString " + jsonString);
            asyncLoader.requestMessage = jsonString;
            asyncLoader.requestActionMethod = "";
            asyncLoader.executeRequest(AppConstant.BASE_URL + AppConstant.SEND_MESSAGE_URL);


    }





    //AsyncLoaderNew.Listener callbacks
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status, int dataIndex) {
        if (listener != null) {
            listener.didReceivedSendMessageResult(AppConstant.CONNECTION_ERROR,messageModel);
        }
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {
        responseString = new String(data);
        // we need to call the background thread to process the data.
        AppConstant.showErrorLog("SendMessageParser", "responseString " + responseString);
        asyncLoader = null;
        execute();
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }

    // Life cycle of AsyncTask
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    @Override
    protected Integer doInBackground(Void... params) {
        try {
            db = new WhooshhDB(context);
            JSONObject result = new JSONObject(responseString);

            int Status = result.getInt("Status");

            if (Status == 200) {

                //WhooshhDB db = new WhooshhDB(context);


                // It is successful response from the server we can process the json file
                JSONObject messageObject = result.getJSONObject(AppConstant.PARSER_TAG.DATA);

                messageModel = new MessageModel(messageObject,AppConstant.DEFAULT_ZERO,AppConstant.DEFAULT_EMPTY);




                if(db.isMessageExit(messageModel.id)){
                    messageModel = db.getMessageWith(messageModel.id);
                    db.updateMessage(messageModel);
                }
                else {
                    db.insertMessage(messageModel);
                }


                messageModel = db.getMessageWith(messageModel.id);

                return AppConstant.SUCCESS;

            } else if (Status == 201) {
                return AppConstant.PROCESSING_ERROR;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("JSONException", "JSONException " + e);
            cancel(true);
        }

        return AppConstant.PROCESSING_ERROR;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        int resultCode = integer.intValue();

        if (listener != null) {
            if (resultCode == AppConstant.SUCCESS) {
                listener.didReceivedSendMessageResult(AppConstant.SUCCESS,messageModel);
            } else {
                listener.didReceivedSendMessageResult(AppConstant.PROCESSING_ERROR,messageModel);
            }
        }
    }
}
