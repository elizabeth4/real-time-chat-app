package com.nimapchatapp.app.chatscreen.model;

import android.content.Context;
import android.os.AsyncTask;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.AsyncLoaderNew;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.globel.WhooshhDB;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetRecentMessageParser extends AsyncTask<Void, Void, Integer> implements
        AsyncLoaderNew.Listener {


    private String TAG = "GetRecentMessageParser";

    // Variable Declarations
    private AsyncLoaderNew asyncLoader;
    private Context context;
    private String responseString;
    private WhooshhDB db;
//    private ArrayList<ContactModel> listOfContact;

    // Callbacks for the initiator
    public interface GetRecentMessageListener {

        void didReceivedRecentMessageResult(int resultCode);
    }

    private GetRecentMessageParser.GetRecentMessageListener listener;

    public void setListener(GetRecentMessageParser.GetRecentMessageListener listener) {
        this.listener = listener;
    }

    public GetRecentMessageParser(Context context) {
        super();
        this.context = context;
    }

    public void getRecentMessageWith(String group_id,String sender_id,String receiver_id,String userToken,String userId,String last_message_id) {


        asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER,
                AsyncLoaderNew.OutputData.TEXT, context);
        asyncLoader.setListener(this);
        JSONObject jsonObject = new JSONObject();

        db = new WhooshhDB(context);



        PreferenceManager.saveStringForKey(context,AppConstant.HEADER_PARM_USER_ID, userId);
        PreferenceManager.saveStringForKey(context, AppConstant.HEADER_PARM_USER_TOKEN, userToken);



        try {

            // We need set the user type to always vendor
            jsonObject.accumulate(AppConstant.PARSER_TAG.GROUP_ID, group_id);
            jsonObject.accumulate(AppConstant.PARSER_TAG.MESSAGE_ID, last_message_id);
            jsonObject.accumulate(AppConstant.PARSER_TAG.SENDER_ID, sender_id);
            jsonObject.accumulate(AppConstant.PARSER_TAG.RECEIVER_ID, receiver_id);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 4. convert JSONObject to JSON to String
        String jsonString = jsonObject.toString();
        AppConstant.showErrorLog("GetRecentMessageParser", "jsonString " + jsonString);
        asyncLoader.requestMessage = jsonString;
        asyncLoader.requestActionMethod = "";
        asyncLoader.executeRequest(AppConstant.BASE_URL + AppConstant.GET_OLD_MESSAGES_URL);

    }





    //AsyncLoaderNew.Listener callbacks
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status, int dataIndex) {
        if (listener != null) {
            listener.didReceivedRecentMessageResult(AppConstant.CONNECTION_ERROR);
        }
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {
        responseString = new String(data);
        // we need to call the background thread to process the data.
        AppConstant.showErrorLog("GetRecentMessageParser", "responseString " + responseString);
        asyncLoader = null;
        execute();
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }

    // Life cycle of AsyncTask
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    @Override
    protected Integer doInBackground(Void... params) {
        try {
            db = new WhooshhDB(context);
            JSONObject result = new JSONObject(responseString);

            int Status = result.getInt(AppConstant.PARSER_TAG.STATUS);

            if (Status == 200) {

                //WhooshhDB db = new WhooshhDB(context);


                // It is successful response from the server we can process the json file
                JSONArray messageObjectsList = result.getJSONArray(AppConstant.PARSER_TAG.DATA);

                MessageModel messageModel;

                for(int i=0;i<messageObjectsList.length();i++){
                    messageModel = new MessageModel(messageObjectsList.getJSONObject(i),AppConstant.DEFAULT_ZERO,AppConstant.DEFAULT_EMPTY);

                    if(db.isMessageExit(messageModel.id)){
                        messageModel = db.getMessageWith(messageModel.id);
                        db.updateMessage(messageModel);
                    }
                    else {
                        db.insertMessage(messageModel);
                    }
                }



                return AppConstant.SUCCESS;

            } else if (Status == 201) {
                return AppConstant.PROCESSING_ERROR;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("JSONException", "JSONException " + e);
            cancel(true);
        }

        return AppConstant.PROCESSING_ERROR;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        int resultCode = integer.intValue();

        if (listener != null) {
            if (resultCode == AppConstant.SUCCESS) {
                listener.didReceivedRecentMessageResult(AppConstant.SUCCESS);
            } else {
                listener.didReceivedRecentMessageResult(AppConstant.PROCESSING_ERROR);
            }
        }
    }
}
