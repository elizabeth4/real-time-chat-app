package com.nimapchatapp.app.chatscreen.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.nimapchatapp.app.globel.AppConstant;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageModel implements Parcelable {

    public String id;
    public String Lid;
    public String messageType;
    public String messageBody;
    public String messageDate;
    public String messageFrom;
    public String messageTo;
    public String messageGroupId;
    public String messageSeen;
    public String messageIsFirst;
    public String messageMultimediaPath;
    public String messageMultimediaOfflinePath;
    public String messageIsDownload;
    public String messageIsDeleted;
    public String messageIsView;
    public String messageSchedule;
    public String messageScheduleDate;
    public String messageCreatedDate;
    public String messageModifiedDate;
    public String senderName;





    public MessageModel(){}

    public MessageModel(JSONObject messageObject, String lid,String offline_path) throws JSONException {


        this.Lid = lid;
        id = messageObject.getString(AppConstant.PARSER_TAG.ID);
        messageType = messageObject.getString(AppConstant.PARSER_TAG.MESSAGE_TYPE);
        messageBody = messageObject.getString(AppConstant.PARSER_TAG.MESSAGE_BODY);
        messageDate = messageObject.getString(AppConstant.PARSER_TAG.CREATED_DATE);
        messageFrom = messageObject.getString(AppConstant.PARSER_TAG.MESSAGE_FROM);
        messageTo = messageObject.getString(AppConstant.PARSER_TAG.MESSAGE_TO);
        messageGroupId = messageObject.getString(AppConstant.PARSER_TAG.GROUP_ID);
        messageSeen = messageObject.getString(AppConstant.PARSER_TAG.SEEN);
        messageIsFirst = messageObject.getString(AppConstant.PARSER_TAG.IS_FIRST);
        messageMultimediaPath = messageObject.getString(AppConstant.PARSER_TAG.MULTIMEDIA_PATH);
        messageMultimediaOfflinePath = offline_path;
        messageIsDownload = messageObject.getString(AppConstant.PARSER_TAG.IS_DOWNLOADED);
        messageIsDeleted = messageObject.getString(AppConstant.PARSER_TAG.IS_DELETED);
        messageIsView = messageObject.getString(AppConstant.PARSER_TAG.SEEN);
        messageSchedule = messageObject.getString(AppConstant.PARSER_TAG.IS_SCHEDULED);
        messageScheduleDate = messageObject.getString(AppConstant.PARSER_TAG.SCHEDULED_DATE);
        messageCreatedDate = messageObject.getString(AppConstant.PARSER_TAG.CREATED_DATE);
        messageModifiedDate = messageObject.getString(AppConstant.PARSER_TAG.MODIFIED_DATE);
        senderName = messageObject.getString(AppConstant.PARSER_TAG.SENDER_NAME);




    }



    protected MessageModel(Parcel in) {
        id = in.readString();
        Lid = in.readString();
        messageType = in.readString();
        messageBody = in.readString();
        messageDate = in.readString();
        messageFrom = in.readString();
        messageTo = in.readString();
        messageGroupId = in.readString();
        messageSeen = in.readString();
        messageIsFirst = in.readString();
        messageMultimediaPath = in.readString();
        messageMultimediaOfflinePath = in.readString();
        messageIsDownload = in.readString();
        messageIsDeleted = in.readString();
        messageIsView = in.readString();
        messageSchedule = in.readString();
        messageScheduleDate = in.readString();
        messageCreatedDate = in.readString();
        messageModifiedDate = in.readString();
    }

    public static final Creator<MessageModel> CREATOR = new Creator<MessageModel>() {
        @Override
        public MessageModel createFromParcel(Parcel in) {
            return new MessageModel(in);
        }

        @Override
        public MessageModel[] newArray(int size) {
            return new MessageModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(Lid);
        dest.writeString(messageType);
        dest.writeString(messageBody);
        dest.writeString(messageDate);
        dest.writeString(messageFrom);
        dest.writeString(messageTo);
        dest.writeString(messageGroupId);
        dest.writeString(messageSeen);
        dest.writeString(messageIsFirst);
        dest.writeString(messageMultimediaPath);
        dest.writeString(messageMultimediaOfflinePath);
        dest.writeString(messageIsDownload);
        dest.writeString(messageIsDeleted);
        dest.writeString(messageIsView);
        dest.writeString(messageSchedule);
        dest.writeString(messageScheduleDate);
        dest.writeString(messageCreatedDate);
        dest.writeString(messageModifiedDate);
    }


    @Override
    public String toString() {
        return "MessageModel{" +
                "id='" + id + '\'' +
                ", Lid='" + Lid + '\'' +
                ", messageType='" + messageType + '\'' +
                ", messageBody='" + messageBody + '\'' +
                ", messageDate='" + messageDate + '\'' +
                ", messageFrom='" + messageFrom + '\'' +
                ", messageTo='" + messageTo + '\'' +
                ", messageGroupId='" + messageGroupId + '\'' +
                ", messageSeen='" + messageSeen + '\'' +
                ", messageIsFirst='" + messageIsFirst + '\'' +
                ", messageMultimediaPath='" + messageMultimediaPath + '\'' +
                ", messageMultimediaOfflinePath='" + messageMultimediaOfflinePath + '\'' +
                ", messageIsDownload='" + messageIsDownload + '\'' +
                ", messageIsDeleted='" + messageIsDeleted + '\'' +
                ", messageIsView='" + messageIsView + '\'' +
                ", messageSchedule='" + messageSchedule + '\'' +
                ", messageScheduleDate='" + messageScheduleDate + '\'' +
                ", messageCreatedDate='" + messageCreatedDate + '\'' +
                ", messageModifiedDate='" + messageModifiedDate + '\'' +
                '}';
    }
}
