package com.nimapchatapp.app.chatscreen.model;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.firebase.database.DataSnapshot;
import com.nimapchatapp.app.globel.AppConstant;

import java.util.HashMap;

public class ChatModel implements Parcelable {

    public String msgKey;
    public long timeStamp;
    public String message;
    public String senderId;
    public String to;
    public String messageType;

    public ChatModel(){

    }


    public ChatModel(DataSnapshot dataSnapshot,int comingFrom){

        HashMap<String, Object> object = (HashMap<String, Object>) dataSnapshot.getValue();
        if(comingFrom == AppConstant.COMMING_FROM_REAL_TIME){
            this.msgKey= dataSnapshot.getKey();
            this.message=object.get("message").toString();
            this.senderId=object.get("senderId").toString();
            this.to=object.get("to").toString();

        }else if(comingFrom == AppConstant.COMMING_FROM_NORMAL_MESSAGE){
            this.msgKey= dataSnapshot.getKey();
            this.timeStamp=Long.parseLong(object.get("timeStamp").toString());
            this.message=object.get("message").toString();
            this.senderId=object.get("senderId").toString();
            this.to=object.get("to").toString();
            this.messageType=object.get("messageType").toString();
        }

    }

    protected ChatModel(Parcel in) {
        msgKey = in.readString();
        timeStamp = in.readLong();
        message = in.readString();
        senderId = in.readString();
        to = in.readString();
        messageType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(msgKey);
        dest.writeLong(timeStamp);
        dest.writeString(message);
        dest.writeString(senderId);
        dest.writeString(to);
        dest.writeString(messageType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChatModel> CREATOR = new Creator<ChatModel>() {
        @Override
        public ChatModel createFromParcel(Parcel in) {
            return new ChatModel(in);
        }

        @Override
        public ChatModel[] newArray(int size) {
            return new ChatModel[size];
        }
    };
}