package com.nimapchatapp.app.chatscreen.controller;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.chatscreen.model.MessageModel;
import com.nimapchatapp.app.globel.NewDateConvertion;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MessageModel> mMessageList;
    private final int VIEW_TYPE_SENDER_MESSAGE = 1;
    private final int VIEW_TYPE_RECEIVER_MESSAGE = 2;
    private final int VIEW_TYPE_DEFAULT = 3;
    private  String sender_id;
    private ChatAdapterOnClickListener chatAdapterOnClickListener;

    public ChatAdapter(List<MessageModel> list,String sender_id,ChatAdapterOnClickListener chatAdapterOnClickListener){
        this.mMessageList = list;
        this.sender_id = sender_id;
         this.chatAdapterOnClickListener = chatAdapterOnClickListener;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case VIEW_TYPE_SENDER_MESSAGE:
                View mBindingSender = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_message_sent, parent, false);
                viewHolder = new ViewSenderMessageHolder(mBindingSender);
                break;
            case VIEW_TYPE_RECEIVER_MESSAGE:
                View mBindingReceiver = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_message_received, parent, false);
                viewHolder = new ViewReceiverMessageHolder(mBindingReceiver);
                break;
        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final MessageModel model = mMessageList.get(position);

        if(holder instanceof ViewSenderMessageHolder ){
            ViewSenderMessageHolder senderHolder = (ViewSenderMessageHolder) holder;

            senderHolder.bindData(model);

        }else if (holder instanceof ViewReceiverMessageHolder){

            ViewReceiverMessageHolder receiverHolder = (ViewReceiverMessageHolder) holder;

            receiverHolder.bindData(model);
        }

    }
    @Override
    public int getItemViewType(int position) {
        MessageModel model = mMessageList.get(position);
        if (model != null) {
            if (model.messageFrom.equals(sender_id)) {
                return VIEW_TYPE_SENDER_MESSAGE;
            }else {
                return VIEW_TYPE_RECEIVER_MESSAGE;
            }
        }
        return VIEW_TYPE_DEFAULT;
    }


    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    private class ViewSenderMessageHolder extends RecyclerView.ViewHolder {

        private EmojiconTextView messageTextView;
        TextView senderTime;

        public ViewSenderMessageHolder(@NonNull final View itemView) {
            super(itemView);
            messageTextView = itemView.findViewById(R.id.send_message_text_view);
            senderTime = itemView.findViewById(R.id.send_text_message_time);

            messageTextView.setEmojiconSize(50);

            messageTextView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    if(chatAdapterOnClickListener!=null){
                        chatAdapterOnClickListener.onMessageDeleteClick(getAdapterPosition(),messageTextView);
                    }

                    return true;
                }
            });

        }


        public void bindData(MessageModel model) {
            messageTextView.setText(model.messageBody);

            String date = model.messageDate;

            StringTokenizer tk = new StringTokenizer(date);

            String newDate = "";
            String time = "";
            String am = "";

            //12 hour format
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date newTime = format.parse(date);
                format = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
                String formattedTime = format.format(newTime);

                StringTokenizer tk1 = new StringTokenizer(formattedTime);

                newDate = tk1.nextToken();
                time = tk1.nextToken();
                am = tk1.nextToken();

                }catch (ParseException ex){
                ex.printStackTrace();
            }

            senderTime.setText((NewDateConvertion.formateddate(newDate)+" "+time+" "+am));
        }
    }

    private class ViewReceiverMessageHolder extends RecyclerView.ViewHolder {

        EmojiconTextView receiveMessageTextView;
        TextView receiverTime,receiverName;

        public ViewReceiverMessageHolder(@NonNull View itemView) {
            super(itemView);
            receiveMessageTextView = itemView.findViewById(R.id.receive_message_text_view);
            receiveMessageTextView.setEmojiconSize(50);
            receiverTime = itemView.findViewById(R.id.receive_text_message_time);
            receiverName = itemView.findViewById(R.id.receiver_name);
        }

        public void bindData(MessageModel model) {
            receiverName.setVisibility(View.VISIBLE);
            receiveMessageTextView.setText(model.messageBody);

            String date = model.messageDate;
           /* StringTokenizer tk = new StringTokenizer(date);

            String newDate = tk.nextToken();
            String time = tk.nextToken();*/

            String newDate = "";
            String time = "";
            String am = "";

            //12 hour format
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date newTime = format.parse(date);
                format = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
                String formattedTime = format.format(newTime);

                StringTokenizer tk1 = new StringTokenizer(formattedTime);

                newDate = tk1.nextToken();
                time = tk1.nextToken();
                am = tk1.nextToken();

            }catch (ParseException ex){
                ex.printStackTrace();
            }

            receiverTime.setText((NewDateConvertion.formateddate(newDate)+" "+time+" "+am));

        }
    }



    interface ChatAdapterOnClickListener{

        public void onMessageDeleteClick(int position,View view);
    }
}





