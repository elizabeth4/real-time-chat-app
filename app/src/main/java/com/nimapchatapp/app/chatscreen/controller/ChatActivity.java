package com.nimapchatapp.app.chatscreen.controller;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.*;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.*;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.appcontroller.AppController;
import com.nimapchatapp.app.chatscreen.model.*;
import com.nimapchatapp.app.firbase.FirebaseHelper;
import com.nimapchatapp.app.firbase.MessagesParser;
import com.nimapchatapp.app.firbase.RealTimeMessagesParser;
import com.nimapchatapp.app.globel.*;
import com.nimapchatapp.app.profilescreen.model.UserModel;
import com.nimapchatapp.app.recentchatuser.controller.RecentChatUserActivity;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import io.fabric.sdk.android.Fabric;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ChatActivity extends BaseAppCompatActivity implements View.OnClickListener,
        SendMessageParser.SendMessageListener,
        MessagesParser.FirebaseCallBacksListener,
        GetOldMessageParser.GetOldMessageListener,
        GetRecentMessageParser.GetRecentMessageListener,
        RealTimeMessagesParser.RealTimeMessagesListener,
        ChatAdapter.ChatAdapterOnClickListener,
        DeleteMessageParser.DeleteMessageListener
        {

            private static final String TAG = ChatActivity.class.getSimpleName();
            private ImageButton imageButtonAttach;
            private LinearLayout mRevealView, realTimeChatView;
            private RecyclerView recyclerView;
            private boolean hidden = true;
            private EmojiconEditText messageEditText;
            private View rootView;
            private ImageView emojiImageView;
            private EmojIconActions emojIcon;
            private AppCompatImageButton sendButton;
            private UserModel chatWithUser,loginUser;
            private ChatAdapter adapter;
            private TextView chatUserNameTextView, realTimeMessageTextView;
            private CircularImageView imgProfile;
            private ArrayList<MessageModel> listOfMessage = new ArrayList<>();

            // firbase
            private String messageType = "";
            private DatabaseReference senderReference, receiverReference;
            private MessagesParser firebaseMessagesManager;
            private String myUserId = "";
            private String mChatUserId = "";
            private RealTimeMessagesParser realTimeMessagesParser;

            private WhooshhDB whooshhDB;

            private String sender_id ;
            private String receiver_id;
            private String userToken ;
            private String userId ;
            private String group_id ;



            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                Fabric.with(this, new Crashlytics());
                setContentView(R.layout.activity_chat);


                whooshhDB = new WhooshhDB(ChatActivity.this);
                String userId = getIntent().getExtras().getString(AppConstant.USER_INTENT_KEY,AppConstant.DEFAULT_ZERO);

                if(!whooshhDB.isUserExit(userId)){
                    ChatActivity.this.finish();

                }

                chatWithUser = whooshhDB.getUserWith(userId);

                if(chatWithUser == null){
                    ChatActivity.this.finish();

                }


                loginUser = whooshhDB.getLoginUser();

                myUserId = loginUser.userId;   //PreferenceManager.getStringForKey(myContext, AppConstant.PREFERENCE_TAG.USER_ID, AppConstant.TAG.DEFAULT);

                sender_id = loginUser.userId;
                receiver_id = chatWithUser.userId;
                userToken = loginUser.userToken;
                this.userId = loginUser.userId;
                group_id = getGroupId(sender_id,receiver_id);

                setData();
                setListener();
            }


            public UserModel getChatWithUser() {
                return chatWithUser;
            }


            @Override
            protected void setData() {

                setCurrentScreenToController();

                realTimeMessageTextView = findViewById(R.id.receive_message_text_view);
                chatUserNameTextView = findViewById(R.id.name_text_view);
                imgProfile = findViewById(R.id.img_profile);
                realTimeChatView = findViewById(R.id.real_time_chat_view);
                realTimeChatView.setVisibility(View.GONE);
                imageButtonAttach = findViewById(R.id.imageButton_attach);
                imageButtonAttach.setVisibility(View.GONE);
                mRevealView = findViewById(R.id.reveal_items);
                mRevealView.setVisibility(View.GONE);
                messageEditText = findViewById(R.id.message_edit_text);
                rootView = findViewById(R.id.linearLayout_user_chat_activity);
                emojiImageView = findViewById(R.id.imageButton_smiley);
                sendButton = findViewById(R.id.send_button);
                recyclerView = findViewById(R.id.recycle_view);
                recyclerView.setLayoutManager(new LinearLayoutManager(myContext));
                getMessageFromLocalDB();
                setAdapter();
                getChatUserData();
                messageEditText.setEmojiconSize(50);
                senderReference = FirebaseHelper.sendMessages().child(myUserId + "_" + mChatUserId);
                receiverReference = FirebaseHelper.sendMessages().child(mChatUserId + "_" + myUserId);
                emojIcon = new EmojIconActions(this, rootView, messageEditText, emojiImageView);
                emojIcon.ShowEmojIcon();
                /*emojIcon.setUseSystemEmoji(true);*/
                emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
                emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
                    @Override
                    public void onKeyboardOpen() {
                        Log.e(TAG, "Keyboard opened!");
                    }

                    @Override
                    public void onKeyboardClose() {
                        Log.e(TAG, "Keyboard closed");
                    }
                });



                getMessageFromFirbase();

                callRealTimeMessagesParser();

                getOldMessageApiServer();
                getRecentMessageApiServer();


            }

            private void getRecentMessageApiServer() {

                String sender_id = loginUser.userId;
                String receiver_id = chatWithUser.userId;
                String userToken = loginUser.userToken;
                String userId = loginUser.userId;
                String group_id = getGroupId(sender_id,receiver_id);
                String last_message_id = whooshhDB.getLastMessageIdWith(sender_id,receiver_id,group_id);

                GetRecentMessageParser getRecentMessageParser = new GetRecentMessageParser(myContext);
                getRecentMessageParser.setListener(ChatActivity.this);
                getRecentMessageParser.getRecentMessageWith(group_id,sender_id,receiver_id,userToken,userId,last_message_id);
            }

            private void setCurrentScreenToController() {

                AppController.getInstance().setChatActivity(ChatActivity.this);
            }

            private void getMessageFromLocalDB() {

                listOfMessage = whooshhDB.getMessagesWith(group_id);
            }

            @Override
            protected void setListener() {
                imageButtonAttach.setOnClickListener(this);
                sendButton.setOnClickListener(this);
                realTimeTextWatcher(messageEditText);

            }

            private void setAdapter() {
                adapter = new ChatAdapter(listOfMessage,sender_id,this);
                recyclerView.setAdapter(adapter);

                if(listOfMessage.size()>0){
                    recyclerView.scrollToPosition(listOfMessage.size() - 1);
                }
            }

            private void getMessageFromFirbase() {

                firebaseMessagesManager = new MessagesParser(senderReference);
                firebaseMessagesManager.setFirebaseCallBacksListener(this);
                firebaseMessagesManager.getMessages();
            }
            // calling real time message parser
            private void callRealTimeMessagesParser(){
                realTimeMessagesParser = new RealTimeMessagesParser();
                realTimeMessagesParser.getRealTimeMessages(myContext,myUserId,mChatUserId);
                realTimeMessagesParser.setRealTimeMessagesListener(this);
            }

            private void getChatUserData() {
                mChatUserId = chatWithUser.userId;
                chatUserNameTextView.setText(chatWithUser.name);
                ImageLoaderPicasso.loadImage(myContext, imgProfile, chatWithUser.image);
            }

            private void realTimeTextWatcher(EditText editText) {
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        realTimeMessagesParser.sendRealTimeMessagesToFirbase(messageEditText.getText().toString().trim());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            }


            private void openMultiMediaOption() {
                int cx = (mRevealView.getLeft() + mRevealView.getRight());
                int cy = (mRevealView.getTop());

                // to find  radius when icon is tapped for showing layout
                int startradius = 0;
                int endradius = Math.max(mRevealView.getWidth(), mRevealView.getHeight());

                // performing circular reveal when icon will be tapped
                Animator animator = ViewAnimationUtils.createCircularReveal(mRevealView, cx, cy, startradius, endradius);
                animator.setInterpolator(new AccelerateDecelerateInterpolator());
                animator.setDuration(400);

                //reverse animation
                // to find radius when icon is tapped again for hiding layout
                //  starting radius will be the radius or the extent to which circular reveal animation is to be shown

                int reverse_startradius = Math.max(mRevealView.getWidth(), mRevealView.getHeight());

                //endradius will be zero
                int reverse_endradius = 0;

                // performing circular reveal for reverse animation
                Animator animate = ViewAnimationUtils.createCircularReveal(mRevealView, cx, cy, reverse_startradius, reverse_endradius);
                if (hidden) {

                    // to show the layout when icon is tapped
                    mRevealView.setVisibility(View.VISIBLE);
                    animator.start();
                    hidden = false;
                } else {
                    mRevealView.setVisibility(View.VISIBLE);

                    // to hide layout on animation end
                    animate.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mRevealView.setVisibility(View.GONE);
                            hidden = true;
                        }
                    });
                    animate.start();
                }
            }

            public void sendMessageToFirebase(String message, String messageType) {


                Map<String, Object> map = new HashMap<>();
                map.put("message", message);
                map.put("messageType", messageType);
                map.put("timeStamp", System.currentTimeMillis());
                map.put("senderId", myUserId);
                map.put("to", mChatUserId);

                String senderKeyToPush = senderReference.push().getKey();
                String receiverKeyToPush = receiverReference.push().getKey();

                senderReference.child(senderKeyToPush).setValue(map);
                receiverReference.child(receiverKeyToPush).setValue(map);

                messageEditText.setText("");


            }


            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.imageButton_attach:
                        openMultiMediaOption();
                        break;
                    case R.id.send_button:

                        String message = messageEditText.getText().toString();

                        if (!TextUtils.isEmpty(message)) {
                            messageType = AppConstant.MESSAGES_TYPE.TEXT;

                            sendMessageToFirebase(message, messageType);
                            sendMessageApiServer(message, messageType);
                        }

                        break;

                }
            }

            private void sendMessageApiServer(String message, String messageType) {


                SendMessageParser sendMessageParser = new SendMessageParser(myContext);
                sendMessageParser.setListener(ChatActivity.this);
                sendMessageParser.sendMessageWith(message,messageType,group_id,sender_id,receiver_id,userToken,userId);

            }


            private void getOldMessageApiServer() {


                String sender_id = loginUser.userId;
                String receiver_id = chatWithUser.userId;
                String userToken = loginUser.userToken;
                String userId = loginUser.userId;
                String group_id = getGroupId(sender_id,receiver_id);
                String last_message_id = whooshhDB.getFirstMessageIdWith();

                GetOldMessageParser getOldMessageParser = new GetOldMessageParser(myContext);
                getOldMessageParser.setListener(ChatActivity.this);
                getOldMessageParser.getOldMessageWith(group_id,sender_id,receiver_id,userToken,userId,last_message_id);

            }

            private String getGroupId(String sender_id, String receiver_id) {

                String  groupId = "-1";

                groupId = whooshhDB.getGroupIdFromMessageWith(sender_id,receiver_id);

                return  groupId;
            }

            //MessagesParser.FirebaseCallBacksListener
            @Override
            public void didReceivedMessage(ChatModel model) {

//                this.listOfMessage.add(model);
//                adapter.notifyDataSetChanged();
//                recyclerView.scrollToPosition(listOfMessage.size() - 1);

            }
            //RealTimeMessagesParser.RealTimeMessagesListener
            @Override
            public void didReceivedRealTimeMessages(ChatModel model) {

                if (!model.senderId.equals(PreferenceManager.getStringForKey(myContext,
                        AppConstant.PREFERENCE_TAG.USER_ID, AppConstant.TAG.DEFAULT))) {
                    if (!model.message.isEmpty()) {
                        realTimeChatView.setVisibility(View.VISIBLE);
                        realTimeMessageTextView.setText(model.message);
                    } else {
                        realTimeChatView.setVisibility(View.GONE);

                    }
                }
            }

            @Override
            public void didReceivedSendMessageResult(int resultCode, MessageModel messageModel) {


                if(resultCode == AppConstant.SUCCESS) {

                    updateRecentChatUserScreen(messageModel);

                    this.listOfMessage.add(messageModel);
                    adapter.notifyDataSetChanged();
                    if (listOfMessage.size() > 0) {
                        recyclerView.scrollToPosition(listOfMessage.size() - 1);
                    }
                }
            }

            private void updateRecentChatUserScreen(MessageModel messageModel) {

                RecentChatUserActivity  recentChatUserActivity = AppController.getInstance().getRecentChatUserActivity();

                if(recentChatUserActivity !=null ){
                    if(whooshhDB.isMessageExit(messageModel.id)){

                        messageModel = whooshhDB.getMessageWith(messageModel.id);
                        whooshhDB.updateMessage(messageModel);
                    }
                    else {
                        whooshhDB.insertMessage(messageModel);
                    }

                    recentChatUserActivity.addFirebasePushNotificationMessageToRecentChatUser(messageModel);

                }

            }

            @Override
            public void didReceivedOldMessageResult(int resultCode) {

                if(resultCode == AppConstant.SUCCESS){

                    String temp_is_first = whooshhDB.getFirstMessageIdWith();

                    if(temp_is_first.equalsIgnoreCase("0")){
                        getOldMessageApiServer();
                    }

                }

            }


            public void deleteFirebasePushNotificationMessage(MessageModel messageModel){

                if(whooshhDB.isMessageExit(messageModel.id)) {
                    messageModel = whooshhDB.getMessageWith(messageModel.id);
                    whooshhDB.deleteMessage(messageModel.id);

//                    this.listOfMessage.remove(messageModel);


                    listOfMessage.clear();
                    ArrayList<MessageModel>listOfMessage_temp = whooshhDB.getMessagesWith(group_id);
                    listOfMessage.addAll(listOfMessage_temp);



                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // Stuff that updates the UI
                            adapter.notifyDataSetChanged();

                            if(listOfMessage.size()>0){
                                recyclerView.scrollToPosition(listOfMessage.size() - 1);
                            }


                        }
                    });

                }

            }



            public void addFirebasePushNotificationMessage(MessageModel messageModel){

                if(whooshhDB.isMessageExit(messageModel.id)){

                    messageModel = whooshhDB.getMessageWith(messageModel.id);
                    whooshhDB.updateMessage(messageModel);
                    messageModel = whooshhDB.getMessageWith(messageModel.id);
                }
                else {

                    whooshhDB.insertMessage(messageModel);
                    messageModel = whooshhDB.getMessageWith(messageModel.id);
                }


                this.listOfMessage.add(messageModel);


                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        // Stuff that updates the UI
                        adapter.notifyDataSetChanged();
                        if(listOfMessage.size()>0){
                            recyclerView.scrollToPosition(listOfMessage.size() - 1);
                        }

                    }
                });




            }

            @Override
            protected void onDestroy() {
                super.onDestroy();
                AppController.getInstance().setChatActivity(null);
            }


            @Override
            public void didReceivedRecentMessageResult(int resultCode) {

                if(resultCode == AppConstant.SUCCESS){

                    listOfMessage.clear();
                    ArrayList<MessageModel>listOfMessage_temp = whooshhDB.getMessagesWith(group_id);
                    listOfMessage.addAll(listOfMessage_temp);

                    adapter.notifyDataSetChanged();

                    if(listOfMessage.size()>0){
                        recyclerView.scrollToPosition(listOfMessage.size() - 1);
                    }
                }

            }

            @Override
            public void onMessageDeleteClick(int position,View view ) {

                openMessageDeleteDialog(listOfMessage.get(position),view);

            }

            private void openMessageDeleteDialog(final MessageModel messageModel, View view) {





                PopupMenu popup = new PopupMenu(ChatActivity.this, view);
                // This activity implements OnMenuItemClickListener .
                //popup.setOnMenuItemClickListener ((OnMenuItemClickListener) this);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete_message:
//                                Toast.makeText(ChatActivity.this, "FriendRequest", Toast.LENGTH_LONG).show();
                                callServerDeleteMessage(messageModel);
                                return true;

                            default:
                                return false;
                        }
                        //return false;
                    }
                });
                popup.inflate(R.menu.delete_message);
                popup.show();

            }

            private void callServerDeleteMessage(MessageModel messageModel) {


                String sender_id = loginUser.userId;
                String receiver_id = chatWithUser.userId;
                String userToken = loginUser.userToken;
                String userId = loginUser.userId;
                String group_id = messageModel.messageGroupId;
                String message_id = messageModel.id;

                DeleteMessageParser deleteMessageParser = new DeleteMessageParser(myContext);
                deleteMessageParser.setListener(ChatActivity.this);
                deleteMessageParser.deleteMessageWith(group_id,sender_id,receiver_id,userToken,userId,message_id,messageModel);

            }

            @Override
            public void didDeleteMessageResult(int resultCode,MessageModel messageModel) {


                if(resultCode == AppConstant.SUCCESS){
                    listOfMessage.remove(messageModel);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onBackPressed() {
                super.onBackPressed();
                startActivity(new Intent(this,RecentChatUserActivity.class));
                finish();
            }
        }


