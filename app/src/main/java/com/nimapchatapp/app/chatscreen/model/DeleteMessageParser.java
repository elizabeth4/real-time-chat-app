package com.nimapchatapp.app.chatscreen.model;

import android.content.Context;
import android.os.AsyncTask;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.AsyncLoaderNew;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.globel.WhooshhDB;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DeleteMessageParser extends AsyncTask<Void, Void, Integer> implements
        AsyncLoaderNew.Listener {


    private String TAG = "DeleteMessageParser";

    // Variable Declarations
    private AsyncLoaderNew asyncLoader;
    private Context context;
    private String responseString;
    private WhooshhDB db;
    private MessageModel messageModel;
//    private ArrayList<ContactModel> listOfContact;

    // Callbacks for the initiator
    public interface DeleteMessageListener {

        void didDeleteMessageResult(int resultCode,MessageModel messageModel);
    }

    private DeleteMessageParser.DeleteMessageListener listener;

    public void setListener(DeleteMessageParser.DeleteMessageListener listener) {
        this.listener = listener;
    }

    public DeleteMessageParser(Context context) {
        super();
        this.context = context;
    }

    public void deleteMessageWith(String group_id,String sender_id,String receiver_id,String userToken,String userId,String message_id,MessageModel messageModel) {


        asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER,
                AsyncLoaderNew.OutputData.TEXT, context);
        asyncLoader.setListener(this);
        JSONObject jsonObject = new JSONObject();

        JSONArray jsonArrayreceiverIds = new JSONArray();


        jsonArrayreceiverIds.put(receiver_id);


        db = new WhooshhDB(context);
        this.messageModel = messageModel;



        PreferenceManager.saveStringForKey(context,AppConstant.HEADER_PARM_USER_ID, userId);
        PreferenceManager.saveStringForKey(context, AppConstant.HEADER_PARM_USER_TOKEN, userToken);



        try {

            // We need set the user type to always vendor
            jsonObject.accumulate(AppConstant.PARSER_TAG.GROUP_ID, group_id);
            jsonObject.accumulate(AppConstant.PARSER_TAG.MESSAGE_ID, message_id);
            jsonObject.accumulate(AppConstant.PARSER_TAG.SENDER_ID, sender_id);
            jsonObject.accumulate(AppConstant.PARSER_TAG.RECEIVER_ID, jsonArrayreceiverIds);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 4. convert JSONObject to JSON to String
        String jsonString = jsonObject.toString();
        AppConstant.showErrorLog("DeleteMessageParser", "jsonString " + jsonString);
        asyncLoader.requestMessage = jsonString;
        asyncLoader.requestActionMethod = "";
        asyncLoader.executeRequest(AppConstant.BASE_URL + AppConstant.DELETE_MESSAGES_URL);


    }





    //AsyncLoaderNew.Listener callbacks
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status, int dataIndex) {
        if (listener != null) {
            listener.didDeleteMessageResult(AppConstant.CONNECTION_ERROR,messageModel);
        }
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {
        responseString = new String(data);
        // we need to call the background thread to process the data.
        AppConstant.showErrorLog("DeleteMessageParser", "responseString " + responseString);
        asyncLoader = null;
        execute();
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }

    // Life cycle of AsyncTask
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    @Override
    protected Integer doInBackground(Void... params) {
        try {
            db = new WhooshhDB(context);
            JSONObject result = new JSONObject(responseString);

            int Status = result.getInt(AppConstant.PARSER_TAG.STATUS);

            if (Status == 200) {

                //WhooshhDB db = new WhooshhDB(context);

                if(db.isMessageExit(messageModel.id)){
                    db.deleteMessage(messageModel.id);
                }


                return AppConstant.SUCCESS;

            } else if (Status == 201) {
                return AppConstant.PROCESSING_ERROR;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("JSONException", "JSONException " + e);
            cancel(true);
        }

        return AppConstant.PROCESSING_ERROR;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        int resultCode = integer.intValue();

        if (listener != null) {
            if (resultCode == AppConstant.SUCCESS) {
                listener.didDeleteMessageResult(AppConstant.SUCCESS,messageModel);
            } else {
                listener.didDeleteMessageResult(AppConstant.PROCESSING_ERROR,messageModel);
            }
        }
    }
}

