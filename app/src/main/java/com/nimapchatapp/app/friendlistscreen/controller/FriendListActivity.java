package com.nimapchatapp.app.friendlistscreen.controller;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.contactscreen.controller.ContactListActivity;
import com.nimapchatapp.app.globel.WhooshhDB;
import com.nimapchatapp.app.profilescreen.model.FriendListModel;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.BaseAppCompatActivity;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class FriendListActivity extends BaseAppCompatActivity implements
        View.OnClickListener {

    private static final String TAG = FriendListActivity.class.getSimpleName();

    private ArrayList<FriendListModel> friendList;

    private FriendListAdapter adapter;
    private WhooshhDB db;

    private RecyclerView recyclerView;
    private TextView errorTextView,inviteTextView;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_recycle_view);

        db = new WhooshhDB(myContext);

        if (friendList == null) {
            friendList = new ArrayList<>();
        }

//        friendList = db.getAllFriendContacts();

        setData();
        setListener();

    }

    //BaseAppCompatActivity
    @Override
    protected void setData() {
        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolBar();
        recyclerView = findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        errorTextView = findViewById(R.id.error_text_view);
        inviteTextView = findViewById(R.id.invite_text_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(myContext));

        if (friendList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText(AppConstant.NO_FRIEND_AVAILABLE_MESSAGE);
        }else {
            recyclerView.setVisibility(View.VISIBLE);
            errorTextView.setVisibility(View.GONE);
            setAdapter();
        }

    }

    private void toolBar(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Friend");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void setListener() {
        inviteTextView.setOnClickListener(this);
    }

    private void setAdapter() {
        adapter = new FriendListAdapter();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.search_menu, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(!s.isEmpty()){

                    AppConstant.showErrorLog(TAG,"-------" +s);
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });



        return true;
    }
    //View.OnClickListener
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.invite_text_view:
                startActivity(new Intent(myContext,ContactListActivity.class));
                break;
        }
    }

    class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {


        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_contact_list, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

            viewHolder.bindData(friendList);
        }

        @Override
        public int getItemCount() {
            return friendList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView nameTextView, numberTextView, tvInitialName,inviteTextView;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                nameTextView = itemView.findViewById(R.id.tv_name);
                numberTextView = itemView.findViewById(R.id.tv_number);
                tvInitialName = itemView.findViewById(R.id.tv_initial_name);
                inviteTextView = itemView.findViewById(R.id.invite_text_view);
                inviteTextView.setVisibility(View.GONE);
            }

            private void bindData(ArrayList<FriendListModel> list) {
                FriendListModel contact = new FriendListModel();
                contact.name = list.get(getAdapterPosition()).name;
                contact.mobileNumber = list.get(getAdapterPosition()).mobileNumber;
                contact.iconHexColor = list.get(getAdapterPosition()).iconHexColor;

                nameTextView.setText(contact.name);
                tvInitialName.setText(AppConstant.getInitials(contact.name));
                numberTextView.setText(list.get(getAdapterPosition()).mobileNumber);

                Drawable drawable = getResources().getDrawable(R.drawable.circuler_text_view);
                drawable.setColorFilter(Color.parseColor(contact.iconHexColor), PorterDuff.Mode.SRC_ATOP);
                tvInitialName.setBackground(drawable);
            }

        }
    }


}
