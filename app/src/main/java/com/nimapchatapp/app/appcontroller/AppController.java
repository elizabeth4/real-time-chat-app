package com.nimapchatapp.app.appcontroller;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.auth.PhoneAuthProvider;
import com.nimapchatapp.app.chatscreen.controller.ChatActivity;
import com.nimapchatapp.app.recentchatuser.controller.RecentChatUserActivity;
import io.fabric.sdk.android.Fabric;
import net.danlew.android.joda.JodaTimeAndroid;


public class AppController extends Application  {

    private static AppController sInstance;
    private ChatActivity chatActivity;
    private RecentChatUserActivity recentChatUserActivity;
    private PhoneAuthProvider.ForceResendingToken forceResendingToken;

    public PhoneAuthProvider.ForceResendingToken getForceResendingToken() {
        return forceResendingToken;
    }

    public void setForceResendingToken(PhoneAuthProvider.ForceResendingToken forceResendingToken) {
        this.forceResendingToken = forceResendingToken;
    }

    public RecentChatUserActivity getRecentChatUserActivity() {
        return recentChatUserActivity;
    }

    public void setRecentChatUserActivity(RecentChatUserActivity recentChatUserActivity) {
        this.recentChatUserActivity = recentChatUserActivity;
    }

    public ChatActivity getChatActivity() {
        return chatActivity;
    }

    public void setChatActivity(ChatActivity chatActivity) {
        this.chatActivity = chatActivity;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        // initialize the singleton
        sInstance = this;
        JodaTimeAndroid.init(this);
    }

    /**
     * @return co.pixelmatter.meme.ApplicationController singleton instance
     */
    public static synchronized AppController getInstance() {
        return sInstance;
    }
}
