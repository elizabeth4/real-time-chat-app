package com.nimapchatapp.app.firbase;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.firebase.database.*;
import com.nimapchatapp.app.chatscreen.model.ChatModel;
import com.nimapchatapp.app.globel.AppConstant;


public class MessagesParser {


    private DatabaseReference reference;

    public FirebaseCallBacksListener listener;

    public interface FirebaseCallBacksListener {

        void didReceivedMessage(ChatModel model);
    }

    public void setFirebaseCallBacksListener(FirebaseCallBacksListener listener){

        this.listener = listener;
    }

    public MessagesParser(DatabaseReference reference){
        this.reference = reference;

    }

    public void getMessages(){

        reference.limitToLast(10)
                .orderByChild("time").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                ChatModel model = new ChatModel(dataSnapshot,AppConstant.COMMING_FROM_NORMAL_MESSAGE);

                if(listener !=null){
                    listener.didReceivedMessage(model);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}