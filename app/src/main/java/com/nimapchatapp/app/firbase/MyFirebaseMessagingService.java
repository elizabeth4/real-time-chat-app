package com.nimapchatapp.app.firbase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.appcontroller.AppController;
import com.nimapchatapp.app.chatscreen.controller.ChatActivity;
import com.nimapchatapp.app.chatscreen.model.MessageModel;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.globel.WhooshhDB;
import com.nimapchatapp.app.profilescreen.model.UserModel;
import com.nimapchatapp.app.recentchatuser.controller.RecentChatUserActivity;
import com.nimapchatapp.app.recentchatuser.model.RecentChatUserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Log.e(TAG, "Message data payload: " + remoteMessage.getData());

            Map<String, String> map = remoteMessage.getData();
            checkNotification(map.toString());

            for (Map.Entry<String, String> entry : map.entrySet()) {
                if(entry.getKey().equalsIgnoreCase("body")){
                   checkNotification(entry.getValue());
                    return;
                }



            }


            String purpose_temp = remoteMessage.getData().get(AppConstant.PARSER_TAG.NOTIFICATION_PURPOSE);

            if(purpose_temp.equalsIgnoreCase(AppConstant.PURPOSE_SEND_MESSGE)){
                checkNotification(remoteMessage.getData().get(AppConstant.PARSER_TAG.NOTIFICATION_BODY));
            }
            else if(purpose_temp.equalsIgnoreCase(AppConstant.PURPOSE_DELETE_MESSGE)){
                checkDeleteNotification(remoteMessage.getData().get(AppConstant.PARSER_TAG.NOTIFICATION_BODY));
            }



//
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
////                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow();
//            }





        }
//        else if (remoteMessage.getNotification() != null)  // Check if message contains a notification payload.
//        {
//            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//
//
//            checkNotification(remoteMessage.getNotification().getBody());
//        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void checkDeleteNotification(String body) {

        try {
            JSONObject messageObject = new JSONObject(body);

            MessageModel messageModel = new MessageModel(messageObject,AppConstant.DEFAULT_ZERO,AppConstant.DEFAULT_EMPTY);

            ChatActivity chatActivity = AppController.getInstance().getChatActivity();
            RecentChatUserActivity recentChatUserActivity = AppController.getInstance().getRecentChatUserActivity();

            if(chatActivity != null){
                //commented for building

                UserModel userModel = chatActivity.getChatWithUser();

                if(userModel.userId.equalsIgnoreCase(messageModel.messageFrom) || userModel.userId.equalsIgnoreCase(messageModel.messageTo)){
                    chatActivity.deleteFirebasePushNotificationMessage(messageModel);
                }
                else {


                    WhooshhDB whooshhDB = new WhooshhDB(getApplicationContext());

                    if(whooshhDB.isMessageExit(messageModel.id)) {
                        messageModel = whooshhDB.getMessageWith(messageModel.id);
                        whooshhDB.deleteMessage(messageModel.id);
                    }

                    //sendChatScreenNotification(messageModel.messageBody,sender_name,user_id);

                    //sendNotificationWhooshh(messageModel.messageBody,messageModel.messageFrom);
                }
            }

            if(recentChatUserActivity!= null){
                recentChatUserActivity.deleteFirebasePushNotificationMessageToRecentChatUser(messageModel);
            }



            if(recentChatUserActivity == null && chatActivity == null){

                WhooshhDB whooshhDB = new WhooshhDB(getApplicationContext());

                if(whooshhDB.isMessageExit(messageModel.id)) {
                    messageModel = whooshhDB.getMessageWith(messageModel.id);
                    whooshhDB.deleteMessage(messageModel.id);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void checkNotification(String body) {

        try {
            JSONObject messageObject = new JSONObject(body);

            MessageModel messageModel = new MessageModel(messageObject,AppConstant.DEFAULT_ZERO,AppConstant.DEFAULT_EMPTY);

            ChatActivity chatActivity = AppController.getInstance().getChatActivity();
            RecentChatUserActivity recentChatUserActivity = AppController.getInstance().getRecentChatUserActivity();

            if(chatActivity != null){

                //commented for building

                UserModel userModel = chatActivity.getChatWithUser();

                if(userModel.userId.equalsIgnoreCase(messageModel.messageFrom) || userModel.userId.equalsIgnoreCase(messageModel.messageTo)){



                    chatActivity.addFirebasePushNotificationMessage(messageModel);
                }
                else {


                    WhooshhDB whooshhDB = new WhooshhDB(getApplicationContext());


                    if(whooshhDB.isMessageExit(messageModel.id)){

                        messageModel = whooshhDB.getMessageWith(messageModel.id);
                        whooshhDB.updateMessage(messageModel);
                    }
                    else {
                        whooshhDB.insertMessage(messageModel);
                    }

                    String sender_name = "",user_id="";

                    if(whooshhDB!=null){
                        RecentChatUserModel recentChatUserModel = whooshhDB.getRecentChatUserWithUserId(messageModel.messageFrom);
                        sender_name = recentChatUserModel.name;
                    }

                    sendChatScreenNotification(messageModel.messageBody,sender_name,user_id);

                    //sendNotificationWhooshh(messageModel.messageBody,messageModel.messageFrom);
                }
            }

             else if(recentChatUserActivity!= null){


                 WhooshhDB whooshhDB = new WhooshhDB(getApplicationContext());


                 if(whooshhDB.isMessageExit(messageModel.id)){

                     messageModel = whooshhDB.getMessageWith(messageModel.id);
                     whooshhDB.updateMessage(messageModel);
                 }
                 else {
                     whooshhDB.insertMessage(messageModel);
                 }

                recentChatUserActivity.addFirebasePushNotificationMessageToRecentChatUser(messageModel);

                 whooshhDB = new WhooshhDB(getApplicationContext());

                 String sender_name = "",user_id="";

                 if(whooshhDB !=null){
                     RecentChatUserModel recentChatUserModel = whooshhDB.getRecentChatUserWithUserId(messageModel.messageFrom);
                     if(recentChatUserModel !=null){
                         sender_name = recentChatUserModel.name;
                         user_id = recentChatUserModel.userId;
                         Log.e("recentChatUserModel",recentChatUserModel.name+" "+recentChatUserModel.userId);
                         //sendChatScreenNotification(messageModel.messageBody,sender_name,user_id);
                     }else {
                         sender_name = messageModel.senderName;
                         user_id = messageModel.messageFrom;
                     }

                 }

                 sendChatScreenNotification(messageModel.messageBody,sender_name,user_id);


                 //sendNotificationWhooshh(messageModel.messageBody,messageModel.messageFrom);

            }



            else if(recentChatUserActivity == null && chatActivity == null){

                WhooshhDB whooshhDB = new WhooshhDB(getApplicationContext());


                if(whooshhDB.isMessageExit(messageModel.id)){

                    messageModel = whooshhDB.getMessageWith(messageModel.id);
                    whooshhDB.updateMessage(messageModel);
                }
                else {
                    whooshhDB.insertMessage(messageModel);
                }

                String sender_name = "",user_id = "";


                if(whooshhDB!=null){
                    RecentChatUserModel recentChatUserModel = whooshhDB.getRecentChatUserWithUserId(messageModel.messageFrom);
                    if(recentChatUserModel !=null){
                        sender_name = recentChatUserModel.name;
                        user_id = recentChatUserModel.userId;
                        Log.e("recentChatBackground",recentChatUserModel.name+" "+recentChatUserModel.userId);
                        //sendBackgroundNotification(messageModel.messageBody,sender_name,user_id);
                    }
                    sender_name = messageModel.senderName;
                    user_id = messageModel.messageFrom;
                }

               sendBackgroundNotification(messageModel.messageBody,sender_name,user_id);


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
    // [END receive_message]


    // [START on_new_token]

//    /**
//     * Called if InstanceID token is updated. This may occur if the security of
//     * the previous token had been compromised. Note that this is called when the InstanceID token
//     * is initially generated so this is where you would retrieve the token.
//     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        String current_token = PreferenceManager.getStringForKey(this,AppConstant.PREFERENCE_TAG.DEVICE_TOKEN,AppConstant.TAG.DEFAULT);

        if(current_token.equalsIgnoreCase(token)){
            PreferenceManager.saveStringForKey(this,AppConstant.PREFERENCE_TAG.DEVICE_TOKEN,AppConstant.TAG.DEFAULT);
        }



//        sendRegistrationToServer(token);
    }
    // [END on_new_token]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
//    private void scheduleJob() {
//        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
//        // [END dispatch_job]
//    }

//    /**
//     * Handle time allotted to BroadcastReceivers.
//     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

//    /**
//     * Persist token to third-party servers.
//     *
//     * Modify this method to associate the user's FCM InstanceID token with any server-side account
//     * maintained by your application.
//     *
//     * @param token The new token.
//     */


    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

//    /**
//     * Create and show a simple notification containing the received FCM message.
//     *
//     * @param messageBody FCM message body received.
//     */


    private void sendBackgroundNotification(String messageBody,String sender_name, String userID) {


        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(AppConstant.USER_INTENT_KEY,userID);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this,channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(sender_name+": "+messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    private void sendChatScreenNotification(String messageBody,String sender_name,String userID) {

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(AppConstant.USER_INTENT_KEY,userID);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this,channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(sender_name+": "+messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}
