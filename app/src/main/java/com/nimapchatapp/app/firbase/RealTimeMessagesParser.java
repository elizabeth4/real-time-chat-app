package com.nimapchatapp.app.firbase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.nimapchatapp.app.chatscreen.model.ChatModel;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.PreferenceManager;

import java.util.HashMap;
import java.util.Map;

public class RealTimeMessagesParser implements ChildEventListener {

    private Context mContext;
    private DatabaseReference senderReference,receiverReference;
    private String senderMessageId="",receiverMessageId="";
    private String myUserId="",mChatUserId="";

    public RealTimeMessagesListener listener;

    public interface RealTimeMessagesListener {

        void didReceivedRealTimeMessages(ChatModel model);
    }

    public void setRealTimeMessagesListener(RealTimeMessagesListener listener){

        this.listener = listener;
    }


    public void getRealTimeMessages(Context context,String myUserId,
                                    String mChatUserId){

        this.mContext = context;
        this.myUserId = myUserId;
        this.mChatUserId = mChatUserId;
        this.senderMessageId = PreferenceManager.getStringForKey(mContext, AppConstant.TAG.REAL_TIME_SENDER, AppConstant.TAG.DEFAULT);
        this.receiverMessageId = PreferenceManager.getStringForKey(mContext, AppConstant.TAG.REAL_TIME_SENDER, AppConstant.TAG.DEFAULT);
        this.senderReference = FirebaseHelper.realTimeSendMessages().child(myUserId + "_" + mChatUserId);
        this.receiverReference = FirebaseHelper.realTimeSendMessages().child(mChatUserId + "_" + myUserId);
        senderReference.addChildEventListener(this);
    }

    public void sendRealTimeMessagesToFirbase(String message){

        Map<String, Object> map = new HashMap<>();
        map.put("message", message);
        map.put("senderId", myUserId);
        map.put("to", mChatUserId);

        if(senderMessageId.equals(AppConstant.TAG.DEFAULT)){
            senderMessageId = senderReference.push().getKey();
            PreferenceManager.saveStringForKey(mContext,AppConstant.TAG.REAL_TIME_SENDER,senderMessageId);
        }

        if(receiverMessageId.equals(AppConstant.TAG.DEFAULT)){
            receiverMessageId = receiverReference.push().getKey();
            PreferenceManager.saveStringForKey(mContext,AppConstant.TAG.REAL_TIME_RECEIVER,receiverMessageId);
        }

        senderReference.child(senderMessageId).setValue(map);
        receiverReference.child(receiverMessageId).setValue(map);

    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        ChatModel model = new ChatModel(dataSnapshot,AppConstant.COMMING_FROM_REAL_TIME);
        if(listener != null){
            listener.didReceivedRealTimeMessages(model);
        }

    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        ChatModel model = new ChatModel(dataSnapshot,AppConstant.COMMING_FROM_REAL_TIME);
        if(listener != null){
            listener.didReceivedRealTimeMessages(model);
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

}
