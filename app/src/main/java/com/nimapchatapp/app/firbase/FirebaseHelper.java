package com.nimapchatapp.app.firbase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nimapchatapp.app.BuildConfig;

public class FirebaseHelper {

    public static final String ROOT_NODE = BuildConfig.ROOT_NODE;
    private static final String BASE_URL = BuildConfig.BASE_URL;
    /*
     * Firebase root nodes
     * */
    public static final String NODE_USERS = "Users";
    public static final String NODE_SEND_FRIEND_REQUEST = "Users";
    public static final String NODE_NOTIFICATION = "notifications";
    public static final String NODE_FRIEND_REQUEST = "Friend_Request";
    public static final String NODE_FRIENDS = "Friends";
    public static final String NODE_MESSAGE = "messages";
    public static final String NODE_REAL_TIME = "realTime";
    public static final String NODE_LIVE_MESSAGE = "liveChat";
    public static final String NODE_CHAT = "Chat";




    /*
     * Authentication reference
     * */
    private static FirebaseAuth mAuth;
    public static FirebaseAuth getAuth()
    {
        if (mAuth == null)
            mAuth = FirebaseAuth.getInstance();
        return mAuth;
    }

    /*
     * Database reference
     * */
    private static DatabaseReference firebase;
    public static DatabaseReference getBaseRef()
    {
        if (firebase == null)
            firebase = FirebaseDatabase.getInstance().getReference();
        return firebase;
    }

    /*
     * Database reference
     * */
    private static FirebaseUser currentUser;
    public static FirebaseUser getCurrentUser()
    {
        if(mAuth != null){
            if (currentUser == null)
                currentUser = mAuth.getCurrentUser();
        }

        return currentUser;
    }


    /**
     * register user
     */


    public static DatabaseReference getUserDateRef(String userId) { return getBaseRef().child(ROOT_NODE).child(NODE_USERS).child(userId); }

    public static DatabaseReference sendMessages()
    {
        return getBaseRef().child(ROOT_NODE).child(NODE_MESSAGE);
    }

    public static DatabaseReference realTimeSendMessages() { return getBaseRef().child(ROOT_NODE).child(NODE_REAL_TIME); }


}

