package com.nimapchatapp.app.otpscreen.controller;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.*;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.appcontroller.AppController;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.BaseAppCompatActivity;
import com.nimapchatapp.app.firbase.FirebaseHelper;
import com.nimapchatapp.app.globel.InternetConnection;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.profilescreen.controller.ProfileActivity;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

public class OTPActivity extends BaseAppCompatActivity implements
        View.OnClickListener {

    private static final String TAG = OTPActivity.class.getSimpleName();

    private EditText mEditTextOtp1, mEditTextOtp2, mEditTextOtp3;
    private TextView mobileNumberTextView;
    private TextView otpVerificationTextView;
    private TextView resendOtpTextView;
    private PhoneAuthProvider.ForceResendingToken forceResendingTokenOtp;
    private String mobileNumber,countryCode;


    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_otp);

        setData();
        setListener();
    }


    //BaseAppCompatActivity
    @Override
    protected void setData() {

        mobileNumber = PreferenceManager.getStringForKey(myContext, AppConstant.PREFERENCE_TAG.MOBILE, "default");
        countryCode = PreferenceManager.getStringForKey(myContext, AppConstant.PREFERENCE_TAG.COUNTRY_CODE, "default");
        getFirebaseOtpToken();
        mEditTextOtp1 = findViewById(R.id.editTextOtp1);
        mEditTextOtp2 = findViewById(R.id.editTextOtp2);
        mEditTextOtp3 = findViewById(R.id.editTextOtp3);
        mobileNumberTextView = findViewById(R.id.mobile_number_text_view);
        otpVerificationTextView = findViewById(R.id.otp_verification_text_view);

        resendOtpTextView = findViewById(R.id.textViewResendOtp);

        mobileNumberTextView.setText(countryCode+"-"+mobileNumber);

    }

    private void getFirebaseOtpToken() {
        forceResendingTokenOtp = AppController.getInstance().getForceResendingToken();
    }

    @Override
    protected void setListener() {
        mEditTextOtp1.addTextChangedListener(new GenericTextWatcher(mEditTextOtp1));
        mEditTextOtp2.addTextChangedListener(new GenericTextWatcher(mEditTextOtp2));
        mEditTextOtp3.addTextChangedListener(new GenericTextWatcher(mEditTextOtp3));
        otpVerificationTextView.setOnClickListener(this);
        resendOtpTextView.setOnClickListener(this);
    }

    public void verifyOtp() {
        if (!TextUtils.isEmpty(mEditTextOtp1.getText()) &&
                mEditTextOtp1.getText().toString().length() == 2 &&
                !TextUtils.isEmpty(mEditTextOtp2.getText()) &&
                mEditTextOtp2.getText().toString().length() == 2 &&
                !TextUtils.isEmpty(mEditTextOtp3.getText()) &&
                mEditTextOtp3.getText().toString().length() == 2) {
            showDialog();
            verifySignInCode();

        } else {
            AppConstant.showToast(myContext, AppConstant.OTP_ERROR_MESSAGE);
        }

    }

    private void verifySignInCode() {

        String codeSent = PreferenceManager.getStringForKey(myContext, AppConstant.PREFERENCE_TAG.CODE_SENT, AppConstant.TAG.DEFAULT);
        String otpNumber = mEditTextOtp1.getText().toString().trim() + "" +
                mEditTextOtp2.getText().toString().trim() + "" +
                mEditTextOtp3.getText().toString().trim();


        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(codeSent, otpNumber);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        FirebaseHelper.getAuth().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            final String uId = FirebaseHelper.getCurrentUser().getUid();

                            final String device_token = FirebaseInstanceId.getInstance().getToken();

                            HashMap<String, String> userMap = new HashMap<>();
                            userMap.put(AppConstant.PARSER_TAG.USERNAME, AppConstant.TAG.DEFAULT);
                            userMap.put(AppConstant.PARSER_TAG.MOBILE, mobileNumber);
                            userMap.put(AppConstant.PARSER_TAG.STATUS, AppConstant.TAG.DEFAULT);
                            userMap.put(AppConstant.PARSER_TAG.IMAGE, AppConstant.TAG.DEFAULT);
                            userMap.put(AppConstant.PARSER_TAG.THUMB_IMAGE, AppConstant.TAG.DEFAULT);
                            userMap.put(AppConstant.PARSER_TAG.DEVICE_TOKEN, device_token);

                            FirebaseHelper.getUserDateRef(uId).setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        hideDialog();

                                        PreferenceManager.saveStringForKey(myContext,AppConstant.PREFERENCE_TAG.MOBILE,mobileNumber);
                                        PreferenceManager.saveStringForKey(myContext,AppConstant.PREFERENCE_TAG.USERNAME,AppConstant.TAG.DEFAULT);
                                        PreferenceManager.saveStringForKey(myContext,AppConstant.PREFERENCE_TAG.DEVICE_TOKEN,device_token);
                                        PreferenceManager.saveStringForKey(myContext,AppConstant.PREFERENCE_TAG.STATUS,AppConstant.TAG.DEFAULT);
                                        PreferenceManager.saveStringForKey(myContext,AppConstant.PREFERENCE_TAG.USER_ID,uId);

                                        Intent intent = new Intent(myContext, ProfileActivity.class);
                                        intent.putExtra(AppConstant.PROFILE_INTENT_KEY,AppConstant.ADD_PROFILE);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        OTPActivity.this.finish();
                                    }else {
                                        hideDialog();
                                        Toast.makeText(myContext,"User Not Register",Toast.LENGTH_LONG).show();
                                    }
                                }
                            });


                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                hideDialog();
                                Toast.makeText(getApplicationContext(),
                                        "Incorrect Verification Code ", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                });
    }

    private void resendVerificationCode() {

        phoneNumber = mobileNumberTextView.getText().toString();
        String[] phoneNumberWithCode = phoneNumber.split("-");

        if (phoneNumber.isEmpty()) {
            Toast.makeText(this, "No Number", Toast.LENGTH_SHORT).show();
            return;
        }else if(InternetConnection.checkConnection(OTPActivity.this)){
            showDialog();
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                     countryCode + phoneNumberWithCode[1],        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks,
                    forceResendingTokenOtp);        // OnVerificationStateChangedCallbacks

        }else {
            AppConstant.showToast(OTPActivity.this,AppConstant.CONNECTION_ERROR_MESSAGE);
        }
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new
            PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                @Override
                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                }

                @Override
                public void onVerificationFailed(FirebaseException e) {

                    hideDialog();
                    e.printStackTrace();

                }

                @Override
                public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                    super.onCodeSent(s, forceResendingToken);
                    forceResendingTokenOtp=forceResendingToken;
                    //codeSent = s;
                    Log.e(TAG, "CODE-------" + s);
                    forcefullyHideDialog();
                    //startActivity(new Intent(myContext, OTPActivity.class));


                }
            };

    private void forcefullyHideDialog(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideDialog();

            }
        },500);
    }





    //View.OnClickListener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewResendOtp:
                resendVerificationCode();
                break;
            case R.id.otp_verification_text_view:
                verifyOtp();
                break;
        }
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.editTextOtp1:
                    if (text.length() == 2)
                        mEditTextOtp2.requestFocus();
                    break;
                case R.id.editTextOtp2:
                    if (text.length() == 2) {
                        mEditTextOtp3.requestFocus();
                    }else if (text.length() == 0) {
                        mEditTextOtp1.requestFocus();
                    }
                    break;
                case R.id.editTextOtp3:
                    if (text.length() == 0) {
                        mEditTextOtp2.requestFocus();
                    }
                    break;
            }
        }







        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }
    }

}
