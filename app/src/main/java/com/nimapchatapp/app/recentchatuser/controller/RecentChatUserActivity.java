package com.nimapchatapp.app.recentchatuser.controller;

import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.appcontroller.AppController;
import com.nimapchatapp.app.chatscreen.controller.ChatActivity;
import com.nimapchatapp.app.chatscreen.model.MessageModel;

import com.nimapchatapp.app.globel.*;
import com.nimapchatapp.app.invite.controller.InviteUserActivity;
import com.nimapchatapp.app.profilescreen.controller.ProfileActivity;

import com.nimapchatapp.app.profilescreen.model.UserModel;
import com.nimapchatapp.app.recentchatuser.model.RecentChatUserModel;
import com.nimapchatapp.app.recentchatuser.model.RecentChatUserParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import io.fabric.sdk.android.Fabric;

public class RecentChatUserActivity extends BaseAppCompatActivity implements RecentChatUserParser.GetRecentChatUserListener {

    private ArrayList<RecentChatUserModel> recentChatUserList;
    private ArrayList<RecentChatUserModel> recentChatUserListFiltered;

    private RecyclerView recyclerView;
    private RecentChatUserAdapter adapter;
    private Toolbar mToolbar;
    private WhooshhDB db;
    private TextView welcomeTextView;
    private UserModel currentLoginUser;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_recent_chat_user);

        setData();
        setListener();

    }

    private void callRecentChatUserApi() {

//        showDialog();

        RecentChatUserParser  recentChatUserParser = new RecentChatUserParser(myContext);
        recentChatUserParser.setListener(RecentChatUserActivity.this);

        String last_message_id = "-1";

        last_message_id = db.getLastMessageIdWith();

        recentChatUserParser.getRecentChatUserWith(last_message_id,currentLoginUser.userToken,currentLoginUser.userId);

    }

    @Override
    protected void setListener() {


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {


                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }

                callRecentChatUserApi();

            }
        });


    }

    @Override
    protected void setData() {

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.recent_chat);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(myContext));
        welcomeTextView = findViewById(R.id.welcome_text_view);
        welcomeTextView.setVisibility(View.GONE);

        swipeRefreshLayout = findViewById(R.id.swipeToRefresh);

        db = new WhooshhDB(myContext);

        currentLoginUser = db.getLoginUser();


        ArrayList<RecentChatUserModel> temp_list = db.getRecentChatUsersWith();


//
        recentChatUserList = new ArrayList<>();
        recentChatUserListFiltered = new ArrayList<>();
//
        recentChatUserList.addAll(temp_list);
       // recentChatUserListFiltered.addAll(temp_list);


        setCurrentScreenToController();

        hideAndShowView(recentChatUserList);

        callRecentChatUserApi();

    }



    private void setCurrentScreenToController() {

        AppController.getInstance().setRecentChatUserActivity(RecentChatUserActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }
        else if (id == R.id.edit_profile) {
            moveToEditProfileActivity();
            return true;
        }
        else if (id == R.id.invite) {

            moveToInviteUserActivity();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void moveToEditProfileActivity() {
        startActivity(new Intent(RecentChatUserActivity.this,ProfileActivity.class).putExtra(AppConstant.PROFILE_INTENT_KEY,AppConstant.EDIT_PROFILE));
    }

    private void moveToInviteUserActivity() {
        startActivity(new Intent(RecentChatUserActivity.this,InviteUserActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.recent_chat_user, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                AppConstant.showErrorLog(TAG, s);
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                AppConstant.showErrorLog(TAG, s);
                return false;
            }
        });


        return true;
    }


    private void hideAndShowView(ArrayList<RecentChatUserModel> recentChatUSerListTemp) {
        if (recentChatUSerListTemp.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);
            welcomeTextView.setVisibility(View.VISIBLE);
            welcomeTextView.setText(AppConstant.NO_RECENT_CHAT_USER_MESSAGE);

        } else {
            recyclerView.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            welcomeTextView.setVisibility(View.GONE);

        }

        setAdapter();
    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new RecentChatUserActivity.RecentChatUserAdapter();
            recyclerView.setAdapter(adapter);

        } else {
            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public void didReceivedGetRecentUserChatResult(int resultCode) {
//        hideDialog();


        if(resultCode == AppConstant.SUCCESS){

            loadDataFromDBAndRefreshUI();

        }

    }

    private void loadDataFromDBAndRefreshUI() {



        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                ArrayList<RecentChatUserModel> temp_list = db.getRecentChatUsersWith();


                Log.e("clearChatUserList", String.valueOf(recentChatUserList.size()));
                Log.e("clearListFiltered", String.valueOf(recentChatUserListFiltered.size()));

                recentChatUserList.clear();
                recentChatUserListFiltered.clear();

                recentChatUserList.addAll(temp_list);
                recentChatUserListFiltered= recentChatUserList ;
                //recentChatUserListFiltered.addAll(temp_list);

                Log.e("addChatUserList", String.valueOf(recentChatUserList.size()));
                Log.e("addListFiltered", String.valueOf(recentChatUserListFiltered.size()));

                hideAndShowView(recentChatUserList);



            }
        });
    }


    class RecentChatUserAdapter extends RecyclerView.Adapter<RecentChatUserActivity.RecentChatUserAdapter.ViewHolder>
            implements Filterable {


        @NonNull
        @Override
        public RecentChatUserActivity.RecentChatUserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_recent_chat_user_list, viewGroup, false);
            return new RecentChatUserActivity.RecentChatUserAdapter.ViewHolder(view);
        }





        @Override
        public void onBindViewHolder(@NonNull RecentChatUserActivity.RecentChatUserAdapter.ViewHolder viewHolder, int i) {

            viewHolder.bindData(recentChatUserListFiltered);
        }

        @Override
        public int getItemCount() {
            return recentChatUserListFiltered.size();
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        recentChatUserListFiltered = recentChatUserList;
                    } else {
                        ArrayList<RecentChatUserModel> filteredList = new ArrayList<>();

                        for (RecentChatUserModel row : recentChatUserList) {


                            // here we are looking for name or phone number match
                            if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.mobileNumber.contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }

                        recentChatUserListFiltered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = recentChatUserListFiltered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                   // recentChatUserListFiltered.clear();
                    recentChatUserListFiltered = (ArrayList<RecentChatUserModel>) filterResults.values;

                    hideAndShowView(recentChatUserListFiltered);

                }
            };
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView nameTextView, senderNameTextView,messageTextView,dateTextView,unreadCountTextView ;
            CircularImageView circularImageView;

            ViewHolder(@NonNull View itemView) {
                super(itemView);
                nameTextView = itemView.findViewById(R.id.tv_name);
                senderNameTextView = itemView.findViewById(R.id.tv_sender_name);
                messageTextView = itemView.findViewById(R.id.tv_message);
                dateTextView = itemView.findViewById(R.id.tv_date);
                unreadCountTextView = itemView.findViewById(R.id.tv_unread_count);
                circularImageView = itemView.findViewById(R.id.img_profile);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                            Intent intent = new Intent(myContext,ChatActivity.class);
//                            Gson gson = new Gson();
//                            String myJson = gson.toJson(getSelectedModel(getAdapterPosition()));
//                            intent.putExtra("ChatWith",myJson);
//
//                            startActivity(intent);

                        moveToChatActivity(getSelectedModel(getAdapterPosition()).userId);

                    }
                });
            }

            private void bindData(ArrayList<RecentChatUserModel> list) {

                RecentChatUserModel contact = list.get(getAdapterPosition());

                nameTextView.setText(contact.name);


                senderNameTextView.setText(contact.lastMessageSenderName + ":");
                messageTextView.setText(contact.lastMessage);

                //12 hour time format
                String date = contact.lastMessageTime;

                String newDate = "";
                String time = "";
                String am = "";

                //12 hour format
                try {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date newTime = format.parse(date);
                    format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
                    String formattedTime = format.format(newTime);

                    StringTokenizer tk1 = new StringTokenizer(formattedTime);

                    newDate = tk1.nextToken();
                    time = tk1.nextToken();
                    am = tk1.nextToken();
                    Log.e("Formatted Time Date ",":::"+time+"::"+formattedTime);

                }catch (ParseException ex){
                    ex.printStackTrace();
                }

                dateTextView.setText((NewDateConvertion.formateddate(newDate)+" "+time+" " +am));
                //dateTextView.setText(DateConvertion.formateddate(contact.lastMessageTime));
                unreadCountTextView.setText(""+contact.unreadMessageCount);

                    unreadCountTextView.setVisibility(View.GONE);

                ImageLoaderPicasso.loadImage(myContext, circularImageView, contact.image);

//                if (contact.isFriend) {
//                    inviteTextView.setBackgroundResource(R.drawable.ic_logo);
//                } else {
//                    inviteTextView.setBackgroundResource(R.drawable.button_design);
//                }

            }

//            private void inviteFriend() {
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                String shareBody = "https://play.google.com/store/apps/details?id=com.scorp.who";
//                String shareSub = "Your subject here";
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//                startActivity(Intent.createChooser(sharingIntent, "Share using"));
//            }

            private RecentChatUserModel getSelectedModel(int position){

                return recentChatUserListFiltered.get(position);
            }

        }
    }

    public void addFirebasePushNotificationMessageToRecentChatUser(MessageModel messageModel){

        boolean temp_flag = db.updateRecentChatUserWithMessage(messageModel);

        /*if(temp_flag){

            callRecentChatUserApi();
            //loadDataFromDBAndRefreshUI();
        }*/

        callRecentChatUserApi();

       /* if(temp_flag){

            callRecentChatUserApi();
            //loadDataFromDBAndRefreshUI();
        }else {
            callRecentChatUserApi();
        }*/



    }


    public void deleteFirebasePushNotificationMessageToRecentChatUser(MessageModel messageModel){


        if(db.isMessageExit(messageModel.id)) {
            messageModel = db.getMessageWith(messageModel.id);
            db.deleteMessage(messageModel.id);
        }

        messageModel = db.getLastMessageWithGroupId(messageModel.messageGroupId);

        boolean temp_flag = db.updateRecentChatUserWithMessage(messageModel);
        callRecentChatUserApi();

        /*if(temp_flag){

            callRecentChatUserApi();
            //loadDataFromDBAndRefreshUI();
        }*/



    }


    private void moveToChatActivity(String userID) {

        NotificationManager notificationManager=(NotificationManager)
                getSystemService(myContext.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        startActivity(new Intent(RecentChatUserActivity.this,ChatActivity.class).putExtra(AppConstant.USER_INTENT_KEY,userID));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().setRecentChatUserActivity(null);
    }



    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Click back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
