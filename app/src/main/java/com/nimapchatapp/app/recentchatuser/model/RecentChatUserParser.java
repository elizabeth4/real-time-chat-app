package com.nimapchatapp.app.recentchatuser.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.AsyncLoaderNew;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.globel.WhooshhDB;
import com.nimapchatapp.app.profilescreen.model.UserModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecentChatUserParser extends AsyncTask<Void, Void, Integer> implements
        AsyncLoaderNew.Listener {


    private String TAG = "RecentChatUserParser";

    // Variable Declarations
    private AsyncLoaderNew asyncLoader;
    private Context context;
    private String responseString;
    private WhooshhDB db;

    // Callbacks for the initiator
    public interface GetRecentChatUserListener {

        void didReceivedGetRecentUserChatResult(int resultCode);
    }

    private RecentChatUserParser.GetRecentChatUserListener listener;

    public void setListener(RecentChatUserParser.GetRecentChatUserListener listener) {
        this.listener = listener;
    }

    public RecentChatUserParser(Context context) {
        super();
        this.context = context;
    }

    public void getRecentChatUserWith(String message_id,String userToken,String userId) {


        asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER,
                AsyncLoaderNew.OutputData.TEXT, context);
        asyncLoader.setListener(this);
        JSONObject jsonObject = new JSONObject();

        db = new WhooshhDB(context);


        PreferenceManager.saveStringForKey(context,AppConstant.HEADER_PARM_USER_ID, userId);
        PreferenceManager.saveStringForKey(context, AppConstant.HEADER_PARM_USER_TOKEN, userToken);



        try {

            // We need set the user type to always vendor

            jsonObject.accumulate(AppConstant.PARSER_TAG.USER_ID, userId);
            jsonObject.accumulate(AppConstant.PARSER_TAG.MESSAGE_ID, message_id);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 4. convert JSONObject to JSON to String
        String jsonString = jsonObject.toString();
        AppConstant.showErrorLog("RecentChatUserParser", "jsonString " + jsonString);
        asyncLoader.requestMessage = jsonString;
        asyncLoader.requestActionMethod = "";
        asyncLoader.executeRequest(AppConstant.BASE_URL + AppConstant.GET_RECENT_CHAT_USER_URL);


    }





    //AsyncLoaderNew.Listener callbacks
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status, int dataIndex) {
        if (listener != null) {
            listener.didReceivedGetRecentUserChatResult(AppConstant.CONNECTION_ERROR);
        }
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {
        responseString = new String(data);
        // we need to call the background thread to process the data.
        AppConstant.showErrorLog("RecentChatUserParser", "responseString " + responseString);
        asyncLoader = null;
        execute();
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }

    // Life cycle of AsyncTask
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    @Override
    protected Integer doInBackground(Void... params) {
        try {
            db = new WhooshhDB(context);
            JSONObject result = new JSONObject(responseString);

            int Status = result.getInt("Status");

            if (Status == 200) {

                //WhooshhDB db = new WhooshhDB(context);


                // It is successful response from the server we can process the json file
                JSONArray recentChatUserArray = result.getJSONArray(AppConstant.PARSER_TAG.DATA);

                RecentChatUserModel recentChatUserModel;
                UserModel userModel;
                JSONObject jsonObject_child;

                for(int i=0;i<recentChatUserArray.length();i++){


                    jsonObject_child = recentChatUserArray.getJSONObject(i);

                    recentChatUserModel = new RecentChatUserModel(jsonObject_child,AppConstant.DEFAULT_ZERO,AppConstant.TAG.DEFAULT,AppConstant.DEFAULT_ONE);

                    userModel = new UserModel();

                    userModel.Lid = AppConstant.DEFAULT_ZERO;
                    userModel.id = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_SERVER_ID);
                    userModel.userToken = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_USER_TOKEN);
                    userModel.name = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_NAME);
                    userModel.userId = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_ID_R);
                    userModel.mobileNumber = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_NUMBER);
                    userModel.country_code = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_COUNTRY_CODE);
                    userModel.image = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_IMAGE);
                    userModel.thumb_image = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_THUMB_IMAGE);
                    userModel.offline_path = "";
                    userModel.device_token = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_DEVICE_TOKEN);
                    userModel.last_seen_hide = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_LAST_SEEN);
                    userModel.auto_download = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_AUTO_DOWNLOAD);
                    userModel.os_type = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_OS_TYPE);
                    userModel.created_date = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_CREATED_DATE);
                    userModel.modified_date = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_MODIFIED_DATE);
                    userModel.status = jsonObject_child.getString(AppConstant.PARSER_TAG.RECEIVER_STATUS);
                    userModel.isLoginUser = AppConstant.DEFAULT_FALSE;

                    if(db.isUserExit(userModel.userId)){
                        userModel.Lid = db.getUserWith(userModel.userId).Lid;
                        db.updateUser(userModel);
                    }else {
                        db.insertUSER(userModel);
                    }

                    if(db.isRecentChatUserExit(recentChatUserModel.userId)){
                        RecentChatUserModel recentChatUserModel_temp = db.getRecentChatUserWithUserId(recentChatUserModel.userId);
                        recentChatUserModel_temp.image = recentChatUserModel.image;
                        recentChatUserModel_temp.thumb_image = recentChatUserModel.thumb_image;

                        recentChatUserModel_temp.lastMessageSenderName = recentChatUserModel.lastMessageSenderName;
                        Log.e("jsonObject_child","::"+recentChatUserModel.lastMessageSenderName);
                        db.updateRecentChatUser(recentChatUserModel_temp);
                    }
                    else {

                        db.insertRecentChatUser(recentChatUserModel);
                    }
                }
                return AppConstant.SUCCESS;

            } else if (Status == 201) {
                return AppConstant.PROCESSING_ERROR;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            AppConstant.showErrorLog("JSONException", "JSONException " + e);
            cancel(true);
        }

        return AppConstant.PROCESSING_ERROR;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        int resultCode = integer.intValue();

        if (listener != null) {
            if (resultCode == AppConstant.SUCCESS) {
                listener.didReceivedGetRecentUserChatResult(AppConstant.SUCCESS);
            } else {
                listener.didReceivedGetRecentUserChatResult(AppConstant.PROCESSING_ERROR);
            }
        }
    }
}

