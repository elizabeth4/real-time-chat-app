package com.nimapchatapp.app.recentchatuser.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.WMasterModel;
import org.json.JSONException;
import org.json.JSONObject;

public class RecentChatUserModel extends WMasterModel implements Parcelable {



    public String Lid;
    public String offline_path;
    public String lastSeenHideFlag;
    public String lastSeenHide;
    public String lastMessage;
    public String lastMessageId;
    public String lastMessageType;
    public String lastMessageTime;
    public int unreadMessageCount;
    public String lastMessageSenderName;
    public String lastMessageSenderNumber;
    public String groupId;
    public String groupName;
//    public int isGroup;


    public RecentChatUserModel() { }

    public RecentChatUserModel(JSONObject recentUserObject, String lid, String offline_path, String unreadMessageCount) throws JSONException {


        this.Lid = lid;
        userId = recentUserObject.getString(AppConstant.PARSER_TAG.RECEIVER_ID_R);
        name = recentUserObject.getString(AppConstant.PARSER_TAG.RECEIVER_NAME);
        mobileNumber = recentUserObject.getString(AppConstant.PARSER_TAG.RECEIVER_NUMBER);
        lastSeenHideFlag = recentUserObject.getString(AppConstant.PARSER_TAG.RECEIVER_LAST_SEEN);
        lastSeenHide = "";//recentUserObject.getString(AppConstant.PARSER_TAG.RECEIVER_LAST_SEEN);
        image = recentUserObject.getString(AppConstant.PARSER_TAG.RECEIVER_IMAGE);
        this.offline_path = offline_path;
        lastMessageId =  recentUserObject.getString(AppConstant.PARSER_TAG.ID);
        lastMessageTime = recentUserObject.getString(AppConstant.PARSER_TAG.CREATED_DATE);
        lastMessageType = recentUserObject.getString(AppConstant.PARSER_TAG.MESSAGE_TYPE);
        lastMessage = recentUserObject.getString(AppConstant.PARSER_TAG.MESSAGE_BODY);
        this.unreadMessageCount = Integer.parseInt(unreadMessageCount);
        groupId = recentUserObject.getString(AppConstant.PARSER_TAG.GROUP_ID);
        groupName = recentUserObject.getString(AppConstant.PARSER_TAG.GROUP_NAME);
        lastMessageSenderNumber = recentUserObject.getString(AppConstant.PARSER_TAG.LAST_USER_NUMBER);
        lastMessageSenderName = recentUserObject.getString(AppConstant.PARSER_TAG.LAST_USER_NAME);

    }


    protected RecentChatUserModel(Parcel in) {
        Lid = in.readString();
        offline_path = in.readString();
        lastSeenHideFlag = in.readString();
        lastSeenHide = in.readString();
        lastMessage = in.readString();
        lastMessageId = in.readString();
        lastMessageType = in.readString();
        lastMessageTime = in.readString();
        unreadMessageCount = in.readInt();
        lastMessageSenderName = in.readString();
        lastMessageSenderNumber = in.readString();
        groupId = in.readString();
        groupName = in.readString();
//        isGroup = in.readInt();
    }

    public static final Creator<RecentChatUserModel> CREATOR = new Creator<RecentChatUserModel>() {
        @Override
        public RecentChatUserModel createFromParcel(Parcel in) {
            return new RecentChatUserModel(in);
        }

        @Override
        public RecentChatUserModel[] newArray(int size) {
            return new RecentChatUserModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Lid);
        dest.writeString(offline_path);
        dest.writeString(lastSeenHideFlag);
        dest.writeString(lastSeenHide);
        dest.writeString(lastMessage);
        dest.writeString(lastMessageId);
        dest.writeString(lastMessageType);
        dest.writeString(lastMessageTime);
        dest.writeInt(unreadMessageCount);
        dest.writeString(lastMessageSenderName);
        dest.writeString(lastMessageSenderNumber);
        dest.writeString(groupId);
        dest.writeString(groupName);
//        dest.writeInt(isGroup);
    }
}
