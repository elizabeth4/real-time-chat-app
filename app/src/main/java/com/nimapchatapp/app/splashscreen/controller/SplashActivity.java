package com.nimapchatapp.app.splashscreen.controller;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.BaseAppCompatActivity;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.recentchatuser.controller.RecentChatUserActivity;
import com.nimapchatapp.app.registerscreen.controller.RegistrationActivity;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseAppCompatActivity {
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);

/*button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

    }
});*/


        // Splash screen timer
        int SPLASH_TIME_OUT = 3000;
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                isUserLogin();

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);


    }


    private void isUserLogin() {

        String userName = PreferenceManager.getStringForKey(myContext,AppConstant.PREFERENCE_TAG.USERNAME,AppConstant.TAG.DEFAULT);
        /* if user name is name means user not register yet */
        if(userName.equals("default")){
            startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
        }else {
            startActivity(new Intent(getApplicationContext(), RecentChatUserActivity.class));
        }

    }

    //BaseAppCompatActivity
    @Override
    protected void setData() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
