package com.nimapchatapp.app.registerscreen.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.nimapchatapp.app.R;
import com.nimapchatapp.app.appcontroller.AppController;
import com.nimapchatapp.app.countrycodescreen.controller.CountryCodeFragment;
import com.nimapchatapp.app.countrycodescreen.model.CountryCodeModel;
import com.nimapchatapp.app.globel.AppConstant;
import com.nimapchatapp.app.globel.BaseAppCompatActivity;
import com.nimapchatapp.app.globel.InternetConnection;
import com.nimapchatapp.app.globel.PreferenceManager;
import com.nimapchatapp.app.otpscreen.controller.OTPActivity;

import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

public class RegistrationActivity extends BaseAppCompatActivity implements

        View.OnClickListener, CountryCodeFragment.GetCodeListener {

    private static final String TAG = RegistrationActivity.class.getSimpleName();

    private CountryCodeFragment countryCodeFragment;
    private FragmentTransaction fragmentTransaction;

    private TextView submitTextView, countryCodeTextView;
    private EditText numberEditText;

    private String phoneNumber;
    private String countryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_registration);

        setData();
        setListener();

    }

    //BaseAppCompatActivity
    @Override
    protected void setData() {
        submitTextView = findViewById(R.id.submit_text_view);
        numberEditText = findViewById(R.id.number_edit_text);
        countryCodeTextView = findViewById(R.id.country_code_text_view);
        // set default india country code
        countryCode = "+91";
        countryCodeTextView.setText(countryCode);

    }

    @Override
    protected void setListener() {
        submitTextView.setOnClickListener(this);
        countryCodeTextView.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (countryCodeFragment != null) {
            unloadCountyCodeFragment();
        } else {
            super.onBackPressed();
        }
    }

    private void loadCountyCodeFragment() {
        if (countryCodeFragment == null) {
            countryCodeFragment = new CountryCodeFragment();
            countryCodeFragment.setCodeListener(this);
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frameLayout, countryCodeFragment);
            fragmentTransaction.commit();
        }
    }

    private void unloadCountyCodeFragment() {

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (countryCodeFragment != null) {
            fragmentTransaction.remove(countryCodeFragment);
            countryCodeFragment = null;
        }
        fragmentTransaction.commit();
    }


    private void sendVerificationCode() {

        phoneNumber = numberEditText.getText().toString();

        if (phoneNumber.isEmpty()) {
            numberEditText.setError("Phone number is required");
            numberEditText.requestFocus();
            return;
        } else if (phoneNumber.length() < 10) {
            numberEditText.setError("Please enter a valid phone");
            numberEditText.requestFocus();
            return;
        }


        if (InternetConnection.checkConnection(RegistrationActivity.this)) {
            showDialog();
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    countryCode + phoneNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks
        } else {
            AppConstant.showToast(RegistrationActivity.this, AppConstant.CONNECTION_ERROR_MESSAGE);
        }

    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new
            PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                @Override
                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                }

                @Override
                public void onVerificationFailed(FirebaseException e) {

                    hideDialog();
                    e.printStackTrace();

                }

                @Override
                public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                    super.onCodeSent(s, forceResendingToken);

                    PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.MOBILE, phoneNumber);
                    PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.COUNTRY_CODE, countryCode);
                    PreferenceManager.saveStringForKey(myContext, AppConstant.PREFERENCE_TAG.CODE_SENT, s);

                    setFirebaseOtpTokenToController(forceResendingToken);
                    //codeSent = s;
                    Log.e(TAG, "CODE-------" + s);
                    startActivity(new Intent(myContext, OTPActivity.class));
                    hideDialog();
                }
            };

    private void setFirebaseOtpTokenToController(PhoneAuthProvider.ForceResendingToken forceResendingToken) {
        AppController.getInstance().setForceResendingToken(forceResendingToken);
    }


    //CountryCodeFragment.GetCodeListener
    @Override
    public void getCountryCode(CountryCodeModel model) {

        if (model != null) {
            unloadCountyCodeFragment();
            countryCode = model.phonecode;
            countryCodeTextView.setText("+" + model.phonecode);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_text_view:

                sendVerificationCode();

                break;
            case R.id.country_code_text_view:
                loadCountyCodeFragment();
                break;
        }
    }

}

